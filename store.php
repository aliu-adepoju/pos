<?php
session_start();
session_regenerate_id(); 
if(!isset($_SESSION['usrID'])){
	header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'controllers/__store.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="";
switch (@$_GET["p"]) {
	case "add":
	case "edit":
		$layout->content(NULL,"view/store/_edit.php");
	break;
	default:
		$layout->content("view/store/_toolbar.php","view/store/_body.php");
	break;
}

/* if(isset($_SERVER["QUERY_STRING"]) && !empty($_SERVER["QUERY_STRING"])){
	list($storeID,$storeName)=json_decode(base64_decode($_SERVER["QUERY_STRING"]),1);
	$layout->content("view/store/_toolbar.php","view/store/_stock.php");
}
else{
	$layout->content("view/store/_toolbar.php","view/store/_body.php");
} */
?>  