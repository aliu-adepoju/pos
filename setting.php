<?php
session_start();
session_regenerate_id(); 
if(!isset($_SESSION['usrID'])){
	header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'controllers/__setting.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="";
/* if(!in_array($_SESSION["roleID"],array(1))){
	header("Location: dashboard.php?auth=Access denied");
	exit(0);
} */
$layout->content(NULL,"view/setting/_body.php");
?>  