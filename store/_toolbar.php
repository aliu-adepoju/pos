<?php
global $pdo;
$valuation=$pdo->query("select * from syncValuation");
$valuation=$valuation->fetch(PDO::FETCH_ASSOC);
?>
<div class="row">
<div class="col-md-2 no-padding"><h3 class="no-margin lh40">Sales</h3></div>
<div class="col-md-6 no-padding adjMt10">
            </div>
            <div class="col-md-4 no-padding adjMt10">
            <ul class="nav nav-pills" role="tablist">
  <li>
    <a href="report.php#outofstock" class="fs15">
      <span class="absTR badge bg-color-danger"><?=$valuation["outofstock"];?></span>
      Out of stock
    </a>
  </li>
  <li>
    <a href="report.php#reorder" class="fs15">
      <span class="absTR badge bg-color-warning"><?=$valuation["reorderlevel"];?></span>
     Re-order level
    </a>
  </li>
  <li>
    <a href="javascript:void(0)" class="fs15">
      <span class="absTR badge bg-color-info"><?=$valuation["backorder"];?></span>
     Backorder
    </a>
  </li>
</ul>
            
            </div>
            </div>