<?php
ini_set("display_errors",1);
include_once '../config.php';
$date=str_replace("-","",$_GET["date"]);
$filter="from_unixtime(date,'%Y%m%d')=$date";

$profile=$pdo->query("select * from profile where profileID=1");
$config=$profile->fetch(PDO::FETCH_ASSOC);
$sales=$pdo->query("select *,s.product as pname,s.customer as cname,s.qty,((s.sprice*s.qty)-s.deduct) as netsale,s.qty,s.discount,s.status from syncSale s where $filter order by s.date desc");
$sales=$sales->fetchAll(PDO::FETCH_ASSOC);
$salesType=$_GET["salesType"];

list($y,$m,$d)=explode("-",$_GET["date"]);
function arrayRowQuery($stack,$key,$query){
	$arr=array();
	if(is_null($query)){
		foreach ($stack as $i=>$data){
			$arr[]=$data[$key];
		}
		return $arr;
	}
	foreach ($stack as $i=>$data){
		if($data["status"]==$query)
			$arr[]=$key=="*"?$data:$data[$key];

	}
	return $arr;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <style>
      body, h1, h2, h3, h4, h5, h6{
      font-family: tahoma, sans-serif;
      }
    </style>
  </head>
  <?php 
  flush();
  ?>
  <body onload="window.print()">
    <div class="container">
    <h3 class="text-right"><img src="../images/logo/<?=$config["logo"];?>" alt="<?=$config["name"];?> Logo" class="pull-left"/> <?=$config["name"];?> | <?=date("D, j M Y",mktime(0, 0, 0,$m,$d,$y))." Sales";?></h3>
<table class="table table-striped table-bordered" id="sales_data_table">
<thead>
<tr><th>#</th><th>Invoice</th><th>Items</th><th>Price</th><th>Discount</th><th>Qty.</th><th>Line Total</th><th>Customer</th><th>Seller</th><th>Store</th></tr>
</thead>
<tbody>
<?php 
$discountList=array();
$LTs=array();
$QTYs=array();
foreach (arrayRowQuery($sales,"*",$salesType) as $i=>$fetch){
	$each=json_decode($fetch["discount"],1)["each"];
	$global=json_decode($fetch["discount"],1)["global"];
	$discountList[]=(($global*$fetch["sprice"])/100)+$each;
	$QTYs[]=$fetch["qty"];
	$LTs[]=($fetch["sprice"]*$fetch["qty"])-($discountList[$i]*$fetch["qty"]);
?>
<tr><td><?=$i+1;?></td><td><?=$fetch["invoice"];?></td><td><?="{$fetch["pname"]}";?></td><td><?=$fetch["sprice"];?></td><td><?=number_format($discountList[$i],2);?></td><td><?=$fetch["qty"];?></td><td><?=number_format($LTs[$i],2);?></td><td><?=$fetch["cname"];?></td><td><?=$fetch["seller"];?></td><td><?="store{$fetch["storeID"]}";?></td></tr>
<?php 
}
?>
<tr><td colspan="5"></td><td><?=array_sum($QTYs);?></td><td><?=number_format(array_sum($LTs),2);?></td><td colspan="2"></td></tr>
</tbody>
</table>
<!-- 
      <div class="clearfix">&nbsp;</div>
      <table class="table table-striped table-bordered" id="sales_data_table">
		<tbody>
		<tr><td>Line Total</td><td><?=number_format(array_sum($LTs),2);?></td></tr>
		<tr><td>Total Discount</td><td><?=number_format(array_sum($LTs),2);?></td></tr>
		</tbody>
		</table>
 -->
    </div>
  </body>
</html>