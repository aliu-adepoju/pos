<?php
include_once '../config.php';
$date=str_replace("-","",$_GET["date"]);
$date=str_replace("-","",$_GET["date"]);
$filter="from_unixtime(date,'%Y%m%d')=$date";
$sales=$pdo->query("select *,s.product as pname,s.customer as cname,s.qty,((s.sprice*s.qty)-s.deduct) as netsale,s.qty,s.discount,s.status from syncSale s where $filter order by s.date desc");
$sales=$sales->fetchAll(PDO::FETCH_ASSOC);
$salesType=$_GET["salesType"];

list($y,$m,$d)=explode("-",$_GET["date"]);
function arrayRowQuery($stack,$key,$query){
	$arr=array();
	if(is_null($query)){
		foreach ($stack as $i=>$data){
			$arr[]=$data[$key];
		}
		return $arr;
	}
	foreach ($stack as $i=>$data){
		if($data["status"]==$query)
			$arr[]=$key=="*"?$data:$data[$key];

	}
	return $arr;
}

ob_end_clean();
$filename="sales_$date";
header("Content-Type: text/csv");
header("Content-Disposition: attachment;filename={$filename}.csv");
$fp=fopen('php://output','w');
fputcsv($fp,array("Invoice","Items","Price","Discount","Qty.","Line Total","Customer","Seller","store"));
$discountList=array();
foreach (arrayRowQuery($sales,"*",$salesType) as $i=>$fetch){
	$each=json_decode($fetch["discount"],1)["each"];
	$global=json_decode($fetch["discount"],1)["global"];
	$discountList[]=(($global*$fetch["sprice"])/100)+$each;
	fputcsv($fp,array("{$fetch["invoice"]}","{$fetch["pname"]}",$fetch["sprice"],number_format($discountList[$i],2),$fetch["qty"],number_format(($fetch["sprice"]*$fetch["qty"])-($discountList[$i]*$fetch["qty"]),2),$fetch["cname"],$fetch["seller"],"store{$fetch["storeID"]}"));
}
fclose($fp);