<?php
session_start();
session_regenerate_id(); 
if(!isset($_SESSION['usrID'])){
	header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';

$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="";
$config=$pdo->query("select * from config where configID=1");
$config=$config->fetch(PDO::FETCH_ASSOC);
if(isset($_GET["edit"]) && !empty($_GET["edit"])){
	$stockID=intval($_GET["edit"]);
	$stock=$pdo->query("select s.*,m.name as mname,p.name as pname,c.name as cname,i.name as iname from (stock s join manufacturer m on m.manufacturerID join product p on p.productID join category c on c.categoryID join img i on i.imgID) where m.manufacturerID=p.manufacturerID and p.productID=s.productID and c.categoryID=p.categoryID and i.imgID=s.imgID and s.stockID=$stockID");
	$data=$stock->fetch(PDO::FETCH_ASSOC);
	$layout->content(NULL,"view/stock/_add.php");
	exit();
}
switch (@$_GET["p"]) {
	case "add":
		/* if(!in_array($_SESSION["roleID"],array(1,2))){
			header("Location: dashboard.php?auth=Access denied");
			exit(0);
		} */
		$layout->content(NULL,"view/stock/_add.php");
	break;
	case "multiple":
		/* if(!in_array($_SESSION["roleID"],array(1,2))){
			header("Location: dashboard.php?auth=Access denied");
			exit(0);
		} */
		$layout->content(NULL,"view/stock/_addMultiple.php");
	break;
	default:
		$layout->content("view/stock/_toolbar.php","view/stock/_body.php");
	break;
} 