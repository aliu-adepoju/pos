function factorial(n){
	return (n>0)?(n*factorial(n-1)):(1);	
}

function rad(n){
	return ($('#degRad').value=='deg')?((Math.PI/180)*n):(n);
}

function arad(){
	return ($('#degRad').value=='deg')?(180/Math.PI):(1);
}


var memory=[];
function inv() {
	toggle("2ndfunc",function(){
	$("#sin_btn").html("sin<sup>-1</sup>");
	$("#sin_btn").value="sin<sup>-1</sup>(";
	
	$("#cos_btn").html("cos<sup>-1</sup>");
	$("#cos_btn").value="cos<sup>-1</sup>(";
	
	$("#tan_btn").html("tan<sup>-1</sup>");
	$("#tan_btn").value="tan<sup>-1</sup>(";
	
	$("#euler_btn").html("e<sup>x</sup>");
	$("#euler_btn").value="e<sup>(";
	$("#euler_btn").setAttribute("onclick","baseBtn(this)");
	
	$("#log_btn").html("10<sup>x</sup>");
	$("#log_btn").value="10<sup>(";
	$("#log_btn").setAttribute("onclick","baseBtn(this)");
	
	$("#pow_btn").html("x<sup>2</sup>");
	$("#pow_btn").value="<sup>(2)</sup>";
	$("#pow_btn").setAttribute("onclick","signBtn(this)");
	
	$("#sqrt_btn").html("<sup>y</sup>√x");
	$("#sqrt_btn").value="√(";
	$("#sqrt_btn").setAttribute("onclick","rootBtn(this)");
	
	$("#rand_btn").html("Rnd");
	$("#rand_btn").value=Math.random();
	$("#rand_btn").setAttribute("onclick","rndBtn(this)");
	
},
function(){
	$("#sin_btn").html("sin");
	$("#sin_btn").value="sin(";
	
	$("#cos_btn").html("cos");
	$("#cos_btn").value="cos(";
	
	$("#tan_btn").html("tan");
	$("#tan_btn").value="tan(";
	
	$("#euler_btn").html("ln");
	$("#euler_btn").value="ln(";
	$("#euler_btn").setAttribute("onclick","funcBtn(this)");
	
	$("#log_btn").html("log<sub>10</sub><sup>x</sup>");
	$("#log_btn").value="log(";
	$("#log_btn").setAttribute("onclick","funcBtn(this)");
	
	$("#pow_btn").html("√");
	$("#pow_btn").value="√(";
	$("#pow_btn").setAttribute("onclick","rootBtn(this)");
	
	$("#sqrt_btn").html("x<sup>y</sup>");
	$("#sqrt_btn").value="<sup>(";
	$("#sqrt_btn").setAttribute("onclick","powBtn(this)");
	
	$("#rand_btn").html("Ans");
	$("#rand_btn").value='Ans';
	$("#rand_btn").setAttribute("onclick","ansBtn(this)");
}
);
}

function numBtn(obj) {
	$('#screen').scrollLeft=$('#screen').scrollWidth;
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
	if (memory.indexOf("<span class='close-bracket'>)</span></sup>") != -1) {
		//handle upper bracket
		numBtn2(obj);
		return false;
}

   $('#ce_btn').html("CE");
	$('#ce_btn').value='ce';	
	

if (lastDataIn != -1) {
		memory=memory.array_extend(lastDataIn,[(/[0-9\x\+\%\-\.\÷\E]+/.test(memory[lastDataIn-1]) || memory[lastDataIn-1].indexOf('(') != -1)?(obj.value):(' x '+obj.value)]);
		$("#screen").html(memory.join(""));
		return false;
		}	
		
	memory.push((!/[0-9\+\-\x\÷\%\.\E]+/.test(memory[memory.length-1]) && memory.length > 0 || ['e'].in_array(memory[memory.length-1]))?(" x "+obj.value):(obj.value));
	$("#screen").html(memory.join(""));
}

function numBtn2(obj) {
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span></sup>");
 if (lastDataIn != -1) {
		memory=memory.array_extend(lastDataIn,[obj.value]);
		$("#screen").html(memory.join(""));
		return false;
		}	
		
	memory.push((((!/[0-9+-x÷%]+/.test(memory[memory.length-1]) || ['e','Ans'].in_array(memory[memory.length-1])) && memory.length > 0))?(" x "+obj.value):(obj.value));
	$("#screen").html(memory.join(""));
}


function funcBtn(obj) {
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
   if(lastDataIn != -1) {
		memory=memory.array_extend(lastDataIn,[(/[\+\-\x\÷\%]+/.test(memory[lastDataIn-1]) || memory[lastDataIn-1].indexOf('(') != -1)?(obj.value):(' x '+obj.value),"<span class='close-bracket'>)</span>"]);
		$("#screen").html(memory.join(""));
		return false;
		}	
	
	memory.push(((!/[\+\-\x\÷]+/.test(memory[memory.length-1]) && memory.length > 0) || /[0-9\)]+/.test(memory[memory.length-1]))?(' x '+obj.value):(obj.value));
	memory.push("<span class='close-bracket'>)</span>");
	$("#screen").html(memory.join(""));
	}

function piBtn(obj) {
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
if (lastDataIn != -1) {
		memory=memory.array_extend(lastDataIn,[(/[\+\-\x\÷\%]+/.test(memory[lastDataIn-1]) || memory[lastDataIn-1].indexOf('(') != -1)?(obj.value):(' x '+obj.value)]);
		$("#screen").html(memory.join(""));
		return false;
		}	
	
	memory.push((/[\+\-\x\÷\%]+/.test(memory[memory.length-1]) || memory.length==0)?(obj.value):(' x '+obj.value));
	$("#screen").html(memory.join(""));
	}

function rootBtn(obj) {
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
if (lastDataIn != -1) {
		memory=memory.array_extend(lastDataIn,[(!/[0-9+-x÷%\(]+/.test(memory[lastDataIn-1]))?(' x '+obj.value):(obj.value),"<span class='close-bracket'>)</span>"]);
		$("#screen").html(memory.join(""));
		return false;
		}	
	
	memory.push((!/[0-9+-x÷%]+/.test(memory[memory.length-1]) && memory.length > 1)?(' x '+obj.value):(obj.value));
	memory.push("<span class='close-bracket'>)</span>");
	$("#screen").html(memory.join(""));
	}

function closeBracketBtn(obj) {
	if (memory.indexOf("<span class='close-bracket'>)</span></sup>") != -1) {
		memory[memory.indexOf("<span class='close-bracket'>)</span></sup>")]=")</sup>";	
		$("#screen").html(memory.join(""));
		return false;
		}

	for (var i=0;i<memory.length;i++) {
		if (memory[i]=="<span class='close-bracket'>)</span>") {
			memory[i]=obj.value;	
			$("#screen").html(memory.join(""));
			return false;
		}
		}
	 var nest=[(memory.toString().match(/\(/g))?(memory.toString().match(/\(/g).length):(0),(memory.toString().match(/\)/g))?(memory.toString().match(/\)/g).length):(0)];//" "+memory.toString().match(/\)/g).length
     if (nest[0]==nest[1]) return false;
		
	memory.push(obj.value);
	$("#screen").html(memory.join(""));
}

function openBracketBtn(obj) {
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
	if (lastDataIn != -1){
   memory=memory.array_extend(lastDataIn,[obj.value,"<span class='close-bracket'>)</span>"]);
	$("#screen").html(memory.join(""));
	return false;
	}

	memory.push((!/[\+\-\x\÷\%]+/.test(memory[memory.length-1]) && memory.length > 0)?(' x '+obj.value):(obj.value));
	memory.push("<span class='close-bracket'>)</span>");
	$("#screen").html(memory.join(""));
}

function ClearEntry(obj) {
	if (memory.indexOf("<span class='close-bracket'>)</span></sup>") != -1) {//handle upper bracket
		ClearEntry2();
		return false;
}
	var nest,bool=true,closeBracketPos=memory.indexOf("<span class='close-bracket'>)</span>");
	if (closeBracketPos != -1){
		if(memory[closeBracketPos-1]=="("){
		memory=memory.array_remove(closeBracketPos-1).array_remove(closeBracketPos-1);
	   $("#screen").html((memory.length==0)?(0):(memory.join("")));
		return false;
		}
	
	   memory=memory.array_remove(closeBracketPos-1);
     nest=[(memory.toString().match(/\(/g))?(memory.toString().match(/\(/g).length):(0),(memory.toString().match(/\)/g))?(memory.toString().match(/\)/g).length):(0)];//" "+memory.toString().match(/\)/g).length
     if (nest[0] > nest[1]) memory.push("<span class='close-bracket'>)</span>");
     if (nest[0] < nest[1]) memory=memory.array_remove(memory.indexOf("<span class='close-bracket'>)</span>"));
		$("#screen").html((memory.length==0 || memory[0]=="<span class='close-bracket'>)</span>")?(0):(memory.join("")));
		if (memory.length==0 || memory[0]=="<span class='close-bracket'>)</span>") {
			memory=[];
			}
		return false;
		}
	memory.pop();
	nest=[(memory.toString().match(/\(/g))?(memory.toString().match(/\(/g).length):(0),(memory.toString().match(/\)/g))?(memory.toString().match(/\)/g).length):(0)];//" "+memory.toString().match(/\)/g).length
   if (nest[0] > nest[1]) memory.push("<span class='close-bracket'>)</span>");

	$("#screen").html((memory.length==0)?(0):(memory.join("")));
}


function ClearEntry2(obj) {
	var nest,bool=true,closeBracketPos=memory.indexOf("<span class='close-bracket'>)</span></sup>");
	if (closeBracketPos != -1){
		if(memory[closeBracketPos-1]=="("){
		memory=memory.array_remove(closeBracketPos-1).array_remove(closeBracketPos-1);
	   $("#screen").html((memory.length==0)?(0):(memory.join("")));
		return false;
		}
	
	  memory=memory.array_remove(closeBracketPos-1);
     nest=[(memory.toString().match(/\(/g))?(memory.toString().match(/\(/g).length):(0),(memory.toString().match(/\)/g))?(memory.toString().match(/\)/g).length):(0)];//" "+memory.toString().match(/\)/g).length
     if (nest[0] > nest[1]) memory.push("<span class='close-bracket'>)</span></sup>");
     if (nest[0] < nest[1]) memory=memory.array_remove(memory.indexOf("<span class='close-bracket'>)</span></sup>"));
		$("#screen").html((memory.length==0 || memory[0]=="<span class='close-bracket'>)</span></sup>")?(0):(memory.join("")));
		if (memory.length==0 || memory[0]=="<span class='close-bracket'>)</span></sup>") {
			memory=[];
			}
		return false;
		}
	memory.pop();
	nest=[(memory.toString().match(/\(/g))?(memory.toString().match(/\(/g).length):(0),(memory.toString().match(/\)/g))?(memory.toString().match(/\)/g).length):(0)];//" "+memory.toString().match(/\)/g).length
   if (nest[0] > nest[1]) memory.push("<span class='close-bracket'>)</span></sup>");
	$("#screen").html((memory.length==0)?(0):(memory.join("")));
}

function baseBtn(obj) {
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
	 if(lastDataIn != -1) {
		memory=memory.array_extend(lastDataIn,[(/[\+\-\x\÷\%]+/.test(memory[lastDataIn-1]) || memory[lastDataIn-1].indexOf('(') != -1)?(obj.value):(' x '+obj.value),"<span class='close-bracket'>)</span>"]);
		$("#screen").html(memory.join(""));
		return false;
		}	
	
	memory.push(((!/[+-x÷]+/.test(memory[memory.length-1]) && memory.length > 0) || /[0-9\)]+/.test(memory[memory.length-1]))?(' x '+obj.value):(obj.value));
	memory.push("<span class='close-bracket'>)</span></sup>");
	$("#screen").html(memory.join(""));
}

function powBtn(obj) {
	if(memory.length==0) return false;
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
	if (lastDataIn != -1) {
		if(!/[0-9]+/.test(memory[lastDataIn-1])) return false;
		memory=memory.array_extend(lastDataIn,[obj.value,"<span class='close-bracket'>)</span></sup>"]);
		$("#screen").html(memory.join(""));
		return false;
		}	
	
	if(!/[0-9]+/.test(memory[memory.length-1])) return false;
   memory.push(obj.value);
	memory.push("<span class='close-bracket'>)</span></sup>");
	$("#screen").html(memory.join(""));
}

function signBtn(obj) {
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
	switch(obj.value) {
	case '÷':
	case '+':
	case 'x':
	case '!':
	case '%':
	case 'E':
	case '<sup>(2)</sup>':
		if(memory.length==0) return false;
	break;
	
}


	if(lastDataIn != -1) {
		memory=memory.array_extend(lastDataIn,[(/[\+\-\x\÷]+/.test(obj.value))?(" "+obj.value+" "):(obj.value)]);
		$("#screen").html(memory.join(""));
		return false;
		}	
		
	memory.push((/[\+\-\x\÷]+/.test(obj.value))?(" "+obj.value+" "):(obj.value));
	$("#screen").html(memory.join(""));
}

function rndBtn() {
	memory=[];
    memory.push(roundTo(Math.random(),5));
	$("#screen").html(memory.join(""));	
}

var _ans=0;
function ansBtn(obj) {
	var lastDataIn=memory.indexOf("<span class='close-bracket'>)</span>");
	if(!_ans) return false;
	$('#ansBox').html(_ans);
	if (lastDataIn != -1) {
		memory=memory.array_extend(lastDataIn,[obj.value]);
		$("#screen").html(memory.join(""));
		return false;
		}	
	
	memory.push(((!/[+-x÷]+/.test(memory[memory.length-1])  || /[0-9e(Ans)]+/.test(memory[memory.length-1])) && memory.length > 0)?(' x '+obj.value):(obj.value));
	$("#screen").html(memory.join(""));
}


function equalBtn(){
	if (memory.length < 1) return false;


	for (var i=0;i<memory.length;i++) {
		if (memory[i]=="<span class='close-bracket'>)</span>") memory[i]=")";
		if (memory[i]=="<span class='close-bracket'>)</span></sup>") memory[i]=")</sup>";
		}
	
	
	
	_ans=compiler(memory);
	$("#screen").html(money(_ans));
	memory=[];
	memory[0]=_ans;
	$('#ce_btn').html("AC");
	$('#ce_btn').value='ac';
	
	//memory.push(res);
}

function compiler(memory) {
	 var ans,str=memory.toString()
	.replace(/[,]+/g,'')
	.replace(/[x]+/g,'*')
	.replace(/[÷]+/g,'/')
	.replace(/Ans/g,_ans)
	.replace(/sin\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.sin(rad($1))')
	.replace(/cos\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.cos(rad($1))')
	.replace(/tan\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.tan(rad($1))')
	.replace(/sin<sup>-1<\/sup>\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.asin($1)*arad()')
	.replace(/cos<sup>-1<\/sup>\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.acos($1)*arad()')
	.replace(/tan<sup>-1<\/sup>\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.atan($1)*arad()')
	.replace(/log\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.log($1)/Math.log(10)')
	.replace(/ln\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.log($1)')
	.replace(/([0-9πe]+)√\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.pow($2,1/$1)')
	.replace(/√\(([0-9πe\+\-\*\/\!\(\)\%\.\s]+)\)/g,'Math.sqrt($1)')
	.replace(/([\.\/0-9]+)<sup>\(([0-9\.]+)\)<\/sup>/g,'Math.pow($1,$2)')
	.replace(/[π]+/g,'Math.PI')
	.replace(/e<sup>\(([0-9]+)\)<\/sup>/g,'Math.pow(Math.E,$1)')
	.replace(/[e]+/g,'Math.E')
	.replace(/([0-9]+)\!/g,'factorial($1)');
	
	try {
	eval("ans="+str);
	 return ans;
		} catch(err) {
			console.log(err.message);
			return "Syntax Error";
		}
	
}