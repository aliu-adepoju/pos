var date=new Date();
var m=date.getMonth();
var y=date.getFullYear();
daysInMonth=function(year,month){
return new Date(year,month + 1,0).getDate();
};

calendar=function(src,year,month){
var k=1;
if(month && year){
date.setFullYear(year,month,1);
}
else{
date.setFullYear(date.getFullYear(),date.getMonth(),1);
}
month=date.getMonth();
year=date.getFullYear();
days=daysInMonth(year,month);
lastMonthDays=daysInMonth(year,month - 1);
month_arr=["Jan.","Feb.","Mar.","Apr.","May","Jun.","Jul.","Aug.","Sep.","Oct.","Nov.","Dec."];
str='';
str += "<table class='table table-striped rel' style='width:200px'>";
str += "<tr><td colspan='7'><button class='btn btn-xs btn-default' id='"+UID(src)+"m_prev'><i class='glyphicon glyphicon-circle-arrow-left'></i></button>"; 
str += "&nbsp;<select id='"+UID(src)+"select_yr'>";
for(var i=(year-10);i<(year+10);i++){
selected=(i==year)?("selected=true"):(null);
str += "<option "+selected+" value='"+i+"'>"+i+"<\/option>";
}
str += "<\/select><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+month_arr[month];
str += " <button class='btn btn-xs btn-default' id='"+UID(src)+"m_next'><i class='glyphicon glyphicon-circle-arrow-right'></i></button><\/td><\/tr>";
str += "<tr><td>S<\/td><td>M<\/td><td>T<\/td><td>W<\/td><td>T<\/td><td>F<\/td><td>S<\/td><\/tr>";
for(var i=0;i<6;i++){
str += "<tr>";
for(var j=1;j<=7;j++){
cal=(j+(i*7)-date.getDay());
if(cal <= 0){
str += "<td><a href='javascript:void(0)'  id='"+((month==0)?(year-1):(year))+"-"+((month==0)?(12):(zerofill(month)))+"-"+(lastMonthDays+cal)+"' style='color:#E8E8E8'>"+(lastMonthDays+cal)+"<\/a><\/td>";
}
else if(cal > days){
str += "<td><a href='javascript:void(0)' id='"+((month+1==12)?(year+1):(year))+"-"+(((month+2==13))?('01'):(zerofill(month+2)))+"-"+zerofill(k)+"' style='color:#E8E8E8'>"+k+"<\/a><\/td>";
k++;
}
else{
mark=(cal==new Date().getDate())?("background:#f5f5f5"):(null);
str += "<td style='"+mark+"'><a href='javascript:void(0)' id='"+year+"-"+zerofill(month+1)+"-"+zerofill(cal)+"'>"+cal+"<\/a><\/td>";
}
}
str += "<\/tr>";
if (cal >= days && i != 6){
			break;
			}
}
str += "<\/table>";
var obj=document.createElement("div");
obj.className="calendar";
obj.id=UID(src)+"calendar";
obj.style.left=src.offsetLeft+'px';
obj.innerHTML=str;
if(isDefined(getObj(UID(src)+"calendar")) && arguments.length==1){
	src.parentNode.removeChild(getObj(UID(src)+"calendar"));
	return false;
}
else if(isDefined(getObj(UID(src)+"calendar"))){
	src.parentNode.removeChild(getObj(UID(src)+"calendar"));
	src.parentNode.appendChild(obj);
}
else{
	src.parentNode.appendChild(obj);
}

a_arr=obj.getElementsByTagName("a");
for(var $i=0;$i<a_arr.length;$i++){
a_arr[$i].onclick=function(){
src.value=this.id;
src.parentNode.removeChild(getObj(UID(src)+"calendar"));
};
}

getObj(UID(src)+'select_yr').onchange=function(){
	y=this.options[this.selectedIndex].value;
	calendar(src,y,m);
};
getObj(UID(src)+"m_next").onclick=function(){
m++;
if(m > 12){
m=1;
y++;
}
calendar(src,y,m);
};
getObj(UID(src)+"m_prev").onclick=function(){
m--;
if(m < 1){
m=12;
y--;
}
calendar(src,y,m);
};
getObj(UID(src)+"y_next").onclick=function(){
y++;
calendar(src,y,m);
};
getObj(UID(src)+"y_prev").onclick=function(){
y--;
calendar(src,y,m);
};
};