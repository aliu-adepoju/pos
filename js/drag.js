function dragObj(evt,o,func,rangeCoor){
	var obj=o || getElementsByClassName(document,"div","draggable")[0],rc=rangeCoor || [0,0,0,0];
	e=evtype(evt);
	mouseover=true;
	//obj.style.position='fixed';
	pleft=parseInt(obj.style.left);
	ptop=parseInt(obj.style.top);
	  xcoor=e.clientX;
	  ycoor=e.clientY;
	 document.onmousemove=function(e){
		 var x=e.clientX,y=e.clientY;
		 if((pleft+x-xcoor) >= rc[0] && ((pleft+x-xcoor+obj.offsetWidth+rc[1]) <= window.innerWidth)){// && window.offsetWidth
			  obj.style.left=pleft+x-xcoor+"px";
			  }
			  
			  if((ptop+y-ycoor) >= rc[2] && ((ptop+y-ycoor+obj.offsetHeight+rc[3]) <= window.innerHeight)){
			  obj.style.top=ptop+y-ycoor+"px";
			  }
		 if(isDefined(func)) func(pleft+x-xcoor,ptop+y-ycoor,obj);
	     return false;
	 	};
	 return false;
	}
	document.onmouseup=function(e){document.onmousemove=null;};
