function validateEmail(email){ 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

function validatePhone(num){
	  var re=/^((234(?!0))|(0))(?!0)[0-9]{10}$/;
	    return re.test(num);
}

function validatePassword(obj){
	var formObj=obj.form;
	if(formObj['pwd1'].value.isEmpty() || formObj['pwd2'].value.isEmpty()){
		formObj[obj.name].parentNode.getObjN("small")[0].setHtml("");
		return false;
	}
	
	if(obj.value.trim().length < 4){
		formObj[obj.name].parentNode.getObjN("small")[0].setHtml("<img src='../iconx/error.png'/>&nbsp;<small class='fg-color-red'>Password too short, minimum of 4 characters</small>");
		formObj[obj.name].isNotFilled();
		return false;
	}
	
	if(formObj['pwd1'].value != formObj['pwd2'].value && (!formObj['pwd1'].value.isEmpty() && !formObj['pwd2'].value.isEmpty())){
			formObj[obj.name].parentNode.getObjN("small")[0].setHtml("<img src='../iconx/error.png'/>&nbsp;<small class='fg-color-red'>Password not match</small>");
			formObj[obj.name].isNotFilled();
			return false;
		}
		else{
			if(!formObj['pwd1'].value.isEmpty() && !formObj['pwd2'].value.isEmpty()){
				formObj['pwd1'].parentNode.getObjN("small")[0].innerHTML=formObj['pwd2'].parentNode.getObjN("small")[0].innerHTML="<img src='../iconx/ok.png'/>";
				formObj[obj.name].isFilled();	
			}
			return true;
		}
	
}

function validateUrl(url){ 
    var re=/^(http(s)?:\/\/)?(www.)?[a-z0-9]+\.[a-z0-9\.(\/)?]+$/i;
    return re.test(url);
}
