addEvent(window,'load',function(){
    	thread=ajaxThread();
    	loading=new Image();
    	//loading.src="../iconx/loading.gif";
    	if (window.document.readyState=="complete"){
    		//adjustAds();
    	}
});




function setProductIcon(src){
	var box=$('#product_icon_box'),thumbnail=box.find('a')[0];
	box.find('img').attr("src",src);
	box.find('a').on("click",function(){
		ImageStore.Web.select(box,ImageStore.n);
	});
	
	box.find('a').click();
}

			var Stock={
					add:function(formObj){
						$("#err").show();
						if(formObj["manufacturer"].value.isEmpty()){
							$("#err").html("Select product manufacturer");
							return false;
						}
						if(formObj["category"].value.isEmpty()){
							$("#err").html("Select product category");
							return false;
						}
						if(formObj["product"].value.isEmpty()){
							$("#err").html("Select product name");
							return false;
						}
						if(formObj["supplier"].value.isEmpty()){
							$("#err").html("Add product supplier");
							return false;
						}
						
						$("#err").hide();
						formObj["icon"].value=$("#product_img").attr("src");
						$('#stock_box').disable();
						formObj.action="controllers/__addStock.php";
						formObj.target=window.frames['addStockFrame'].name;
					},
					success:function(){
						window.location="../stock.php?p=add&response=Successfully added";
					}
					,
					error:function(response){
						$("#err").show();
						$("#err").text(response);
					},Multiple:{data:[],icon:trigger(function(data){
					
						var form=data[0],n=data[1];
						  if(!Stock.Multiple.data[n][1].isEmpty() && !Stock.Multiple.data[n][2].isEmpty() && !Stock.Multiple.data[n][3].isEmpty()){
								thread(["view/stock/icon.php",$.param({"manufacturer":Stock.Multiple.data[n][1],"product":Stock.Multiple.data[n][2],"category":Stock.Multiple.data[n][3]})],function(){},function(xhr){
									if(!xhr.responseText.isEmpty()){
									$($("img.product-img").get(n)).attr("src","view/upload/thumbnail/"+xhr.responseText);
									$("form.stock-form")[0]["icon["+n+"]"].value="view/upload/thumbnail/"+xhr.responseText;
									Stock.Multiple.update($("form.stock-form")[0],n);
								}
							},function(){});
							
						}
					},5),
						add:function(){
							this.data.push(["img/burger1.png","","","",[],"0.00","0.00",1]);
							this.model();
						},remove:function(n){
							this.data.splice(n,1);
							this.model();
						},calc:function(form,n){
							this.update(form,n);
							var totalSupplyCost=[];
							for(var i=0;i<this.data.length;i++) totalSupplyCost.push(this.data[i][5]*this.data[i][7]);
							$("#total_supply_cost").text(money(totalSupplyCost.array_sum()));
						},update:function(form,n){
							this.data[n]=[form["icon["+n+"]"].value,form["manufacturer["+n+"]"].value,form["product["+n+"]"].value,form["category["+n+"]"].value,[],form["cprice["+n+"]"].value,form["sprice["+n+"]"].value,form["qty["+n+"]"].value];
							
						},model:function(){
							var str=[],totalSupplyCost=[];
							for(var i=0;i<this.data.length;i++){
								totalSupplyCost.push(this.data[i][5]*this.data[i][7]);
								str.push(template($("#multiple_stock_tmpl").find("tr")[0].outerHTML,{"i":i,"sn":i+1,"icon":this.data[i][0],"manufacturer":this.data[i][1],"product":this.data[i][2],"category":this.data[i][3],"productCode":this.data[i][4],"cprice":this.data[i][5],"sprice":this.data[i][6],"qty":this.data[i][7]}));
								this.ProductCode.model(i);
							}
							$("#total_supply_cost").text(money(totalSupplyCost.array_sum()));
							$("table.add-multiple-stock").find("tbody").html(str.join("\n"));
						},ProductCode:{data:{},
							box:function(obj,n){
								this.close();
								this.obj=obj;
								this.getControl=$(obj.parentNode).find("div").filter(function(index){return $(this).attr("data-dialog")=='productcode';})[0];
								$(this.getControl).show();
								this.model(n);
								},save:function(n){
									this.data[n]=$(this.getControl).getFormData("text");
								},close:function(){
									if(isDefined(this.getControl)) $(this.getControl).hide();
								},clear:function(n){
									this.data[n]=array_repeat($('form.stock-form')[0]["qty"].value,'');
									this.model(n);
								},model:function(n){
									var arr=[];
									for(var i=0;i<Stock.Multiple.data[n][7];i++) arr.push(template("<input type='text' name='codes[%n%][]' class='form-control' onmouseover='this.focus()' onchange='Stock.Multiple.ProductCode.save(%n%)' value='%code%'/>",{"n":n,"code":(isDefined(this.data[n]) && i < this.data[n].length)?(this.data[n][i]):('')}));
									$(this.getControl).find("div").filter(function(){ return this.hasAttribute("data-code-input");}).html(arr.join("\n"));
								}
								
							},validate:function(form){
								try {
									if(form["supplier"].value.isEmpty()) throw "Add items supplier";
									for(var i=0;i<this.data.length;i++){
										if(this.data[i][1].isEmpty()) throw template("<strong>Line %line%:</strong> Add manufacturer",{"line":i+1});
										if(this.data[i][2].isEmpty()) throw template("<strong>Line %line%:</strong> Add category",{"line":i+1});
										if(this.data[i][2].isEmpty()) throw template("<strong>Line %line%:</strong> Add product name",{"line":i+1});
									}
								} catch (e) {
									$("#err").show().html(e);
									form.action="javascript:void(0)";
									return false;
								}
								return true;
							},submit:function(form){
								if(this.validate(form) !== true) return false;
								$("#err").hide();
								$(form).disable();
								form.action="controllers/__addMultipleStock.php";
								form.target=window.frames['addStockFrame'].name;
							},success:function(id){
								window.location="../stock.php?p=multiple&response=Successfully added";
							},error:function(res){
								$("#err").show().html(res);
								form.action="javascript:void(0)";
							}		
						},ProductCode:{data:{},
						box:function(obj){
							this.close();
							this.obj=obj;
							this.getControl=$(obj.parentNode).find("div").filter(function(index){return $(this).attr("data-dialog")=='productcode';})[0];
							$(this.getControl).show();
							this.model();
							},save:function(){
								this.data=$(this.getControl).getFormData("text");
							},close:function(){
								if(isDefined(this.getControl)) $(this.getControl).hide();
							},clear:function(n){
								this.data=array_repeat($('form.stock-form')[0]["qty"].value,'');
								this.model();
							},model:function(){
								var arr=[];
								for(var i=0;i<$('form.stock-form')[0]["qty"].value;i++) arr.push(template("<input type='text' name='codes[]' class='form-control' onmouseover='this.focus()' onchange='Stock.ProductCode.save()' value='%code%'/>",{"code":(isDefined(this.data) && i < this.data.length)?(this.data[i]):('')}));
								$(this.getControl).find("div").filter(function(){ return this.hasAttribute("data-code-input");}).html(arr.join("\n"));
							}
							
						},Icon:{
							load:function(formElem){
								this.form=formElem;
								var fields=$(formElem).find(":input[name='manufacturer'],:input[name='category'],:input[name='product']").serializeArray();
							    if(!formElem["manufacturer"].value.isEmpty() && !formElem["category"].value.isEmpty() && !formElem["product"].value.isEmpty()){
									thread(["view/stock/icon.php",$.param(fields)],function(){},function(xhr){
										if(!xhr.responseText.isEmpty()){
											getObj('product_img').src="view/upload/thumbnail/"+xhr.responseText;
											getObj("icon_name_holder").value="view/upload/thumbnail/"+xhr.responseText;
										}
									},function(){});
									
								}
							}
						}
					
			};

			
			var Cart={sn:0,
					data:{},stockID:[],
					calc:function(n,qty){
						this.data[n][3]=qty || this.data[n][3];
						var subtotal=[],QTY=[],dtotal=[];
						for(var k in this.data){
							if(this.data.hasOwnProperty(k)){
								QTY.push(this.data[k][3]);
								subtotal.push(this.data[k][2]*this.data[k][3]);
								dtotal.push((this.Discount.each[k]*this.data[k][3])+(((parseInt(this.Discount.global)*this.data[k][2])/100)*this.data[k][3]));
							}
						}
						this.totalQty=QTY.array_sum();
						this.total=subtotal.array_sum()-dtotal.array_sum();
						$('#total_qty').html((QTY.length > 1)?("("+QTY.array_sum()+" items)"):("("+this.totalQty+" item)"));
						$('#subtotal_'+n).html((parseInt(this.Discount.each[n]) !=0 || parseInt(this.Discount.global) !=0)?(template("<span class='label label-warning'>₦%DPR%</span> <span class='strike fg-color-red'>₦%RPR%</span>",{'DPR':money((this.data[n][2]-parseInt(Math.abs(this.Discount.each[n]))-((parseInt(this.Discount.global)*this.data[n][2])/100))*this.data[n][3]),'RPR':money(this.data[n][2]*this.data[n][3])})):("<span class='label label-warning'>₦"+money(this.data[n][2]*this.data[n][3])+"</span>"));//"₦"+money(this.data[n][2]*this.data[n][3]);
						$("#total_box").html(template("<div class='col-xs-7'><h5>Sub-Total &raquo; <strong>₦%st%</strong></h5><h5>Discounts &raquo; <strong>₦%td%</strong></h5></div><div class='col-xs-5 padding5'><h3 class='no-margin'>Total <br/> <strong class='digit fg-color-success'>₦%gt%</strong></h3></div>",{"st":money(subtotal.array_sum()),"td":money(dtotal.array_sum()),"gt":money(this.total)}));
					},
					items:function(){
						var isCartEmpty=true,data=[],stockID;
						$('#cart_box').empty('');
						data=json2array(this.data);
						data.sort(function(a, b) {
					        var x = a[a.length-1]; var y = b[b.length-1];
					        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
					    });
						stockID=[];
						for(var k=0;k<data.length;k++){
								 stockID.push(data[k][0]);
								 isCartEmpty=false;
								 $('#cart_box').append(template($('#tmpl').html(),{n:data[k][0],pname:data[k][1],sprice:data[k][2],qty:data[k][3],subtotal:(data[k][2]*data[k][3]),edis:money(this.Discount.each[data[k][0]]),hasSN:data[k][4]}));
								 this.calc(data[k][0]);	
						}
						this.stockID=stockID;
						this.check();
						
						if(isCartEmpty){
							$('#cart_box').html("<div class='text-center'><i class='icon-empty-cart fs250'></i><br/><h1><small>Empty cart</small></h1></div>");
						}
						$('#suspended_sales').hide();
						$('#shopping_cart').show();
						},
					add:function(data){
						if(!isDefined(this.data[data[0]])){
							data.push(this.sn+1);
							this.Discount.each[data[0]]=0;
							this.data[data[0]]=data;
							this.sn +=1;
						}
						else{
							delete this.data[data[0]];
						}
						this.items();
					},
					remove:function(n){
						delete this.Discount.each[n];
						delete this.data[n];
						this.items();
					},
					cancel:function(){
						this.data={};
						this.items();
					},move:function(n){
						this.data[n][this.data[n].length-1]=this.sn +=1;
					    this.items();
					},check:function(){
						var itemBox=$(document.body).find("div").filter(function(index){return this.hasAttribute("data-stockID");});
						for(var i=0;i<itemBox.length;i++){
							$('#item_box').find("div").filter(function(){ return this.hasAttribute('data-search');}).show();
							if(Cart.stockID.in_array(itemBox[i].getAttribute("data-stockID"))) 
								$(itemBox[i]).show();
							else 
								$(itemBox[i]).hide();
						}
					},Preview:{
						show:function(data){
							//alert(JSON.stringify(data));
							$("#cart_preview").find("span").text(data["mname"]+" "+data["pname"]);
							$("#cart_preview").find("img").attr("src","view/upload/large/"+data["iname"]);
							$("#item_edit").attr("href","stock.php?edit="+data["stockID"]);
							$("#cart_preview").find("tbody").html(template("<tr><td>Manufacturer</td><td>%mname%</td></tr><tr><td>Category</td><td>%cname%</td></tr><tr><td>Product</td><td>%pname%</td></tr><tr><td>Quantity</td><td>%qty%</td></tr><tr><td>Sale price</td><td>%sprice%</td></tr><tr><td>Cost price</td><td>%cprice%</td></tr>",data));
							lock(true);
							$("#cart_preview").show();
							$("#cart_preview").center();
							
					},
					hide:function(){
						lock();
						$("#cart_preview").hide();
						if(window.innerWidth > 980) window.document.body.style.overflowY='hidden';
					}
			},Discount:{each:{},global:0,
						setEach:function(n,v){
							this.each[n]=Math.abs(v);
							Cart.calc(n);
						},setGlobal:function(v){
							this.global=v;
							for(var n in Cart.data) Cart.calc(n);
						}
						
					},SerialNo:{data:{},
						box:function(n,obj){
							this.close();
							this.obj=obj;
							this.getControl=$(obj.parentNode).find("div").filter(function(index){return this.hasAttribute("data-code");})[0];
							$(this.getControl).show();
							this.model(n);
						},apply:function(n){
							this.data[n]=$(this.getControl).getFormData("text");
							if(this.data[n].array_trim().length == 0) Cart.data[n][4]="btn btn-xs fg-color-lBlack";
							if(this.data[n].array_trim().length != 0 && this.data[n].array_trim().length < this.data[n].length) Cart.data[n][4]="btn btn-xs fg-color-danger";
							if(this.data[n].array_trim().length > 0 && this.data[n].array_trim().length == this.data[n].length) Cart.data[n][4]="btn btn-xs fg-color-success";
							$(this.obj).attr("class",Cart.data[n][4]);
							this.close();
						},close:function(){
							if(isDefined(this.getControl)) $(this.getControl).hide();
						},clear:function(n){
							this.data[n]=array_repeat(Cart.data[n][3],'');
							this.model(n);
						},model:function(n){
							for(var i=0,str='';i<Cart.data[n][3];i++) str +=template("<input type='text' class='form-control' onmouseover='this.focus()' value='%code%'/>",{"code":(isDefined(this.data[n]) && i < this.data[n].length)?(this.data[n][i]):('')});
							$(this.getControl).find("div").filter(function(){ return this.hasAttribute("data-code-input");}).html(str);
						}
						
					},QTY:{
						incr:function(n,obj){
							var q,getControl=$(obj.parentNode).find("div").filter(function(index){return this.hasAttribute("data-qty");})[0];
							q=parseInt($(getControl).text())+1;
							$(getControl).text(q);
							Cart.calc(n,q);
						},
						decr:function(n,obj){
							var q,getControl=$(obj.parentNode).find("div").filter(function(index){return this.hasAttribute("data-qty");})[0];
							q=parseInt($(getControl).text())-1;
							$(getControl).text(q);
							Cart.calc(n,q);
						}
					},Customer:{data:[],suggest:trigger(function(data){
						thread(["view/cart/suggest.customer.php","data="+data[0]],function(){},function(e,dest){
							var data=$.parseJSON(e.responseText);
							$(dest).find("ul").empty();
							for(var i=0;i<data.length;i++) $(dest).find("ul").append(template("<li><a href='javascript:void(0)' onclick=\"Cart.Customer.select(['%name%','%email%','%phone%','%addr%']);$('#%id%').hide()\">%name%</a></li>",{"id":dest.id,"name":data[i]["name"],"email":data[i]["email"],"phone":data[i]["phone"],"addr":data[i]["addr"]}));
						},function(){},data[1]);
						
					},2),open:function(arg){
						if(arg===true) Cart.Customer.update();
						$('#_customer').show();
						
					},close:function(){
						$('#_customer').hide();
					},toggle:function(arg){
						if(arg===true) Cart.Customer.update();
						toggle('_cust',function(){$('#_customer').show();},function(){$('#_customer').hide();});
					},
						search:function(evt,obj,dropdown){
							this.formObj=obj.form;
							if(obj.value.isEmpty()){
								$(dropdown).hide();
								return false;
							}
							$(dropdown).show();
							$(dropdown).find("ul").html("<li><a href='javascript:void(0)'>Searching...</a></li>");
							onMenu(evt,dropdown,function(){});
							this.suggest(obj.value,dropdown);
						},update:function(){
							this.formObj=$('form.customer-form')[0];
							this.data=[this.formObj['name'].value,this.formObj['email'].value,this.formObj['phone'].value,this.formObj['addr'].value];
						},select:function(data){
							this.formObj=(isDefined(this.formObj))?(this.formObj):($('form.customer-form')[0]);
							for(var i=0,arr=["name","email","phone","addr"];i<arr.length;i++) this.formObj[arr[i]].value=(data[i]=='null')?(''):(data[i]);
							this.data=data;
						}
					},Suspend:{data:[],
						add:function(){
								if(Cart.Customer.data.array_trim().length==0) {
									Cart.Customer.open(true);
								    return false;
								}
								var mktime=new Date();
								this.data.push({"items":Cart.data,"discount":{"each":Cart.Discount.each,"global":Cart.Discount.global},"serialNo":Cart.SerialNo.data,"customer":Cart.Customer.data,"qty":Cart.totalQty,"date":mktime.getTime()});
								this.data.sort(function(a, b) {
							        var x = a['date']; var y = b['date'];
							        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
							    });
								this.store(this.data);
								this.model();
								//alert(JSON.stringify(Base64.decode(getCookie("suspendDB"))));
							},remove:function(n,refresh){
								this.data.splice(n,1);
								this.store(this.data);
								if(isDefined(refresh) && refresh==true) {
									$('#suspend_count').text(this.count());
									return false;
								}
									this.model();
							},store:function(data){
								var today=new Date(),expire=new Date();
								expire.setTime(today.getTime()+3600000*24*365);
								setCookie("suspendDB",Base64.encode(JSON.stringify(data)),expire);
							},load:function(n){
								Cart.data=this.data[n]["items"];
								Cart.Discount.each=this.data[n]["discount"]["each"];
								Cart.Discount.global=this.data[n]["discount"]["global"];
								Cart.SerialNo.data=this.data[n]["serialNo"];
								Cart.Customer.select(Cart.Customer.data=this.data[n]["customer"]);
								Cart.items();
								this.remove(n,true);
								toggleReset('_cust');
							},count:function(){
								if(!isDefined(getCookie("suspendDB"))) return 0;
								this.data=JSON.parse(Base64.decode(getCookie("suspendDB")));
								return this.data.length;
							},model:function(){
								Cart.Customer.close();
								var mktime=new Date();
								$('#suspended_sales').show();
								$('#shopping_cart').hide();
								$('#suspend_tb').find("tbody").empty();
								for(var x in this.data){
									mktime.setTime(this.data[x]["date"]);
									if(this.data.hasOwnProperty(x))
										$('#suspend_tb').find("tbody").append(template("<tr><td>%sn%</td><td><a href='javascript:void(0)' onclick='Cart.Suspend.load(%i%)'>%name%</a> <span class='badge pull-right'>%qty%</span></td><td>%date% <a href='#' class='close' onclick='Cart.Suspend.remove(%i%)' title='Remove'>&times;</a></td></tr>",{"sn":parseInt(x)+1,"i":x,"name":this.data[x]["customer"][0],"qty":this.data[x]["qty"],"date":mktime.format('D, j/m')}));
									
								}
								$('#suspend_tb').dataTable({"paging":false,"info":false});
								$('#suspend_count').text(this.count());
							},close:function(){
								toggleReset('_cust');
								$('#suspended_sales').hide();
								$('#shopping_cart').show();
								Cart.Customer.close();
							}
					},BackOrder:{data:{},
						process:function(formObj){
						this.formObj=formObj;
						if(Cart.Customer.data.array_trim().length==0) {
							Cart.Customer.open(true);
							return false;
						}
						this.data={"items":Cart.data,"discount":{"each":Cart.Discount.each,"global":Cart.Discount.global},"serialNo":Cart.SerialNo.data,"customer":Cart.Customer.data,"print":0};
						this.formObj["data"].value=JSON.stringify(this.data);
						this.formObj.submit();
						this.formObj.action="controllers/__checkout.php?task=backorder";
						this.formObj.target=getObj('checkout_frame').name;
						
						},
						error:function(err){
							this.formObj.action="javascript:void(0)";
							//$('#process_box').enable();
							//$('#checkout_err').innerHTML="<div class='alert alert-danger padding5 no-margin'>"+err+"</div>";
						},
						success:function(invoice){
							window.location="../pos.php?trans="+invoice;
						}
						
					},Checkout:{
							process:function(){
								if(Cart.Customer.data.array_trim().length==0) {
									Cart.Customer.open(true);
									return false;
								}
								Cart.Payment.show();
							}
					},Payment:{formObj:null,print:true,data:{},
							show:function(){
								Cart.Customer.close();
								this.data={"items":Cart.data,"discount":{"each":Cart.Discount.each,"global":Cart.Discount.global},"serialNo":Cart.SerialNo.data,"customer":Cart.Customer.data,"print":this.print};
								this.box=$(document.body).find("div").filter(function(){ return this.hasAttribute("data-commit-sale");});
								$(this.box).show();
								$(this.box).find("input").filter(function(){ return this.name=='total';}).val('₦'+money(Cart.total));
								this.data["paid"]=Cart.total;
								this.write();
							},hide:function(){
								toggleReset('_cust');
								$(this.box).hide();	
							},change:function(paid){
								$(this.box).find("input").filter(function(){ return this.name=='change';}).val('₦'+money((paid > Cart.total)?(paid-Cart.total):(0)));
								this.data["paid"]=paid;
								this.write();
							},write:function(){
								$(this.box).find("input").filter(function(){ return this.name=='data';}).val(JSON.stringify(this.data));
								return true;
							},receipt:function(bool){
								this.data["print"]=bool;
								this.write();
							},
							process:function(formObj){
								this.formObj=formObj;
								this.formObj.submit();
								this.formObj.action="controllers/__checkout.php";
								this.formObj.target=getObj('checkout_frame').name;
								
								//$('#process_box').disable();
							},
							error:function(err){
								this.formObj.action="javascript:void(0)";
								//$('#process_box').enable();
								//$('#checkout_err').innerHTML="<div class='alert alert-danger padding5 no-margin'>"+err+"</div>";
							},
							success:function(invoice,reload){
								if(reload===0) return false
								window.location="../pos.php?trans="+invoice;
							}
							
					}
			};
			
					var NumberPicker={
					memory:[],pick:function(n,num,elem){
						if(!isDefined(this.memory[n])) this.memory[n]=[];
						this.memory[n].push(num);
						if(isDefined(elem)) {
							var paid=this.memory[n].join("");
							$(elem).html(money(paid));
							Cart.Payment.change(paid);
							return false;
						}
						var q=this.memory[n].join("");
						$('#item_box_'+n).find('div').filter(function(){ return this.hasAttribute("data-qty");}).text(q);
						Cart.calc(n,parseInt(q));
					},
					clearEntry:function(n,elem){
						if(!isDefined(this.memory[n])) this.memory[n]=[];
						this.memory[n].pop();
						if(isDefined(elem)) {
							var paid=(this.memory[n].length==0)?(0):(this.memory[n].join(""));
							$(elem).html(money(paid));
							Cart.Payment.change(paid);
							return false;
						}
						var q=(this.memory[n].length==0)?(1):(this.memory[n].join(""));
						$('#item_box_'+n).find('div').filter(function(){ return this.hasAttribute("data-qty");}).text(q);
						Cart.calc(n,parseInt(q));
					}
			};
					
					
					var ProcessCart={
							open:function(box){
								toggle(box.id,function(elem){
									elem.show();
									ProcessCart.fetch();
								},function(elem){elem.hide();},box);
								
							},fetch:function(){
								var arr=json2array(Cart.data),data=[];
								for(var i=0;i<arr.length;i++){
									data.push(arr[i][5]+","+arr[i][3]);
								}
								//alert(Cart.data.toJSONString());
								 thread(["view/items_detail.php",data.ajax_params()+"&hasCodes="+$('#hasCodes').checked],function(){},function(e){
									 $('#items_detail').innerHTML=e.responseText;
								 },function(e){});
							}
					};
					
					
					var Tab={
							toggle:function(liObj){
								var li,contents;
								li=$(liObj.parentNode).find("li").filter(function(){ return this.hasAttribute("data-tab");});
								contents=$(document.body).find("div").filter(function(){ return this.hasAttribute("data-content");});
								for(var i=0;i<li.length;i++){
									if(li[i].getAttribute("data-tab")==liObj.getAttribute("data-tab")){
										$(li[i]).addClass("active");
										$(contents[i]).removeClass("hide2");
									}else{
										$(li[i]).removeClass("active");
										$(contents[i]).addClass("hide2");
									}
								}
							}
					};
					
					function searchDropdownCategory(v){
						(v.isEmpty())?($('#search_label').text("Category")):($('#search_label').text(v));
						$('#search_strong_keyword').val(v);
						//SearchList.config({"delay":0});
						SearchList.filter("",Cart.check);
					}
					
					function inCart(){
						searchDropdownCategory('');
						$('#search_label').text("Items In Cart");
						 var box=$('#item_box').find("div").filter(function(){ return this.hasAttribute('data-search');});
						 for(var i=0;i<box.length;i++){
							 if(!Cart.stockID.in_array($(box[i]).find("div").filter(function(){ return this.hasAttribute('data-stockID');}).attr("data-stockID"))){
								 $(box[i]).hide();
							 }
						 }
					}
					
					
					var DList={data:[],suggest:trigger(function(data){
						thread(["controllers/__DList.php?tb="+data[1].id,"data="+data[0]],function(){},function(e,dest){
							var data=$.parseJSON(e.responseText);
							$(dest).find("ul").empty();
							for(var i=0;i<data.length;i++) $(dest).find("ul").append(template("<li><a href='javascript:void(0)' onclick=\"DList.select('%id%','%data%');$('#%id%').hide()\">%name%</a></li>",{"id":dest.id,"name":data[i]["name"],"data":Base64.encode(JSON.stringify(data[i]))}));
						},function(){},data[1]);
						
					},2),show:function(box){
						toggle(box.id,function(o){$(o).show();},function(o){$(o).hide();},box);
					},
						search:function(evt,obj,dropdown,n){
							this.n=n;
							this.formObj=obj.form;
							if(obj.value.isEmpty()){
								$(dropdown).hide();
								return false;
							}
							$(dropdown).show();
							$(dropdown).find("ul").html("<li><a href='javascript:void(0)'>Searching...</a></li>");
							onMenu(evt,dropdown,function(){});
							this.suggest(obj.value,dropdown);
						},update:function(){
							this.formObj=(isDefined(this.formObj))?(this.formObj):($('form.stock-form')[0]);
							this.data=[this.formObj['name'].value,this.formObj['email'].value,this.formObj['phone'].value,this.formObj['addr'].value];
						},select:function(key,data){
							if(isDefined(this.n)){
								var arr=JSON.parse(Base64.decode(data)),targetInput,inputs={"manufacturer_dlist":[["manufacturer"],["name"]],"category_dlist":[["category"],["name"]],"product_dlist":[["product"],["name"]]};
								this.formObj[key.split("_")[0]+"["+this.n+"]"].value=arr["name"];	
								Stock.Multiple.update(this.formObj,this.n);
								Stock.Multiple.icon(this.formObj,this.n);
							}
							else{
								var arr=JSON.parse(Base64.decode(data)),inputs={"manufacturer_dlist":[["manufacturer"],["name"]],"category_dlist":[["category"],["name"]],"product_dlist":[["product"],["name"]],"supplier_dlist":[["supplier","email","phone","addr"],["name","email","phone","addr"]]};
								for(var i=0;i<inputs[key][0].length;i++){
									this.formObj[inputs[key][0][i]].value=arr[inputs[key][1][i]];
								}
							}
						}
					};
					
					var ImageStore={
						show:function(data,n){
						this.n=n;
						$("#image_store").show();
						formObj=$("form.stock-form")[0];
						thread(["view/related_images.php",$.param(data)],function(e){},function(e){
							var imgs=JSON.parse(e.responseText),str=[];
							for(var i=0;i<imgs.length;i++){
								str.push(template("<div class='col-lg-3 col-md-4 col-xs-6 thumb'><a class='thumbnail' href='javascript:void(0)' data-src='%src%' onclick=\"ImageStore.Web.select(this,%n%)\"><img src='%src%' alt=''></a></div>",{"i":i,"n":n,"src":"view/upload/thumbnail/"+imgs[i][0]}));
							}
							$("#related_thumbnails").html(str.length > 0?str.join(""):"<small>No image found</small>");
						},function(e){});
					},hide:function(){
						$("#image_store").hide();
					},
							Web:{index:1,
								search:function(keyword){
									if(keyword.isEmpty()) return false;
									$('#image_store_web').disable();
									if(isDefined(this.keyword) && this.keyword !=keyword){
										this.index=1;
										$("#image_search_pagination").find("li").each(function(index){
											if(index==0) $(this).addClass("active");
											else $(this).removeClass("active");
										});
									}
									this.keyword=keyword;
									var api="https://www.googleapis.com/customsearch/v1?q="+encodeURI(this.keyword)+"&imgSize=large&searchType=image&start="+this.index+"&cx=001919984404042786376:qqcfqr__yl8&key=AIzaSyAG8ESrAWjtBs6taJo82Lew2lD9fZjSp94";
									thread([api],function(e){},function(e){
										var data=JSON.parse(e.responseText)["items"],imgs=[],str=[];
										for(var i=0;i<data.length;i++){
											imgs.push([data[i]["image"]["thumbnailLink"],data[i]["link"]]);
										}
										
										for(var i=0;i<imgs.length;i++){
											str.push(template("<div class='col-lg-3 col-md-4 col-xs-6 thumb'><a class='thumbnail' href='javascript:void(0)' data-src='%src%' onclick=\"ImageStore.Web.select(this)\"><img src='%src%' alt='' style='width:100px;height:100px'></a><div class='clear'></div></div>",{"i":i,"src":imgs[i][1]}));
										}
										$("#search_thumbnails").html(str.join(""));
									//	alert(JSON.stringify(items));
										$("#image_search_pagination").show();
										$('#image_store_web').enable();
									},function(e){});
								},pagination:function(n){
									this.index=(n*10)+1;
									this.search(this.keyword);
									//$("#image_search_pagination").show();
									$("#image_search_pagination").find("li").each(function(index){
										if(index==n) $(this).addClass("active");
										else $(this).removeClass("active");
									});
								},select:function(obj,n){
										var src=decodeURI($(obj).find("img")[0].src);
										if(isDefined(ImageStore.n)){
											$($("img.product-img").get(ImageStore.n)).attr("src",src);
											$("form.stock-form")[0]["icon["+ImageStore.n+"]"].value=src;
											Stock.Multiple.update($("form.stock-form")[0],ImageStore.n);
										}
										else{
											$("#product_img").attr("src",src);
										}
										$("#image_store").find("a.thumbnail").each(function(index){
										if(src.indexOf(pathinfo(decodeURI($(this).find("img").attr('src')))["filename"]) != -1) $(this).addClass("bluB");
										else $(this).removeClass("bluB");
									});
								}
							},Synchronize:{data:[],init:false,
								fetch:function(){
									thread(["sync/fetch.php",$.param({data:this.data})],function(){},function(xhr){
									var imgs=JSON.parse(xhr.responseText);
										for(var i=0;i<imgs.length;i++){
											$("#syn_thumbnails").prepend(template("<div class='col-xs-3 thumb'><a class='thumbnail rel' href='javascript:void(0)' data-src='%src%' onclick=\"ImageStore.Web.select(this)\"><img src='%src%' alt='' style='width:100px;height:100px'><button class='btn btn-xs btn-default absBR' style='bottom:-1px;right:-1px' onclick=\"Crop.show('%src%')\"><i class='glyphicon glyphicon-resize-small'></i></button></a></div>",{"i":i,"src":"sync/large/"+imgs[i]}));
											ImageStore.Synchronize.data.push(imgs[i]);
										}
									
									setTimeout(function(){ImageStore.Synchronize.fetch();},5000);
									},function(){});
								},start:function(){
									if(this.init==false){
										this.init=true;
										this.fetch();
									}
								}
							}
					};
					
					var Crop={
						show:function(img){
							lock(true);
							$('#dialog').show();
							$('#dialog').center();
							getObj("imgCropFrame").src="widget/imgcrop/upload.php?img="+img;
						}
					};
					
					
					var Synchronize={
						start:function(date,valuation){
						Connection.run(function(){
							thread(["store/fetch.php",$.param({'date':date})],function(xhr){Notification.set("fetchSales");},function(xhr){
								Notification.set("fetchSales");
								//http://www.pointandbuy.com.ng
								if(JSON.parse(xhr.responseText).length == 0) return false;
								thread(["store/sync.php",$.param({'data':xhr.responseText,'valuation':valuation,'date':date})],function(xhr){Notification.set("synSales");},function(xhr){
								//alert(xhr.responseText);
									Notification.set("synchronized");
									setTimeout(function(){Notification.hide();},5000);
								},function(xhr){
									//alert(xhr.responseText);
								});
							},function(xhr){});
							});
							}
					};
					 