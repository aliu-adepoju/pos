<?php
ini_set("display_errors",1);
session_start();
session_regenerate_id();
include_once 'layout.php';
include_once 'controllers/__rstore.php';
include_once 'lib/Time.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="";
$layout->content("store/_toolbar.php","store/_body.php");