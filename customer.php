<?php
session_start();
session_regenerate_id(); 
if(!isset($_SESSION['usrID'])){
	header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'controllers/__customer.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="";
switch (@$_GET["p"]) {
	case "add":
	case "edit":
		$layout->content(NULL,"view/customer/_edit.php");
	break;
	default:
		$layout->content("view/customer/_toolbar.php","view/customer/_body.php");
	break;
}
?>  