<?php 
ini_set("display_errors",1);
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Simple POS</title>
    <link href="css/styles.css" rel="stylesheet">    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <script src="js/core.js" type="text/javascript"></script>
      <script src="js/ds_a.js" type="text/javascript"></script>
     <script src="js/pi.js" type="text/javascript"></script>
      <script src="js/json.js" type="text/javascript"></script>
      <script src="js/imageUpload.js" type="text/javascript"></script>
      <script src="js/validator.js" type="text/javascript"></script>
     <script src="js/inventory.js" type="text/javascript"></script>
  </head>
  <body>
     <div class="row">
              <div class="col-md-12 no-padding">  
              <div id="sidebar-wrapper">
      
        <ul class="sidebar-nav" id="sidebar">    
        <li><a><span class="glyphicon glyphicon-user"></span></a></li>
         <li><a><span class="glyphicon glyphicon-cog"></span></a></li>
          <li title="Add New Stock"><a href="javascript:void(0)" onclick="toggle('add_stock_box',function(e){e.show();},function(e){e.hide();},getObj('add_stock_box'))"><span class="glyphicon glyphicon-plus"></span></a></li>
          <li><a><span class="glyphicon glyphicon-th"></span></a></li>
           <li><a><span class="glyphicon glyphicon-info-sign"></span></a></li>
        </ul>
      </div>            
              <section style="margin-left:50px">
              <div class="row"> 
              <div class="col-md-12  ae-tab no-padding">
              
              <ul class="no-margin no-padding">
  <li class="active"><a href="#">In-stock</a></li>
  <li><a href="#">Sold</a></li>
  <li><a href="#">Store</a></li>
  <li><a href="#">Customers</a></li>
  <li><a href="#">Suppliers</a></li>
   <li><a href="#">Staff</a></li>
  <li class="pull-right"><img alt="loading" src="iconx/loading.gif">&nbsp;</li>
</ul>
              </div>
             
     
              </div>
              </section> 

            </div>
            
          </div>
    
<?php 
include 'view/add_stock.php';
?>
   <div class="hide" id="tmpl">
   <div class='item-box rel' id='item_box_%n%'>
   <div class='row'>
   <div class='col-md-12 no-padding'>
   <a href='javascript:void(0)' onclick='Cart.remove(%n%)' class='close'>&times;</a>
   <span class='label label-default'>%cname%</span>&nbsp;<span>%pname%</span>
   <span id='subtotal_%n%'>₦%subtotal%</span>
   <div class='clear'></div>
   <div class='btn-group'>
   <button class='btn btn-xs btn-default active'>₦%sprice%</button>
   <input type='number' class='btn btn-xs btn-default' value='%qty%' min='1' style='width:70px' onkeyup='Cart.qty(%n%,this.value)' onchange='Cart.qty(%n%,this.value)' onclick="onMenu(event,getObj('number_box_%n%'),function(){});toggle('ae_menu_%n%',function(o){o.show();},function(o){o.hide();},getObj('number_box_%n%'))"/></div></div></div>
   <div class='number-box row hide2' id='number_box_%n%'>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,1)'>1</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,2)'>2</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,3)'>3</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,4)'>4</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,5)'>5</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,6)'>6</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,7)'>7</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,8)'>8</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,9)'>9</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,0)'>0</button>
   <button class='col-xs-6 btn btn-default' onclick='NumberPicker.clearEntry(%n%)'>CE</button>
   </div><div class='clear'></div>
   </div>
   </div>
    <?php 
    include 'view/process_cart.php';
    ?>
  </body>
</html>

