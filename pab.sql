-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 04, 2014 at 11:04 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pab`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `best_sell`
--
CREATE TABLE IF NOT EXISTS `best_sell` (
`name` varchar(511)
,`freq` decimal(32,0)
,`instock` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `best_sell_1m`
--
CREATE TABLE IF NOT EXISTS `best_sell_1m` (
`name` varchar(255)
,`freq` decimal(32,0)
,`instock` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`categoryID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryID`, `name`) VALUES
(1, 'Phone'),
(2, 'home appliace'),
(3, 'camera'),
(4, 'Fast food'),
(5, 'Deodourant'),
(6, 'home appliaces'),
(7, 'Food'),
(8, 'Computer'),
(9, 'softdrink'),
(10, 'money'),
(11, 'SMart Phone');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `configID` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_note` text NOT NULL,
  `invoice_printer` enum('standard','mini') NOT NULL,
  `auto_sync` tinyint(1) NOT NULL DEFAULT '1',
  `expiry` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`configID`, `invoice_note`, `invoice_printer`, `auto_sync`, `expiry`) VALUES
(1, 'Hello world', 'mini', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customerID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `addr` text,
  `usrID` int(11) NOT NULL,
  `date` varchar(25) NOT NULL,
  PRIMARY KEY (`customerID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`),
  KEY `usrID` (`usrID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=163 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerID`, `name`, `phone`, `email`, `addr`, `usrID`, `date`) VALUES
(1, 'Aliu Wuyi Adepoju', NULL, 'aliuadepoju@gmail.com', 'Hello world', 0, '1409067394'),
(2, 'Adeyinka Grandson', '08034309999', 'adeyinkagrandson@gmail.com', 'Hello world', 0, '1409067430'),
(5, 'Anderson Obah', '08032447313', 'andersonmobah@gmail.com', 'UI/UX designer', 0, '1409067565'),
(6, 'Bisola', NULL, NULL, NULL, 0, '1409158043'),
(7, 'Superior mobile', NULL, NULL, NULL, 0, '1409159234'),
(8, 'Victor obehi', NULL, NULL, NULL, 0, '1409187571'),
(11, 'Anonymous', NULL, NULL, NULL, 0, '1409859767'),
(15, 'Anonymous', NULL, NULL, NULL, 0, '1410253079'),
(16, 'Anonymous', NULL, NULL, NULL, 0, '1410266613'),
(18, 'Aliu Wuyi Adepoju', '2348034309999', 'aliu.adepoju@gmail.com', '    ', 0, '1410330582'),
(20, '', '', '', '    ', 0, '1410330619'),
(25, 'Anonymous', NULL, NULL, NULL, 0, '1410333018'),
(26, 'Anonymous', NULL, NULL, NULL, 0, '1410333073'),
(27, 'Anonymous', NULL, NULL, NULL, 0, '1410795025'),
(29, 'Anonymous', NULL, NULL, NULL, 0, '1410814581'),
(30, 'Anonymous', NULL, NULL, NULL, 0, '1410943434'),
(31, 'Anonymous', NULL, NULL, NULL, 0, '1410943499'),
(34, 'Anonymous', NULL, NULL, NULL, 0, '1410943866'),
(36, 'Anonymous', NULL, NULL, NULL, 0, '1410943940'),
(37, 'Anonymous', NULL, NULL, NULL, 0, '1410944014'),
(38, 'Anonymous', NULL, NULL, NULL, 0, '1410955930'),
(39, 'Anonymous', NULL, NULL, NULL, 0, '1411060561'),
(40, 'Anonymous', NULL, NULL, NULL, 0, '1411710800'),
(41, 'Lydia', NULL, NULL, NULL, 0, '1413534409'),
(42, 'Bose', NULL, NULL, NULL, 0, '1413701272'),
(43, 'Christian', NULL, NULL, NULL, 0, '1413707027'),
(44, 'Anonymous', NULL, NULL, NULL, 0, '1413785815'),
(45, 'Anonymous', NULL, NULL, NULL, 0, '1413801206'),
(46, 'Henry', NULL, NULL, NULL, 0, '1413881287'),
(47, 'Becky', NULL, NULL, NULL, 0, '1413901205'),
(48, 'Lydia', NULL, NULL, NULL, 0, '1413979080'),
(49, 'Ronke', NULL, NULL, NULL, 0, '1413979105'),
(50, 'Busola', NULL, NULL, NULL, 0, '1413979196'),
(51, 'Felix', NULL, NULL, NULL, 0, '1414040983'),
(52, 'Kunle', NULL, NULL, NULL, 0, '1414041263'),
(53, 'Felix', NULL, NULL, NULL, 0, '1414167988'),
(54, 'Ayo', NULL, NULL, NULL, 0, '1414169826'),
(55, 'Segun', NULL, NULL, NULL, 0, '1414266086'),
(56, 'Hakeem', NULL, NULL, NULL, 0, '1414397189'),
(57, 'SuperiorMobile', NULL, NULL, NULL, 0, '1414418816'),
(58, 'Oshi o da', NULL, NULL, NULL, 0, '1414419495'),
(59, 'Christian', NULL, NULL, NULL, 0, '1414678317'),
(60, 'opeyemi', NULL, NULL, NULL, 0, '1414820191'),
(61, 'Kunle', NULL, NULL, NULL, 0, '1414879693'),
(62, 'opeyemi', NULL, NULL, NULL, 0, '1414884351'),
(63, 'Ogundipe', NULL, NULL, NULL, 0, '1414884442'),
(64, 'Alhaja', NULL, NULL, NULL, 0, '1414917116'),
(65, 'Anonymous', NULL, NULL, NULL, 0, '1415012389'),
(66, 'Anonymous', NULL, NULL, NULL, 0, '1415017305'),
(67, 'Anonymous', NULL, NULL, NULL, 0, '1415017321'),
(68, 'Anonymous', NULL, NULL, NULL, 0, '1415036390'),
(69, 'Lydia', NULL, NULL, NULL, 0, '1415039281'),
(70, 'Anonymous', NULL, NULL, NULL, 0, '1415039771'),
(71, 'Anonymous', NULL, NULL, NULL, 0, '1415094202'),
(72, 'Anonymous', NULL, NULL, NULL, 0, '1415094581'),
(73, 'Anonymous', NULL, NULL, NULL, 0, '1415094640'),
(74, 'Anonymous', NULL, NULL, NULL, 0, '1415096221'),
(88, 'Anonymous', NULL, NULL, NULL, 0, '1415099435'),
(89, 'Anonymous', NULL, NULL, NULL, 0, '1415100135'),
(90, 'Anonymous', NULL, NULL, NULL, 0, '1415100988'),
(93, 'Anonymous', NULL, NULL, NULL, 0, '1415120459'),
(94, 'Oshi o da', NULL, NULL, NULL, 0, '1415121767'),
(99, 'Anonymous', NULL, NULL, NULL, 0, '1415377409'),
(100, 'Anonymous', NULL, NULL, NULL, 0, '1415377706'),
(101, 'Kofo', NULL, NULL, 'Owode, Ajah', 0, '1415377831'),
(102, 'Anonymous', NULL, NULL, NULL, 0, '1415404900'),
(103, 'Anonymous', NULL, NULL, NULL, 0, '1415479586'),
(104, 'Anonymous', NULL, NULL, NULL, 0, '1415714180'),
(105, 'Anonymous', NULL, NULL, NULL, 0, '1415734597'),
(106, 'Anonymous', NULL, NULL, NULL, 0, '1415734779'),
(107, 'Anonymous', NULL, NULL, NULL, 0, '1415734967'),
(108, 'Anonymous', NULL, NULL, NULL, 0, '1415737124'),
(109, 'Anonymous', NULL, NULL, NULL, 0, '1415737567'),
(110, 'Anonymous', NULL, NULL, NULL, 0, '1415737854'),
(111, 'Anonymous', NULL, NULL, NULL, 0, '1415806389'),
(112, 'Anonymous', NULL, NULL, NULL, 0, '1415810485'),
(113, 'Anonymous', NULL, NULL, NULL, 0, '1415811437'),
(114, 'Anonymous', NULL, NULL, NULL, 0, '1415881467'),
(115, 'Anonymous', NULL, NULL, NULL, 0, '1415881562'),
(116, 'Anonymous', NULL, NULL, NULL, 0, '1416055420'),
(117, 'Anonymous', NULL, NULL, NULL, 0, '1416058973'),
(118, 'Anonymous', NULL, NULL, NULL, 0, '1416059149'),
(119, 'Anonymous', NULL, NULL, NULL, 0, '1416059249'),
(120, 'Anonymous', NULL, NULL, NULL, 0, '1416059370'),
(121, 'Anonymous', NULL, NULL, NULL, 0, '1416059457'),
(122, 'Anonymous', NULL, NULL, NULL, 0, '1416059510'),
(123, 'Anonymous', NULL, NULL, NULL, 0, '1416059606'),
(124, 'Anonymous', NULL, NULL, NULL, 0, '1416064562'),
(125, 'Anonymous', NULL, NULL, NULL, 0, '1416070277'),
(126, 'Anonymous', NULL, NULL, NULL, 0, '1416153740'),
(127, 'Anonymous', NULL, NULL, NULL, 0, '1416153784'),
(128, 'Anonymous', NULL, NULL, NULL, 0, '1416153850'),
(129, 'Anonymous', NULL, NULL, NULL, 0, '1416153975'),
(130, 'Anonymous', NULL, NULL, NULL, 0, '1416154333'),
(131, 'Anonymous', NULL, NULL, NULL, 0, '1416398459'),
(132, 'Oretu Ruth', NULL, NULL, NULL, 0, '1416399022'),
(136, 'Anonymous', NULL, NULL, NULL, 0, '1416686195'),
(137, 'Anonymous', NULL, NULL, NULL, 0, '1416686283'),
(138, 'Anonymous', NULL, NULL, NULL, 0, '1416686374'),
(139, 'Anonymous', NULL, NULL, NULL, 0, '1416686390'),
(140, 'Anonymous', NULL, NULL, NULL, 0, '1416687460'),
(141, 'Anonymous', NULL, NULL, NULL, 0, '1416716269'),
(142, 'Anonymous', NULL, NULL, NULL, 0, '1416716341'),
(143, 'Anonymous', NULL, NULL, NULL, 0, '1416845845'),
(144, 'Anonymous', NULL, NULL, NULL, 0, '1416912043'),
(145, 'Anonymous', NULL, NULL, NULL, 0, '1417330106'),
(146, 'Anonymous', NULL, NULL, NULL, 0, '1417330393'),
(147, 'Anonymous', NULL, NULL, NULL, 0, '1417334919'),
(148, 'mathew', NULL, NULL, NULL, 0, '1417359452'),
(149, 'Anonymous', NULL, NULL, NULL, 0, '1417410943'),
(150, 'Anonymous', NULL, NULL, NULL, 0, '1417411687'),
(151, 'Anonymous', NULL, NULL, NULL, 0, '1417413102'),
(152, 'Anonymous', NULL, NULL, NULL, 0, '1417416027'),
(153, 'Anonymous', NULL, NULL, NULL, 0, '1417422738'),
(154, 'Anonymous', NULL, NULL, NULL, 0, '1417422970'),
(155, 'Anonymous', NULL, NULL, NULL, 0, '1417425821'),
(156, 'Anonymous', NULL, NULL, NULL, 0, '1417498921'),
(157, 'Anonymous', NULL, NULL, NULL, 0, '1417499032'),
(158, 'Anonymous', NULL, NULL, NULL, 0, '1417499103'),
(159, 'Anonymous', NULL, NULL, NULL, 0, '1417499765'),
(160, 'Jaiyeola', NULL, NULL, NULL, 0, '1417593866'),
(161, 'Christian', NULL, NULL, NULL, 0, '1417670381'),
(162, 'Adewuyi', NULL, NULL, NULL, 0, '1417670784');

-- --------------------------------------------------------

--
-- Table structure for table `img`
--

CREATE TABLE IF NOT EXISTS `img` (
  `imgID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`imgID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=166 ;

--
-- Dumping data for table `img`
--

INSERT INTO `img` (`imgID`, `name`) VALUES
(8, NULL),
(103, '0iVAsNlEyUBR.png'),
(13, '0phCgGzaXgnw.png'),
(33, '0QZOilbc4rjX.png'),
(165, '0zCd9hVPZojY.png'),
(38, '1iRddfR1fhqh.png'),
(29, '1vZD97GZYXVf.png'),
(6, '21688kettles.jpg'),
(3, '21688kettles.png'),
(85, '2Ki8qPVwLcAp.png'),
(145, '2Q1AbDqU5Q.png'),
(50, '3coblayTjg.png'),
(113, '3Li8QtApcLPV.png'),
(155, '3OLRy9KxeN.png'),
(32, '417GpDGvYzsT.png'),
(101, '4dFGGKOvNp8e.png'),
(93, '5EpOsYQBFyGo.png'),
(89, '6Xtym86ad4Gi.png'),
(115, '80nDfKhvLR49.png'),
(131, '8kTK6BQHj5hU.png'),
(112, '9hOf6KwZC3A7.png'),
(65, 'aaJfVybut4A1.png'),
(95, 'At3l3KDqAiHU.png'),
(104, 'bBuo0wX9ikUP.png'),
(35, 'bdfodFxJpPMs.png'),
(107, 'Be7NUJxaGd3e.png'),
(11, 'Bp3nKMrIHKuJpng'),
(19, 'BQDjbSdhiCET.png'),
(106, 'btpgU2fBWhNx.png'),
(129, 'burger1.png'),
(48, 'C4L3yZRi1C.png'),
(164, 'CvTiynAaONYa.png'),
(26, 'DDf9zPMDDsrb.png'),
(63, 'DGRMA9ylChTE.png'),
(1, 'dKjE9foxSo.png'),
(42, 'DrKEbZzCjLwI.png'),
(49, 'dSLGgBbO6L.png'),
(53, 'dSWADxW7e00g.png'),
(91, 'DThRpOIw5gvf.png'),
(22, 'dXp2ZLTb8T6Z.png'),
(58, 'E9n2P1V9Apf3.png'),
(56, 'ehUTMgiZKu7K.png'),
(78, 'eJNl2WMf2Bc1.png'),
(151, 'ElXXjljSGFdD.png'),
(28, 'EPbEJl4E1QVd.png'),
(39, 'EPq1kbp8qc2i.png'),
(27, 'f4fZCth1ixyH.png'),
(54, 'FaGr5rAvn3FJ.png'),
(70, 'fi01zxbnCib7.png'),
(87, 'fSIYlzH2ppRG.png'),
(147, 'FVqdzDxhIdGI.png'),
(23, 'G1IdUG7iw6KC.png'),
(90, 'gEMxb2av3u2n.png'),
(146, 'GIyxwIf1WkHP.png'),
(41, 'gvyIsjwSwt0v.png'),
(72, 'hFH2OGbnJyrC.png'),
(34, 'hjr99xicap10.png'),
(18, 'hK65Qs3Yu3Bu.png'),
(86, 'hKYYgJOnbCHs.png'),
(57, 'i62Nx2dMzL.png'),
(83, 'ia7M998sBJRJ.png'),
(66, 'iHYVjYj2YLZS.png'),
(12, 'ImY1TiGYfoUW.png'),
(150, 'JADQlftQpxP1.png'),
(69, 'JcapHfAbgL83.png'),
(73, 'JfhFBuHQP6h8.png'),
(71, 'jhjLfmToTs8J.png'),
(100, 'JiVAVyTHljoe.png'),
(111, 'JuZfL4SE3ANk.png'),
(110, 'jWjrOIFhFASB.png'),
(59, 'JYPG9cyBbo.png'),
(114, 'KIPaatgw5Qz4.png'),
(132, 'kr8DtIUxF5GO.png'),
(75, 'Ks0oZWipV1Iw.png'),
(108, 'Ktwj3PAH5eDZ.png'),
(109, 'lDHrQqWgIXdA.png'),
(31, 'lpmXF8BMf1uK.png'),
(2, 'M872co9VyI.png'),
(20, 'Mep2125Ht1ZC.png'),
(67, 'mswALZ4xGDOS.png'),
(134, 'NnNCxzRpEIXU.png'),
(153, 'o79pbuYFqy17.png'),
(94, 'oeij2PFbHyXq.png'),
(51, 'oLS4n6naQI.png'),
(74, 'PDcjpDeB7AhB.png'),
(76, 'pERTHcDCWakI.png'),
(21, 'pLIs6kqGmqf7.png'),
(130, 'PRz7u4rvi4Jp.png'),
(92, 'Q5o4c9bee60q.png'),
(143, 'RgWuHkwjHkfC.png'),
(96, 'rIixq4agpC46.png'),
(55, 'sWTaK1x1J6ZC.png'),
(77, 'T7Yx8KSaIT1X.png'),
(24, 'TcgjKGtP30sM.png'),
(47, 'tfGdHFsNbNKw.png'),
(79, 'tPIzMOFQCm6E.png'),
(36, 'tuxgWO8SpF9D.png'),
(152, 'uc7HtWzXEBS0.png'),
(154, 'UI3Gw1YoOy5J.png'),
(148, 'uOknXh5TSfvy.png'),
(105, 'W49nzIR6drCv.png'),
(81, 'w98qn49ICgiN.png'),
(17, 'WbXdKLEt4Bmi.png'),
(43, 'WdU8k0EAykZM.png'),
(40, 'WS4wBY5I7JHC.png'),
(52, 'wwRwxOAG2Z.png'),
(136, 'WXgDgkZIenzD.png'),
(37, 'X7CXiUtTPbEW.png'),
(102, 'xfLVKiRomIZB.png'),
(68, 'xJlv2TA16Mmx.png'),
(25, 'xqxcuuiV7HMf.png'),
(80, 'XUcE96HZKfPp.png'),
(84, 'YG9dNl4aeHhp.png'),
(14, 'YH8q2enHvCnT.png'),
(30, 'zAcSlDh5DyPA.png'),
(16, 'zHxC8bOOhPPa.png'),
(99, 'ZM8EvSkqMcRI.png'),
(88, 'ZQwT7I4EqfQ5.png'),
(82, 'ZrNdV4rZ5aSC.png'),
(133, 'zTPr1xLBIKkI.png');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `manufacturerID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`manufacturerID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`manufacturerID`, `name`) VALUES
(1, 'Nokia'),
(2, 'Blackberry'),
(3, 'Eleganza'),
(4, 'canon'),
(5, 'Nestle'),
(6, 'Reckitt benckiser'),
(7, 'Apple'),
(8, 'golden penny'),
(9, 'Alomo'),
(10, 'Self'),
(11, 'HP'),
(12, 'Pepsi'),
(13, 'Nigeria'),
(14, 'Microsoft');

-- --------------------------------------------------------

--
-- Stand-in structure for view `most_sold`
--
CREATE TABLE IF NOT EXISTS `most_sold` (
`name` varchar(255)
,`productID` int(11)
,`freq` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `outofstock`
--
CREATE TABLE IF NOT EXISTS `outofstock` (
`cname` varchar(255)
,`pname` varchar(511)
,`cprice` decimal(11,2)
);
-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `productID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `manufacturerID` int(11) unsigned NOT NULL,
  `categoryID` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `reorder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`productID`),
  UNIQUE KEY `makeID` (`manufacturerID`,`categoryID`,`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`productID`, `manufacturerID`, `categoryID`, `name`, `description`, `reorder`) VALUES
(1, 1, 1, 'XL', '', 5),
(2, 2, 1, 'z10', '', 0),
(3, 2, 1, 'XL', '', 0),
(4, 3, 2, 'kettle', '', 0),
(5, 4, 3, 'm77', '', 0),
(6, 3, 1, 'XL', '', 20),
(7, 5, 4, 'golden morn', '', 0),
(8, 3, 4, 'XL', '', 0),
(9, 6, 5, 'Air wick', '', 0),
(10, 7, 1, 'iphone 5', '', 300),
(11, 8, 4, 'noodles', '', 0),
(12, 2, 1, 'curve 3', '', 0),
(13, 7, 1, 'iphone 6', '', 0),
(14, 3, 6, 'kettle', '', 0),
(15, 10, 7, 'Rice', '', 0),
(16, 11, 8, 'laptop', '', 0),
(17, 11, 8, '6735s', '', 0),
(18, 12, 9, 'drink', '', 0),
(19, 13, 10, '1000', '', 0),
(20, 1, 1, '2310', '', 0),
(21, 1, 11, 'XL', '', 0),
(22, 1, 1, 'C5', NULL, 0),
(23, 1, 1, 'X2', 'Seun', 0),
(24, 1, 1, 'X1', 'supplied by seun', 0),
(25, 1, 1, 'X0', 'Hello world', 0),
(26, 14, 1, 'Lumia', 'MS product', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `profileID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `storeID` varchar(10) DEFAULT NULL,
  `abbr` varchar(5) DEFAULT NULL,
  `phone` varchar(125) NOT NULL,
  `email` varchar(75) NOT NULL,
  `web` varchar(50) NOT NULL,
  `addr` text,
  `logo` varchar(20) NOT NULL,
  `synKey` varchar(25) DEFAULT NULL,
  `synPwd` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`profileID`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `storeID` (`storeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`profileID`, `name`, `storeID`, `abbr`, `phone`, `email`, `web`, `addr`, `logo`, `synKey`, `synPwd`) VALUES
(1, 'b-lint', '2', 'BL', '2348034309999,08058322957', 'aliu.adepoju@gmail.com', 'www.attemptexam.com', '3rd floor, safeway building, sangotedo, Ajah axis, lekki/epe expressway', 'store.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchaseID` int(11) NOT NULL AUTO_INCREMENT,
  `stockID` int(11) NOT NULL,
  `codes` text,
  `qty` int(11) NOT NULL,
  `supplierID` int(11) NOT NULL,
  `receiverID` int(11) NOT NULL,
  `receipt` varchar(50) DEFAULT NULL,
  `expiry` varchar(20) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `date` varchar(25) NOT NULL,
  PRIMARY KEY (`purchaseID`),
  KEY `productID` (`stockID`,`supplierID`,`receiverID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=179 ;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`purchaseID`, `stockID`, `codes`, `qty`, `supplierID`, `receiverID`, `receipt`, `expiry`, `note`, `date`) VALUES
(1, 1, NULL, 5, 1, 1, NULL, NULL, NULL, ''),
(2, 2, NULL, 15, 2, 1, NULL, NULL, NULL, ''),
(3, 2, NULL, 2, 1, 1, NULL, NULL, NULL, ''),
(4, 2, NULL, 10, 1, 1, NULL, NULL, NULL, ''),
(5, 3, NULL, 5, 1, 1, NULL, NULL, NULL, '1409402454'),
(6, 4, NULL, 10, 1, 1, NULL, NULL, NULL, '1409432275'),
(7, 5, NULL, 30, 1, 1, NULL, NULL, NULL, '1409432507'),
(8, 5, NULL, 30, 1, 1, NULL, NULL, NULL, '1409432631'),
(9, 5, NULL, 20, 1, 1, NULL, NULL, NULL, '1409432855'),
(10, 5, NULL, 20, 1, 1, NULL, NULL, NULL, '1409434979'),
(11, 5, NULL, 20, 1, 1, NULL, NULL, NULL, '1409435396'),
(12, 5, NULL, 20, 1, 1, NULL, NULL, NULL, '1409435524'),
(13, 5, NULL, 20, 1, 1, NULL, NULL, NULL, '1409435547'),
(14, 5, NULL, 20, 1, 1, NULL, NULL, NULL, '1409435746'),
(15, 5, NULL, 20, 1, 1, NULL, NULL, NULL, '1409435786'),
(16, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409436428'),
(17, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409436464'),
(18, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409436664'),
(19, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409436688'),
(20, 7, NULL, 5, 1, 1, NULL, NULL, NULL, '1409437081'),
(21, 8, NULL, 1, 1, 1, NULL, NULL, NULL, '1409533828'),
(22, 8, NULL, 1, 1, 1, NULL, NULL, NULL, '1409560565'),
(23, 8, NULL, 1, 1, 1, NULL, NULL, NULL, '1409560611'),
(24, 8, NULL, 1, 1, 1, NULL, NULL, NULL, '1409560822'),
(25, 9, NULL, 30, 1, 1, NULL, NULL, NULL, '1409560924'),
(26, 9, NULL, 30, 1, 1, NULL, NULL, NULL, '1409561133'),
(27, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409581814'),
(28, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409581862'),
(29, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409581900'),
(30, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409582155'),
(31, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409582306'),
(32, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409582335'),
(33, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409582362'),
(34, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409582414'),
(35, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409582433'),
(36, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409582463'),
(37, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409586802'),
(38, 6, NULL, 1, 1, 1, NULL, NULL, NULL, '1409586908'),
(39, 10, NULL, 1, 1, 1, NULL, NULL, NULL, '1409587382'),
(40, 10, NULL, 1, 1, 1, NULL, NULL, NULL, '1409587601'),
(41, 10, NULL, 1, 1, 1, NULL, NULL, NULL, '1409587733'),
(42, 11, NULL, 15, 1, 1, NULL, NULL, NULL, '1409594029'),
(43, 11, NULL, 15, 1, 1, NULL, NULL, NULL, '1409596760'),
(44, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409603888'),
(45, 13, NULL, 47, 1, 1, NULL, NULL, NULL, '1409608426'),
(46, 13, NULL, 1, 1, 1, NULL, NULL, NULL, '1409608543'),
(47, 14, NULL, 1, 5, 1, NULL, NULL, NULL, '1409648621'),
(48, 15, NULL, 7, 1, 1, NULL, NULL, NULL, '1409658093'),
(49, 3, NULL, 8, 1, 1, NULL, NULL, NULL, '1409658250'),
(50, 16, NULL, 8, 42, 1, NULL, NULL, NULL, '1409660928'),
(51, 2, NULL, 1, 1, 1, NULL, NULL, NULL, '1409661457'),
(52, 5, NULL, 200, 1, 1, NULL, NULL, NULL, '1409663525'),
(53, 5, NULL, 400, 1, 1, NULL, NULL, NULL, '1409663588'),
(54, 16, NULL, 8, 1, 1, NULL, NULL, NULL, '1409663757'),
(55, 14, NULL, 1, 1, 1, NULL, NULL, NULL, '1409664529'),
(56, 17, NULL, 0, 1, 1, NULL, NULL, NULL, '1409664918'),
(57, 6, NULL, 16, 1, 1, NULL, NULL, NULL, '1409665579'),
(58, 6, NULL, 32, 1, 1, NULL, NULL, NULL, '1409665727'),
(59, 17, NULL, 1, 1, 1, NULL, NULL, NULL, '1409665791'),
(60, 1, NULL, 5, 1, 1, NULL, NULL, NULL, '1409665864'),
(61, 10, NULL, 3, 1, 1, NULL, NULL, NULL, '1409666284'),
(62, 13, NULL, 48, 1, 1, NULL, NULL, NULL, '1409671497'),
(63, 2, NULL, 13, 1, 1, NULL, NULL, NULL, '1409680698'),
(64, 16, NULL, 16, 1, 1, NULL, NULL, NULL, '1409680834'),
(65, 16, NULL, 32, 1, 1, NULL, NULL, NULL, '1409681136'),
(66, 3, NULL, 13, 1, 1, NULL, NULL, NULL, '1409681187'),
(67, 15, NULL, 7, 1, 1, NULL, NULL, NULL, '1409681247'),
(68, 9, NULL, 60, 1, 1, NULL, NULL, NULL, '1409681396'),
(69, 8, NULL, 4, 1, 1, NULL, NULL, NULL, '1409681481'),
(70, 11, NULL, 30, 62, 1, NULL, NULL, NULL, '1409681840'),
(71, 18, NULL, 13, 1, 1, NULL, NULL, NULL, '1409689665'),
(72, 18, NULL, 13, 1, 1, NULL, NULL, NULL, '1409689814'),
(73, 18, NULL, 26, 1, 1, NULL, NULL, NULL, '1409689881'),
(74, 18, NULL, 52, 1, 1, NULL, NULL, NULL, '1409690809'),
(75, 18, NULL, 104, 1, 1, NULL, NULL, NULL, '1409690946'),
(76, 18, NULL, 208, 1, 1, NULL, NULL, NULL, '1409691004'),
(77, 18, NULL, 416, 1, 1, NULL, NULL, NULL, '1409691203'),
(78, 18, NULL, 416, 1, 1, NULL, NULL, NULL, '1409691225'),
(79, 18, NULL, 416, 1, 1, NULL, NULL, NULL, '1409691257'),
(80, 18, NULL, 416, 1, 1, NULL, NULL, NULL, '1409691292'),
(81, 18, NULL, 416, 1, 1, NULL, NULL, NULL, '1409691346'),
(82, 18, NULL, 416, 1, 1, NULL, NULL, NULL, '1409691398'),
(83, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692109'),
(84, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692189'),
(85, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692228'),
(86, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692273'),
(87, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692306'),
(88, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692385'),
(89, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692416'),
(90, 12, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692482'),
(91, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692825'),
(92, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692866'),
(93, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692900'),
(94, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692933'),
(95, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409692970'),
(96, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409693002'),
(97, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409693048'),
(98, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409693083'),
(99, 19, NULL, 34, 1, 1, NULL, NULL, NULL, '1409695473'),
(100, 20, NULL, 9, 65, 1, NULL, NULL, NULL, '1409813527'),
(101, 21, NULL, 1, 66, 1, NULL, NULL, NULL, '1409813528'),
(102, 20, NULL, 9, 67, 1, NULL, NULL, NULL, '1409813648'),
(103, 21, NULL, 1, 68, 1, NULL, NULL, NULL, '1409813649'),
(104, 22, NULL, 11, 69, 1, NULL, NULL, NULL, '1409813649'),
(105, 20, NULL, 9, 70, 1, NULL, NULL, NULL, '1409813922'),
(106, 21, NULL, 1, 71, 1, NULL, NULL, NULL, '1409813922'),
(107, 22, NULL, 11, 72, 1, NULL, NULL, NULL, '1409813923'),
(108, 23, NULL, 15, 73, 1, NULL, NULL, NULL, '1409813923'),
(109, 20, NULL, 9, 74, 1, NULL, NULL, NULL, '1409814072'),
(110, 21, NULL, 1, 75, 1, NULL, NULL, NULL, '1409814072'),
(111, 22, NULL, 11, 76, 1, NULL, NULL, NULL, '1409814072'),
(112, 24, NULL, 15, 77, 1, NULL, NULL, NULL, '1409814072'),
(113, 20, NULL, 9, 78, 1, NULL, NULL, NULL, '1409814137'),
(114, 21, NULL, 1, 79, 1, NULL, NULL, NULL, '1409814137'),
(115, 22, NULL, 11, 80, 1, NULL, NULL, NULL, '1409814138'),
(116, 24, NULL, 15, 81, 1, NULL, NULL, NULL, '1409814138'),
(117, 20, NULL, 9, 82, 1, NULL, NULL, NULL, '1409814246'),
(118, 21, NULL, 1, 83, 1, NULL, NULL, NULL, '1409814246'),
(119, 22, NULL, 11, 84, 1, NULL, NULL, NULL, '1409814246'),
(120, 24, NULL, 15, 85, 1, NULL, NULL, NULL, '1409814246'),
(121, 20, NULL, 9, 86, 1, NULL, NULL, NULL, '1409814267'),
(122, 21, NULL, 1, 87, 1, NULL, NULL, NULL, '1409814267'),
(123, 22, NULL, 11, 88, 1, NULL, NULL, NULL, '1409814267'),
(124, 25, NULL, 15, 89, 1, NULL, NULL, NULL, '1409814267'),
(125, 20, NULL, 9, 90, 1, NULL, NULL, NULL, '1409814314'),
(126, 21, NULL, 1, 91, 1, NULL, NULL, NULL, '1409814314'),
(127, 22, NULL, 11, 92, 1, NULL, NULL, NULL, '1409814314'),
(128, 26, NULL, 20, 93, 1, NULL, NULL, NULL, '1409814314'),
(129, 20, '["aaa","bbb","ccc","ddd","eee"]', 5, 94, 1, NULL, NULL, NULL, '1409814866'),
(130, 20, NULL, 1, 95, 1, NULL, NULL, NULL, '1409814998'),
(131, 6, NULL, 1, 96, 1, NULL, NULL, NULL, '1409814999'),
(132, 21, NULL, 1, 97, 1, NULL, NULL, NULL, '1409814999'),
(133, 27, NULL, 1, 98, 1, NULL, NULL, NULL, '1409814999'),
(134, 20, NULL, 1, 99, 1, NULL, NULL, NULL, '1409815085'),
(135, 20, NULL, 1, 100, 1, NULL, NULL, NULL, '1409815117'),
(136, 27, NULL, 1, 101, 1, NULL, NULL, NULL, '1409817001'),
(137, 28, NULL, 1, 102, 1, NULL, NULL, NULL, '1409817001'),
(138, 6, NULL, 1, 103, 1, NULL, NULL, NULL, '1409817120'),
(139, 29, NULL, 5, 1, 1, NULL, NULL, NULL, '1409835464'),
(140, 29, NULL, 5, 1, 1, NULL, NULL, NULL, '1409835490'),
(141, 20, NULL, 1, 106, 1, NULL, NULL, NULL, '1409835642'),
(142, 21, NULL, 1, 107, 1, NULL, NULL, NULL, '1409835642'),
(143, 6, NULL, 1, 108, 1, NULL, NULL, NULL, '1409835642'),
(144, 29, NULL, 0, 109, 1, NULL, NULL, NULL, '1409848591'),
(145, 21, NULL, 0, 1, 1, NULL, NULL, NULL, '1409889079'),
(146, 4, NULL, 0, 1, 1, NULL, NULL, NULL, '1409889149'),
(147, 30, NULL, 0, 112, 1, NULL, NULL, NULL, '1409889214'),
(148, 21, NULL, 0, 113, 1, NULL, NULL, NULL, '1409889263'),
(149, 7, NULL, 0, 1, 1, NULL, NULL, NULL, '1409889329'),
(150, 31, NULL, 78, 1, 1, NULL, NULL, NULL, '1410502569'),
(151, 30, NULL, 20, 115, 1, NULL, NULL, NULL, '1410502757'),
(152, 32, NULL, 3, 1, 4, NULL, NULL, NULL, '1415465259'),
(153, 33, NULL, 4, 123, 4, NULL, NULL, NULL, '1415465776'),
(154, 34, NULL, 3, 3, 2, NULL, NULL, NULL, '1415466858'),
(155, 35, NULL, 15, 5, 4, NULL, '08/12/2015', NULL, '1415483463'),
(156, 36, NULL, 9, 126, 4, NULL, NULL, 'Hello world', '1415487394'),
(157, 37, NULL, 10, 127, 4, NULL, NULL, '', '1415531167'),
(158, 38, NULL, 1, 115, 4, NULL, NULL, '', '1415531271'),
(159, 36, NULL, 8, 1, 4, NULL, NULL, '', '1415531332'),
(160, 39, NULL, 5, 128, 4, NULL, NULL, NULL, '1415594472'),
(161, 40, NULL, 20, 129, 4, NULL, NULL, NULL, '1415594475'),
(162, 41, NULL, 1, 130, 4, NULL, NULL, NULL, '1415598038'),
(163, 42, NULL, 1, 3, 4, NULL, NULL, NULL, '1415607318'),
(164, 43, NULL, 1, 1, 4, NULL, NULL, '    Hello world', '1415607740'),
(165, 44, NULL, 17, 128, 4, NULL, NULL, 'Nokia for android lovers', '1416727055'),
(166, 44, NULL, 17, 1, 4, NULL, NULL, 'Android', '1416727154'),
(167, 30, NULL, 2, 129, 4, NULL, NULL, '', '1417362310'),
(168, 34, NULL, 15, 130, 2, NULL, NULL, '', '1417683430'),
(169, 45, NULL, 17, 131, 2, NULL, NULL, '', '1417683721'),
(170, 46, NULL, 10, 132, 2, NULL, NULL, '', '1417683856'),
(171, 46, NULL, 2, 133, 2, NULL, NULL, 'correct', '1417683908'),
(172, 47, NULL, 2, 134, 2, NULL, NULL, 'correct', '1417683955'),
(173, 48, NULL, 20, 135, 2, NULL, NULL, '', '1417684066'),
(174, 49, NULL, 15, 136, 2, NULL, NULL, '', '1417684215'),
(175, 50, NULL, 5, 137, 2, NULL, NULL, NULL, '1417686310'),
(176, 51, NULL, 5, 138, 2, NULL, NULL, NULL, '1417686605'),
(177, 52, NULL, 2, 139, 2, NULL, NULL, '', '1417687121'),
(178, 53, NULL, 15, 140, 2, NULL, NULL, 'Include note', '1417687335');

-- --------------------------------------------------------

--
-- Stand-in structure for view `reorderlevel`
--
CREATE TABLE IF NOT EXISTS `reorderlevel` (
`cname` varchar(255)
,`pname` varchar(511)
,`cprice` decimal(11,2)
,`qty` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `roleID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleID`, `name`) VALUES
(1, 'owner'),
(2, 'manager'),
(3, 'seller');

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE IF NOT EXISTS `sale` (
  `saleID` int(11) NOT NULL AUTO_INCREMENT,
  `stockID` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `discount` varchar(150) DEFAULT NULL,
  `deduct` decimal(20,2) DEFAULT '0.00',
  `paid` decimal(20,2) NOT NULL,
  `via` enum('cash','debit','credit','cheque') NOT NULL,
  `invoice` varchar(10) NOT NULL,
  `serialNo` text,
  `customerID` int(11) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  `status` enum('sold','refund','backorder') NOT NULL,
  `usrID` int(11) NOT NULL,
  `date` varchar(25) NOT NULL,
  PRIMARY KEY (`saleID`),
  KEY `typeID` (`stockID`),
  KEY `customerID` (`customerID`),
  KEY `agentID` (`usrID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=405 ;

--
-- Dumping data for table `sale`
--

INSERT INTO `sale` (`saleID`, `stockID`, `qty`, `discount`, `deduct`, `paid`, `via`, `invoice`, `serialNo`, `customerID`, `note`, `status`, `usrID`, `date`) VALUES
(1, 1, 1, '{"each":0,"global":24}', 0.00, 0.00, 'cash', '1409850128', NULL, 1, NULL, 'sold', 1, ''),
(2, 2, 1, '{"each":0,"global":24}', 0.00, 0.00, 'cash', '1409850128', NULL, 1, NULL, 'sold', 1, ''),
(3, 9, 1, '{"each":0,"global":24}', 0.00, 0.00, 'cash', '1409850128', NULL, 1, NULL, 'sold', 1, ''),
(4, 12, 1, '{"each":0,"global":24}', 0.00, 0.00, 'cash', '1409850128', NULL, 1, NULL, 'sold', 1, ''),
(5, 14, 1, '{"each":0,"global":24}', 0.00, 0.00, 'cash', '1409850128', NULL, 1, NULL, 'sold', 1, ''),
(6, 15, 1, '{"each":0,"global":24}', 0.00, 0.00, 'cash', '1409850128', NULL, 1, NULL, 'sold', 1, ''),
(7, 16, 5, '{"each":0,"global":24}', 0.00, 0.00, 'cash', '1409850128', NULL, 1, NULL, 'sold', 1, ''),
(8, 23, 1, '{"each":0,"global":24}', 0.00, 0.00, 'cash', '1409850128', NULL, 1, NULL, 'sold', 1, ''),
(9, 1, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409850262', NULL, 2, NULL, 'sold', 1, ''),
(10, 3, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409850262', NULL, 2, NULL, 'sold', 1, ''),
(11, 8, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409850262', NULL, 2, NULL, 'sold', 1, ''),
(12, 15, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409850262', NULL, 2, NULL, 'sold', 1, ''),
(13, 3, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409859767', NULL, 11, NULL, 'sold', 1, ''),
(14, 14, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409859767', NULL, 11, NULL, 'sold', 1, ''),
(15, 15, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409859767', NULL, 11, NULL, 'sold', 1, ''),
(16, 1, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409880716', NULL, 1, NULL, 'sold', 1, ''),
(17, 15, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409880716', NULL, 1, NULL, 'sold', 1, ''),
(18, 12, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409880834', NULL, 5, NULL, 'sold', 1, ''),
(19, 13, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409880834', NULL, 5, NULL, 'sold', 1, ''),
(20, 19, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409880834', NULL, 5, NULL, 'sold', 1, ''),
(21, 26, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1409880834', NULL, 5, NULL, 'sold', 1, ''),
(22, 3, 1, '{"each":0,"global":18}', 0.00, 0.00, 'cash', '1410247834', NULL, 2, NULL, 'sold', 1, ''),
(23, 12, 1, '{"each":0,"global":18}', 0.00, 0.00, 'cash', '1410247834', NULL, 2, NULL, 'sold', 1, ''),
(24, 16, 1, '{"each":0,"global":18}', 0.00, 0.00, 'cash', '1410247834', NULL, 2, NULL, 'sold', 1, ''),
(25, 4, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410248102', NULL, 1, NULL, 'sold', 1, ''),
(26, 11, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410248102', NULL, 1, NULL, 'sold', 1, ''),
(27, 12, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410248102', NULL, 1, NULL, 'sold', 1, ''),
(28, 13, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410248102', NULL, 1, NULL, 'sold', 1, ''),
(29, 26, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410248102', NULL, 1, NULL, 'sold', 1, ''),
(30, 11, 1, '{"each":0,"global":18}', 0.00, 0.00, 'cash', '1410252967', NULL, 5, NULL, 'sold', 1, ''),
(31, 12, 1, '{"each":0,"global":18}', 0.00, 0.00, 'cash', '1410252967', NULL, 5, NULL, 'sold', 1, ''),
(32, 16, 1, '{"each":0,"global":18}', 0.00, 0.00, 'cash', '1410252967', NULL, 5, NULL, 'sold', 1, ''),
(33, 25, 8, '{"each":100,"global":18}', 0.00, 0.00, 'cash', '1410252967', NULL, 5, NULL, 'sold', 1, ''),
(34, 11, 1, '{"each":0,"global":61}', 0.00, 0.00, 'cash', '1410253079', NULL, 15, NULL, 'sold', 1, ''),
(35, 13, 9, '{"each":5,"global":61}', 0.00, 0.00, 'cash', '1410253079', NULL, 15, NULL, 'sold', 1, ''),
(36, 19, 3, '{"each":0,"global":61}', 0.00, 0.00, 'cash', '1410253079', NULL, 15, NULL, 'sold', 1, ''),
(37, 15, 1, '{"each":0,"global":17}', 0.00, 0.00, 'cash', '1410266613', NULL, 16, NULL, 'sold', 1, ''),
(38, 16, 1, '{"each":0,"global":17}', 0.00, 0.00, 'cash', '1410266613', NULL, 16, NULL, 'sold', 1, ''),
(39, 12, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410333018', NULL, 25, NULL, 'sold', 1, ''),
(40, 17, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410333018', NULL, 25, NULL, 'sold', 1, ''),
(41, 1, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410333073', NULL, 26, NULL, 'sold', 1, ''),
(42, 15, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410333073', NULL, 26, NULL, 'sold', 1, ''),
(43, 1, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410795025', NULL, 27, NULL, 'sold', 1, ''),
(44, 12, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410795025', NULL, 27, NULL, 'sold', 1, ''),
(45, 16, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410795025', NULL, 27, NULL, 'sold', 1, ''),
(46, 2, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410795084', NULL, 2, NULL, 'sold', 1, ''),
(47, 3, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410795084', NULL, 2, NULL, 'sold', 1, ''),
(48, 7, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410795084', NULL, 2, NULL, 'sold', 1, ''),
(49, 11, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410795084', NULL, 2, NULL, 'sold', 1, ''),
(50, 26, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410795084', NULL, 2, NULL, 'sold', 1, ''),
(51, 1, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410814581', NULL, 29, NULL, 'sold', 1, ''),
(52, 2, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410814581', NULL, 29, NULL, 'sold', 1, ''),
(53, 3, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410814581', NULL, 29, NULL, 'sold', 1, ''),
(54, 12, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410814581', NULL, 29, NULL, 'sold', 1, ''),
(55, 16, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '1410814581', NULL, 29, NULL, 'sold', 1, ''),
(134, 12, 7, '{"each":0,"global":0}', 0.00, 715970.00, 'cash', '000200201', NULL, 51, NULL, 'sold', 1, '1414040983'),
(130, 12, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000190201', NULL, 50, NULL, 'sold', 1, '1413979196'),
(131, 13, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000190201', NULL, 50, NULL, 'sold', 1, '1413979196'),
(132, 19, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000190201', NULL, 50, NULL, 'sold', 1, '1413979196'),
(133, 20, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000190201', NULL, 50, NULL, 'sold', 1, '1413979196'),
(137, 26, 1, '{"each":0,"global":0}', 0.00, 715970.00, 'cash', '000200201', NULL, 51, NULL, 'sold', 1, '1414040983'),
(136, 19, 4, '{"each":0,"global":0}', 0.00, 715970.00, 'cash', '000200201', NULL, 51, NULL, 'sold', 1, '1414040983'),
(135, 13, 3, '{"each":0,"global":0}', 0.00, 715970.00, 'cash', '000200201', NULL, 51, NULL, 'sold', 1, '1414040983'),
(129, 11, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000190201', NULL, 50, NULL, 'sold', 1, '1413979196'),
(128, 26, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000180201', NULL, 49, NULL, 'sold', 1, '1413979105'),
(127, 25, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000180201', NULL, 49, NULL, 'sold', 1, '1413979105'),
(80, 15, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000050201', NULL, 39, NULL, 'sold', 1, '1411060561'),
(81, 19, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000050201', NULL, 39, NULL, 'sold', 1, '1411060561'),
(82, 15, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000060201', NULL, 40, NULL, 'sold', 1, '1411710800'),
(83, 16, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000060201', NULL, 40, NULL, 'sold', 1, '1411710800'),
(84, 19, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000060201', NULL, 40, NULL, 'sold', 1, '1411710800'),
(85, 7, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000070201', NULL, 5, NULL, 'sold', 1, '1411710870'),
(86, 9, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000070201', NULL, 5, NULL, 'sold', 1, '1411710870'),
(87, 11, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000070201', NULL, 5, NULL, 'sold', 1, '1411710870'),
(88, 13, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000070201', NULL, 5, NULL, 'sold', 1, '1411710870'),
(89, 26, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000070201', NULL, 5, NULL, 'sold', 1, '1411710870'),
(90, 15, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000080201', NULL, 1, NULL, 'sold', 1, '1411711114'),
(91, 16, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000080201', NULL, 1, NULL, 'sold', 1, '1411711114'),
(92, 11, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000090201', NULL, 5, NULL, 'sold', 1, '1411717861'),
(93, 24, 8, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000090201', NULL, 5, NULL, 'sold', 1, '1411717861'),
(94, 30, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000090201', NULL, 5, NULL, 'sold', 1, '1411717861'),
(95, 3, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000100201', NULL, 41, NULL, 'sold', 1, '1413534409'),
(96, 8, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000100201', NULL, 41, NULL, 'sold', 1, '1413534409'),
(97, 15, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000100201', NULL, 41, NULL, 'sold', 1, '1413534409'),
(98, 21, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000100201', NULL, 41, NULL, 'sold', 1, '1413534409'),
(99, 28, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000100201', NULL, 41, NULL, 'sold', 1, '1413534409'),
(100, 3, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000110201', NULL, 42, NULL, 'sold', 1, '1413701273'),
(101, 12, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000110201', NULL, 42, NULL, 'sold', 1, '1413701273'),
(102, 16, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000110201', NULL, 42, NULL, 'sold', 1, '1413701273'),
(107, 4, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000130201', NULL, 44, NULL, 'sold', 1, '1413785815'),
(108, 30, 2, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000130201', '["aaa","bbb"]', 44, NULL, 'sold', 1, '1413785815'),
(109, 31, 3, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000130201', NULL, 44, NULL, 'sold', 1, '1413785815'),
(126, 23, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000180201', NULL, 49, NULL, 'sold', 1, '1413979105'),
(125, 12, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000180201', NULL, 49, NULL, 'sold', 1, '1413979105'),
(124, 11, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000170201', NULL, 48, NULL, 'sold', 1, '1413979080'),
(123, 9, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000170201', NULL, 48, NULL, 'sold', 1, '1413979080'),
(122, 5, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '000170201', NULL, 48, NULL, 'sold', 1, '1413979080'),
(138, 13, 1, '{"each":0,"global":0}', 0.00, 65040.00, 'cash', '000210201', NULL, 52, NULL, 'sold', 1, '1414041263'),
(139, 19, 1, '{"each":0,"global":0}', 0.00, 65040.00, 'cash', '000210201', NULL, 52, NULL, 'sold', 1, '1414041263'),
(140, 13, 1, '{"each":0,"global":0}', 0.00, 65040.00, 'cash', '000220201', NULL, 53, NULL, 'sold', 1, '1414167988'),
(141, 19, 1, '{"each":0,"global":0}', 0.00, 65040.00, 'cash', '000220201', NULL, 53, NULL, 'sold', 1, '1414167989'),
(142, 7, 1, '{"each":0,"global":0}', 0.00, 135850.00, 'cash', '000230201', NULL, 54, NULL, 'sold', 1, '1414169826'),
(143, 12, 1, '{"each":0,"global":0}', 0.00, 135850.00, 'cash', '000230201', NULL, 54, NULL, 'sold', 1, '1414169826'),
(144, 24, 1, '{"each":0,"global":0}', 0.00, 135850.00, 'cash', '000230201', NULL, 54, NULL, 'sold', 1, '1414169826'),
(145, 12, 1, '{"each":0,"global":0}', 0.00, 66700.00, 'cash', '000240201', NULL, 55, NULL, 'sold', 1, '1414266086'),
(146, 25, 1, '{"each":0,"global":0}', 0.00, 66700.00, 'cash', '000240201', NULL, 55, NULL, 'sold', 1, '1414266086'),
(147, 26, 1, '{"each":0,"global":0}', 0.00, 66700.00, 'cash', '000240201', NULL, 55, NULL, 'sold', 1, '1414266086'),
(148, 13, 1, '{"each":0,"global":0}', 0.00, 65040.00, 'cash', '000250201', NULL, 56, NULL, 'sold', 1, '1414397189'),
(149, 19, 1, '{"each":0,"global":0}', 0.00, 65040.00, 'cash', '000250201', NULL, 56, NULL, 'sold', 1, '1414397189'),
(150, 3, 1, '{"each":0,"global":10}', 0.00, 24300.00, 'cash', '000260201', NULL, 57, NULL, 'sold', 1, '1414418816'),
(151, 4, 1, '{"each":0,"global":10}', 0.00, 24300.00, 'cash', '000260201', NULL, 57, NULL, 'sold', 1, '1414418816'),
(152, 1, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(153, 2, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(154, 3, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(155, 4, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(156, 7, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(157, 11, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(158, 12, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(159, 13, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(160, 14, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(161, 15, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(162, 16, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(163, 19, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(164, 23, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(165, 24, 20, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1415419495'),
(166, 25, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1414419495'),
(167, 26, 1, '{"each":0,"global":22}', 0.00, 368113.20, 'cash', '000270201', NULL, 58, NULL, 'sold', 1, '1416419495'),
(168, 4, 1, '{"each":0,"global":0}', 0.00, 139500.00, 'cash', '000280201', NULL, 59, NULL, 'sold', 1, '1414678317'),
(169, 5, 1, '{"each":0,"global":0}', 0.00, 139500.00, 'cash', '000280201', NULL, 59, NULL, 'sold', 1, '1414678317'),
(170, 7, 1, '{"each":0,"global":0}', 0.00, 139500.00, 'cash', '000280201', NULL, 59, NULL, 'sold', 1, '1417178317'),
(389, 19, 1, '{"each":0,"global":0}', 0.00, 66000.00, 'cash', '001200204', NULL, 156, '', 'sold', 4, '1417498921'),
(390, 20, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '001210204', NULL, 157, '', 'sold', 4, '1417499032'),
(391, 20, 1, '{"each":0,"global":0}', 0.00, 0.00, 'cash', '001220204', NULL, 158, '', 'sold', 4, '1417499103'),
(392, 12, 1, '{"each":0,"global":8}', 0.00, 0.00, 'cash', '001230204', NULL, 159, NULL, 'backorder', 4, '1417499765'),
(393, 13, 1, '{"each":0,"global":8}', 0.00, 0.00, 'cash', '001230204', NULL, 159, NULL, 'backorder', 4, '1417499765'),
(394, 23, 1, '{"each":0,"global":8}', 0.00, 0.00, 'cash', '001230204', NULL, 159, NULL, 'backorder', 4, '1417499765'),
(404, 23, 2, '{"each":0,"global":0}', 0.00, 17000.00, 'cash', '001260204', NULL, 162, '', 'sold', 4, '1417670784'),
(403, 5, 6, '{"each":0,"global":0}', 0.00, 17000.00, 'cash', '001260204', NULL, 162, '', 'sold', 4, '1417670784'),
(402, 11, 11, '{"each":0,"global":0}', 0.00, 6500.00, 'cash', '001250204', NULL, 161, '', 'sold', 4, '1417670381'),
(401, 9, 6, '{"each":0,"global":0}', 0.00, 6500.00, 'cash', '001250204', NULL, 161, '', 'sold', 4, '1417670381'),
(400, 40, 1, '{"each":0,"global":0}', 0.00, 66000.00, 'cash', '001240204', NULL, 160, '', 'sold', 4, '1417593866'),
(399, 12, 1, '{"each":0,"global":0}', 0.00, 66000.00, 'cash', '001240204', NULL, 160, '', 'sold', 4, '1417593866'),
(398, 11, 1, '{"each":0,"global":0}', 0.00, 66000.00, 'cash', '001240204', NULL, 160, '', 'sold', 4, '1417593866'),
(397, 39, 1, '{"each":0,"global":8}', 0.00, 0.00, 'cash', '001230204', NULL, 159, NULL, 'backorder', 4, '1417499765'),
(396, 32, 1, '{"each":0,"global":8}', 0.00, 0.00, 'cash', '001230204', NULL, 159, NULL, 'backorder', 4, '1417499765'),
(395, 26, 1, '{"each":0,"global":8}', 0.00, 0.00, 'cash', '001230204', NULL, 159, NULL, 'backorder', 4, '1417499765'),
(388, 13, 1, '{"each":0,"global":0}', 0.00, 66000.00, 'cash', '001200204', NULL, 156, '', 'sold', 4, '1417498921'),
(387, 11, 1, '{"each":0,"global":0}', 0.00, 66000.00, 'cash', '001200204', NULL, 156, '', 'sold', 4, '1417498921'),
(386, 9, 1, '{"each":0,"global":0}', 0.00, 66000.00, 'cash', '001200204', NULL, 156, '', 'sold', 4, '1417498921'),
(385, 13, 1, '{"each":0,"global":0}', 0.00, 3540.00, 'cash', '001190204', NULL, 155, '', 'sold', 4, '1417425821'),
(373, 16, 1, '{"each":0,"global":0}', 0.00, 38700.00, 'cash', '001110204', NULL, 147, '', 'sold', 4, '1417334919'),
(372, 11, 1, '{"each":0,"global":0}', 0.00, 38700.00, 'cash', '001110204', NULL, 147, '', 'sold', 4, '1417234919'),
(371, 5, 1, '{"each":0,"global":0}', 0.00, 38700.00, 'cash', '001110204', NULL, 147, '', 'sold', 4, '1417320393'),
(384, 11, 10, '{"each":0,"global":0}', 0.00, 3540.00, 'cash', '001190204', NULL, 155, '', 'sold', 4, '1417425821');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `salesID` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(250) NOT NULL,
  `category` varchar(25) NOT NULL,
  `qty` int(11) NOT NULL,
  `sprice` decimal(10,2) NOT NULL,
  `cprice` decimal(10,2) NOT NULL,
  `discount` varchar(150) DEFAULT NULL,
  `invoice` varchar(15) NOT NULL,
  `serialNo` text,
  `customer` varchar(75) DEFAULT NULL,
  `seller` varchar(75) NOT NULL,
  `usrID` int(25) NOT NULL,
  `store` varchar(25) NOT NULL,
  `synID` int(11) NOT NULL,
  `date` varchar(25) NOT NULL,
  PRIMARY KEY (`salesID`),
  KEY `typeID` (`product`),
  KEY `customerID` (`customer`),
  KEY `agentID` (`seller`),
  KEY `sellerID` (`usrID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`salesID`, `product`, `category`, `qty`, `sprice`, `cprice`, `discount`, `invoice`, `serialNo`, `customer`, `seller`, `usrID`, `store`, `synID`, `date`) VALUES
(1, 'Blackberry z10', 'Phone', 1, 0.00, 300.00, '{"each":0,"global":10}', '000000202', NULL, 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094202'),
(2, 'Eleganza kettle', 'home appliace', 1, 700.00, 500.00, '{"each":0,"global":10}', '000000202', NULL, 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094202'),
(3, 'Eleganza kettle', 'home appliace', 1, 700.00, 500.00, '{"each":0,"global":0}', '000020202', NULL, 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094640'),
(4, 'Eleganza kettle', 'home appliace', 1, 2000.00, 1500.00, '{"each":0,"global":10}', '000000202', '["vvv"]', 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094202'),
(5, 'Eleganza kettle', 'home appliace', 1, 2500.00, 2000.00, '{"each":0,"global":10}', '000000202', NULL, 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094202'),
(6, 'canon m77', 'camera', 1, 70000.00, 56000.00, '{"each":0,"global":10}', '000000202', NULL, 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094202'),
(7, 'Nestle golden morn', 'Fast food', 5, 400.00, 370.00, '{"each":0,"global":10}', '000000202', '["sss","","","",""]', 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094202'),
(8, 'Reckitt benckiser Air wick', 'Deodourant', 1, 350.00, 300.00, '{"each":0,"global":10}', '000000202', '["aaa"]', 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094202'),
(9, 'Reckitt benckiser Air wick', 'Deodourant', 1, 350.00, 300.00, '{"each":0,"global":0}', '000010202', NULL, 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094581'),
(10, 'Apple iphone 6', 'Phone', 1, 65000.00, 59000.00, '{"each":0,"global":0}', '000020202', NULL, 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094640'),
(11, 'golden penny noodles', 'Fast food', 1, 40.00, 35.00, '{"each":0,"global":10}', '000000202', NULL, 'Anonymous', 'Adewuyi Adepoju', 2, 'Queen shopping mall', 2, '1415094202');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `stockID` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) NOT NULL,
  `SKU` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `imgID` int(11) NOT NULL,
  `cprice` decimal(11,2) NOT NULL,
  `sprice` decimal(11,2) NOT NULL,
  `date` varchar(25) NOT NULL,
  PRIMARY KEY (`stockID`),
  UNIQUE KEY `productID` (`productID`,`cprice`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stockID`, `productID`, `SKU`, `qty`, `imgID`, `cprice`, `sprice`, `date`) VALUES
(1, 2, '', -4, 57, 77000.00, 100000.00, ''),
(2, 1, '', -5, 51, 19000.00, 30000.00, ''),
(3, 3, '', 0, 63, 19000.00, 25000.00, ''),
(4, 4, '', 0, 131, 1500.00, 2000.00, ''),
(5, 4, '', 775, 50, 2000.00, 2500.00, ''),
(6, 1, '', 59, 55, 0.00, 0.00, ''),
(7, 5, '', -1, 134, 56000.00, 70000.00, ''),
(8, 6, '', -3, 66, 0.00, 0.00, ''),
(9, 7, '', 99, 65, 370.00, 400.00, ''),
(10, 8, '', -2, 58, 0.00, 0.00, ''),
(11, 9, '', 14, 67, 300.00, 350.00, ''),
(12, 10, '', 275, 87, 59000.00, 65000.00, ''),
(13, 11, '', 23, 59, 35.00, 40.00, ''),
(14, 3, '', -1, 52, 27000.00, 35000.00, ''),
(15, 12, '', -1, 52, 19000.00, 25000.00, ''),
(16, 1, '', 11, 51, 30000.00, 35000.00, ''),
(17, 1, '', 0, 56, 2000.00, 10000.00, ''),
(18, 7, '', 2584, 79, 300.00, 600.00, ''),
(19, 13, '', 125, 96, 59000.00, 65000.00, ''),
(20, 10, '', 82, 129, 0.00, 0.00, ''),
(21, 4, '', 0, 133, 0.00, 0.00, ''),
(22, 1, '', 65, 55, 25000.00, 0.00, ''),
(23, 7, '', 6, 107, 700.00, 850.00, ''),
(24, 7, '', 0, 79, 750.00, 850.00, ''),
(25, 7, '', 0, 79, 780.00, 850.00, ''),
(26, 7, '', 8, 79, 800.00, 850.00, ''),
(27, 7, '', 1, 79, 0.00, 0.00, ''),
(28, 2, '', -1, 57, 300.00, 0.00, ''),
(29, 1, '', -8, 51, 200.00, 0.00, ''),
(30, 4, '', 1, 136, 500.00, 700.00, '1417362310'),
(31, 4, '', 82, 141, 200.00, 300.00, '1415464735'),
(32, 14, '', 12, 143, 500.00, 700.00, '1415465259'),
(33, 14, '', 0, 143, 200.00, 300.00, '1415465776'),
(34, 1, '', 10, 155, 22000.00, 24500.00, '1417683430'),
(35, 9, '', 1, 146, 230.00, 350.00, '1415483463'),
(36, 9, '', 13, 67, 200.00, 300.00, '1415531332'),
(37, 15, '', 3, 147, 500.00, 800.00, '1415531167'),
(38, 16, '', -2, 148, 79000.00, 112000.00, '1415531271'),
(39, 17, '', 1, 150, 79000.00, 120000.00, '1415594472'),
(40, 18, '', 4, 151, 50.00, 70.00, '1415594475'),
(41, 19, '', 0, 152, 900.00, 1000.00, '1415598038'),
(42, 20, '', 0, 129, 200.00, 700.00, '1415607318'),
(43, 17, '', 0, 148, 50000.00, 70000.00, '1415607740'),
(44, 21, '', 0, 154, 22000.00, 24500.00, '1416727154'),
(45, 1, 'N00008Ph', 17, 155, 21000.00, 24500.00, '1417683721'),
(46, 22, 'N00001C5', 12, 56, 20000.00, 25500.00, '1417683908'),
(47, 22, 'N00002C5', 2, 56, 21000.00, 25500.00, '1417683955'),
(48, 1, 'N00009XL', 20, 155, 20000.00, 25500.00, '1417684066'),
(49, 22, 'N003C5', 15, 56, 19000.00, 20000.00, '1417684215'),
(50, 23, 'N001X2', 5, 155, 20000.00, 22500.00, '1417686310'),
(51, 24, 'N001X1', 5, 155, 19000.00, 21000.00, '1417686605'),
(52, 25, 'N001X0', 2, 164, 15000.00, 17000.00, '1417687121'),
(53, 26, 'M001Lu', 15, 165, 27000.00, 35000.00, '1417687335');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE IF NOT EXISTS `store` (
  `storeID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `synKey` varchar(25) NOT NULL,
  `usrID` varchar(25) NOT NULL,
  `pwd` varchar(25) NOT NULL,
  `expiry` datetime NOT NULL,
  `date` varchar(25) NOT NULL,
  PRIMARY KEY (`storeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`storeID`, `name`, `synKey`, `usrID`, `pwd`, `expiry`, `date`) VALUES
(1, 'superiormobile', '', '', '', '0000-00-00 00:00:00', ''),
(2, 'blint', 'xyz', '', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `supplierID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `addr` text,
  `usrID` int(11) NOT NULL,
  `date` varchar(25) NOT NULL,
  PRIMARY KEY (`supplierID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`),
  KEY `usrID` (`usrID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=141 ;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplierID`, `name`, `phone`, `email`, `addr`, `usrID`, `date`) VALUES
(1, 'Attemptexams', '2348034309999', 'aliu.adepoju@gmail.com', '    ', 4, '1406726960'),
(3, 'Progal Initiative', '2380223232', NULL, '    ', 4, '1407163981'),
(4, 'Safeway', NULL, NULL, '    ', 1, '1407164575'),
(5, 'Laby B', NULL, 'ladyb@gmail.com', 'Awoyaya', 4, '1407933208'),
(115, 'pointandbuy', NULL, 'info@pab.com', '    ', 4, ''),
(123, 'pointandbuy', NULL, NULL, '    ', 4, ''),
(126, 'pointandbuy', NULL, NULL, '    ', 4, ''),
(127, 'Laby B', NULL, NULL, '    ', 4, ''),
(128, 'Adewuyi', NULL, NULL, '    ', 4, ''),
(129, 'pointandbuy', '+234 8034309999', 'info@progal.com', '   gfffnvnvhvhjvhvhjv', 4, ''),
(130, 'Progal Initiative', NULL, NULL, '    ', 2, ''),
(131, 'Attemptexams', NULL, NULL, '    ', 2, ''),
(132, 'Progal Initiative', NULL, NULL, '    ', 2, ''),
(133, 'Progal Initiative', NULL, NULL, '    ', 2, ''),
(134, 'Progal Initiative', NULL, NULL, '    ', 2, ''),
(135, 'Christain', NULL, NULL, '    ', 2, ''),
(136, 'Christain', NULL, NULL, '    ', 2, ''),
(137, 'Christain', NULL, NULL, '    ', 2, ''),
(138, 'Christain', NULL, NULL, '    ', 2, ''),
(139, 'Christain', NULL, NULL, '    ', 2, ''),
(140, 'Progal Initiative', NULL, NULL, '    ', 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `sync`
--

CREATE TABLE IF NOT EXISTS `sync` (
  `syncID` int(11) NOT NULL AUTO_INCREMENT,
  `auto_sync` tinyint(1) NOT NULL DEFAULT '1',
  `syncKey` varchar(250) NOT NULL,
  PRIMARY KEY (`syncID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sync`
--

INSERT INTO `sync` (`syncID`, `auto_sync`, `syncKey`) VALUES
(1, 1, 'xyz');

-- --------------------------------------------------------

--
-- Table structure for table `usr`
--

CREATE TABLE IF NOT EXISTS `usr` (
  `usrID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(125) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `pwd` varchar(50) NOT NULL,
  `storeID` int(11) DEFAULT NULL,
  `roleID` int(11) NOT NULL,
  `date` varchar(20) NOT NULL,
  PRIMARY KEY (`usrID`),
  KEY `storeID` (`storeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `usr`
--

INSERT INTO `usr` (`usrID`, `name`, `email`, `phone`, `pwd`, `storeID`, `roleID`, `date`) VALUES
(1, 'Bisola Adepoju', 'bisola.adepoju@gmail.com', '2348058322957', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1, 3, '1398079703'),
(2, 'Adewuyi Adepoju', 'aliu.adepoju@gmail.com', '2348034309999', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 2, 1, '1398079703'),
(3, 'Ruth Oretu', 'ruth.oretu@gmail.com', '', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 4, 2, '1407944698'),
(4, 'Anderson Obah', '', '2348032447313', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 0, 3, '1414915893');

-- --------------------------------------------------------

--
-- Stand-in structure for view `valuation`
--
CREATE TABLE IF NOT EXISTS `valuation` (
`tcp` decimal(43,2)
,`tsp` decimal(43,2)
);
-- --------------------------------------------------------

--
-- Structure for view `best_sell`
--
DROP TABLE IF EXISTS `best_sell`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `best_sell` AS select concat(`m`.`name`,' ',`p`.`name`) AS `name`,sum(`s`.`qty`) AS `freq`,(select sum(`x`.`qty`) from `stock` `x` where (`x`.`productID` = `i`.`productID`)) AS `instock` from (((`sale` `s` join `stock` `i` on(`i`.`stockID`)) join `product` `p` on(`p`.`productID`)) join `manufacturer` `m` on(`m`.`manufacturerID`)) where ((`p`.`productID` = `i`.`productID`) and (`i`.`stockID` = `s`.`stockID`) and (`m`.`manufacturerID` = `p`.`manufacturerID`) and (from_unixtime(`s`.`date`) > (now() - interval 10 day))) group by `i`.`productID` order by sum(`s`.`qty`) desc;

-- --------------------------------------------------------

--
-- Structure for view `best_sell_1m`
--
DROP TABLE IF EXISTS `best_sell_1m`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `best_sell_1m` AS select `p`.`name` AS `name`,sum(`s`.`qty`) AS `freq`,(select sum(`x`.`qty`) from `stock` `x` where (`x`.`productID` = `i`.`productID`)) AS `instock` from ((`sale` `s` join `stock` `i` on(`i`.`stockID`)) join `product` `p` on(`p`.`productID`)) where ((`p`.`productID` = `i`.`productID`) and (`i`.`stockID` = `s`.`stockID`) and (from_unixtime(`s`.`date`) > (now() - interval 1 month))) group by `i`.`productID` order by sum(`s`.`qty`) desc;

-- --------------------------------------------------------

--
-- Structure for view `most_sold`
--
DROP TABLE IF EXISTS `most_sold`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `most_sold` AS select `p`.`name` AS `name`,`i`.`productID` AS `productID`,sum(`s`.`qty`) AS `freq` from ((`sale` `s` join `stock` `i` on(`i`.`stockID`)) join `product` `p` on(`p`.`productID`)) where ((`p`.`productID` = `i`.`productID`) and (`i`.`stockID` = `s`.`stockID`)) group by `i`.`productID` order by sum(`s`.`qty`) desc;

-- --------------------------------------------------------

--
-- Structure for view `outofstock`
--
DROP TABLE IF EXISTS `outofstock`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `outofstock` AS select `c`.`name` AS `cname`,concat(`m`.`name`,' ',`p`.`name`) AS `pname`,`s`.`cprice` AS `cprice` from (((`stock` `s` join `category` `c` on(`c`.`categoryID`)) join `product` `p` on(`p`.`productID`)) join `manufacturer` `m` on(`m`.`manufacturerID`)) where ((`c`.`categoryID` = `p`.`categoryID`) and (`p`.`productID` = `s`.`productID`) and (`m`.`manufacturerID` = `p`.`manufacturerID`) and (`s`.`qty` <= 0));

-- --------------------------------------------------------

--
-- Structure for view `reorderlevel`
--
DROP TABLE IF EXISTS `reorderlevel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reorderlevel` AS select `c`.`name` AS `cname`,concat(`m`.`name`,' ',`p`.`name`) AS `pname`,`s`.`cprice` AS `cprice`,`s`.`qty` AS `qty` from (((`stock` `s` join `category` `c` on(`c`.`categoryID`)) join `product` `p` on(`p`.`productID`)) join `manufacturer` `m` on(`m`.`manufacturerID`)) where ((`c`.`categoryID` = `p`.`categoryID`) and (`p`.`productID` = `s`.`productID`) and (`m`.`manufacturerID` = `p`.`manufacturerID`) and (`s`.`qty` <= `p`.`reorder`));

-- --------------------------------------------------------

--
-- Structure for view `valuation`
--
DROP TABLE IF EXISTS `valuation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `valuation` AS select sum((`s`.`qty` * `s`.`cprice`)) AS `tcp`,sum((`s`.`qty` * `s`.`sprice`)) AS `tsp` from `stock` `s`;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `invoice_prefix` ON SCHEDULE EVERY 1 DAY STARTS '2013-02-25 11:55:56' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE blint.invoice set prefix=lpad(round(rand()*100),3,0) where id=1$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
