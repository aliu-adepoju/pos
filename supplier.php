<?php
session_start();
session_regenerate_id(); 
if(!isset($_SESSION['usrID'])){
	header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'controllers/__supplier.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="";
switch (@$_GET["p"]) {
	case "add":
	case "edit":
		$layout->content(NULL,"view/supplier/_edit.php");
		break;
	default:
		$layout->content("view/supplier/_toolbar.php","view/supplier/_body.php");
		break;
}