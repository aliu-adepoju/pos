<?php
mt_srand((double)microtime()*1000000);
class RandNumGen{
	public function ranChar(){
		$charset=implode('',array_merge(range(0,9),range('a','z'),range('A','Z')));
		return $charset[mt_rand(0,(strlen($charset)-1))];
	}
	public function init($len=6){
		$retrn="";
		for ($i=0;$i<$len;$i++){
			$retrn  .= $this->ranChar();
		}
		return($retrn);
	}
}
?>