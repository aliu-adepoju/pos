<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inventory</title>    
    <link href="css/styles.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container">
	<div class="logo"><?=@$_GET["store"];?></div>
	<div class="login-container">
            <div class="form-box">
            <?php 
            if(isset($_GET["auth"])){
            ?>
            <div class="alert alert-warning"><?=$_GET["auth"];?></div>
            <?php 
            }
            ?>
                 <form action="controllers/__rlogin.php" method="post">
      <h4 class="text-left">Login </h4>
<div class="row form-group no-margin">
<div class="col-md-12 no-padding">
<input type="text" name="email" onmouseover="this.focus()" class="form-control" placeholder="Email or Mobile number" >
</div>
</div>

<div class="row form-group no-margin">
<div class="col-md-12 no-padding">
<input type="password" name="pwd" onmouseover="this.focus()" class="form-control" placeholder="********"/>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-12 no-padding">
<button type="submit" name="login" class="btn btn-primary btn-block login-button">Login</button>
</div>
</div>

</form>
            </div>
        </div>
        
	</div>
  </body>
</html>