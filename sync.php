<?php
session_start();
session_regenerate_id(); 
if(!isset($_SESSION['usrID'])){
	header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'controllers/__sync.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="";
$layout->content(NULL,"sync/_body.php");
?>  