<?php
session_start();
session_regenerate_id(); 
if(!isset($_SESSION['usrID'])){
	header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'controllers/__user.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="PointAndBuy | Users";
if(!in_array($_SESSION["roleID"],array(1,2))){
	header("Location: dashboard.php?auth=Access denied");
	exit(0);
}
switch (@$_GET["p"]) {
	case "add":
	case "edit":
		$layout->content(NULL,"view/user/_edit.php");
	break;
	default:
		$layout->content("view/user/_toolbar.php","view/user/_body.php");
	break;
} 