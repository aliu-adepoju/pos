<?php
ini_set("display_errors",1); 
include_once '../config.php';
$search_word=strtolower(trim($_POST['data']));
$keywords=array_map(function($arg){ return "name like '{$arg}%'";},explode(" ",$search_word));
$keywords=implode(" or ",$keywords);
$tb=explode("_",$_GET["tb"])[0];
$search=$pdo->query("select * from {$tb} where ($keywords)");
$i=0;
list($words,$distants,$sorted_words)=array(array(),array(),array());
foreach($search->fetchAll(PDO::FETCH_ASSOC) as $fetch){
	$d=similar_text($search_word,$fetch['name']);
	$distants[]=$d;
	$words[]=$fetch;
}

$xdistants=$distants;
rsort($distants);
	
function array_map_keys($arr1,$arr2){
	$keys=array();
	$k=0;
	for($i=0;$i<count($arr2);$i++){
		for($j=0;$j<count($arr1);$j++){
			if($arr1[$i]==$arr2[$j]) {
				if(!in_array($j,$keys)){
					$keys[$k++]=$j;
				}
			}
		}
	}
	return $keys;
}

foreach (array_map_keys($distants,$xdistants) as $v){
	$sorted_words[]=$words[$v];
}
echo json_encode((count($sorted_words)==0)?(array(array("name"=>$search_word))):(array_values($sorted_words)));
?>