<?php
$isDeleted=false;
if(isset($_POST["del"]) && !empty($_POST["del"])){
	$invoice=$_POST["del"];
	try{
		$pdo->beginTransaction();
		foreach ($pdo->query("select stockID,qty from sale where invoice='{$invoice}' and status='sold'") as $fetch){
			$pdo->exec("update stock set qty=qty+{$fetch['qty']} where stockID={$fetch['stockID']}");
		}
		$note=$pdo->quote($_POST["note"]);
		$pdo->exec("update sale set status='refund',note=$note where invoice='{$invoice}'");
		$isDeleted=true;
		$pdo->commit();
	}
	catch (PDOException $ex){
		$pdo->rollBack();
	}
	
}
