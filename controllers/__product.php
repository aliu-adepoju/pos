<?php
$isOk=false;
if (isset($_POST["product"])) {
	try{
		if(empty($_POST["name"])) throw new Exception(NULL,1);
		if(isset($_GET["p"]) && $_GET["p"]=="edit"){
			$product=$pdo->prepare("update product set name=:name,reorder=:reorder where productID=:productID");
			$product->execute(array("name"=>$_POST["name"],"reorder"=>$_POST["reorder"],"productID"=>intval($_POST["id"])));
		}
		else{
			$product=$pdo->prepare("insert into product set name=:name,reorder=:reorder");
			$product->execute(array("name"=>$_POST["name"],"reorder"=>$_POST["reorder"]));
		}
		$isOk=true;
	}
	catch (PDOException $e) {
		$err=$e->getMessage();
		
	}
}