<?php
/* print("<pre>");
print_r($_POST);
print("</pre>"); */
ini_set("display_errors",1);
session_start();
session_regenerate_id(); 
if(!isset($_SESSION['usrID'])){
	//header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include '../config.php';
$profile=$pdo->query("select * from profile where profileID=1");
$profile=$profile->fetch(PDO::FETCH_ASSOC);

$config=$pdo->query("select item_desc,invoice_printer from config where configID=1");
$config=array_merge($profile,$config->fetch(PDO::FETCH_ASSOC));

$reload=1;
$hasInvoice=false;
$data=json_decode($_POST["data"],1);
$customer_data=array_combine(array("name","email","phone","addr"), $data["customer"]);
$customer_data=rmJsNull($customer_data);
/* print("<pre>");
print_r($config);
print_r($_POST);
print_r($data);
//print_r($data["items"]);
//print_r($customer_data);
print("</pre>");  */
//exit(0);
function rmJsNull($arr){
	return array_map(function($v){ return (trim($v)=='null' || empty($v))?(NULL):(trim($v));},$arr);
}

setcookie("transID",(isset($_COOKIE['transID']))?(intval($_COOKIE['transID'])+1):(0),time()+60*60*24*365,"/");

$transID=$_COOKIE['transID'];
$invoice=sprintf("%05d%02d%02d",$transID,$config["storeID"],$usrID);

$discount=$data["discount"];
try{
$pdo->beginTransaction();
if(!empty($customer_data['email'])){
	if(!filter_var($customer_data['email'],FILTER_VALIDATE_EMAIL))
		Throw new PDOException("Invalid email address");
		$customer_data["email"]=filter_var($customer_data['email'],FILTER_SANITIZE_EMAIL);
}
if (!empty($customer_data["phone"])) $customer_data["phone"]=$customer_data["phone"];

$customer=$pdo->prepare("insert into customer set name=:name,phone=:phone,email=:email,addr=:addr,date=unix_timestamp() on duplicate key update customerID=LAST_INSERT_ID(customerID),name=:name,phone=:phone,email=:email,addr=:addr");
$customer->execute($customer_data);
$customerID=$pdo->lastInsertId();

if(isset($_GET["task"]) && $_GET["task"]=="backorder"){
	$sale=$pdo->prepare("insert into sale set stockID=:stockID,qty=:qty,discount=:discount,paid=:paid,invoice=:invoice,serialNo=:serialNo,customerID=:customerID,status='backorder',usrID=:usrID,date=unix_timestamp()");
	foreach ($data["items"] as $stockID=>$arr){
		$qty=$arr[3];
		$sale->execute(array("stockID"=>$stockID,"qty"=>$qty,"discount"=>json_encode(array("each"=>$discount["each"][$stockID],"global"=>$discount["global"])),"paid"=>0.00,"invoice"=>$invoice,"serialNo"=>(array_key_exists($stockID,$data["serialNo"]))?(json_encode($data["serialNo"][$stockID])):(NULL),"customerID"=>$customerID,"usrID"=>$usrID));
	}
	
	echo "<script>window.parent.Cart.BackOrder.success($invoice);</script>";
	$pdo->commit();
	exit(0);
}



$sale=$pdo->prepare("insert into sale set stockID=:stockID,qty=:qty,discount=:discount,deduct=:deduct,paid=:paid,via=:via,invoice=:invoice,serialNo=:serialNo,customerID=:customerID,note=:note,usrID=:usrID,date=unix_timestamp()");
foreach ($data["items"] as $stockID=>$arr){
	$qty=$arr[3];
	$deduct=($discount["each"][$stockID]+(($arr[2]*$discount["global"])/100))*$qty;
	$sale->execute(array("stockID"=>$stockID,"qty"=>$qty,"discount"=>json_encode(array("each"=>$discount["each"][$stockID],"global"=>$discount["global"])),"deduct"=>$deduct,"paid"=>$data["paid"],"via"=>$_POST["via"],"invoice"=>$invoice,"serialNo"=>(array_key_exists($stockID,$data["serialNo"]))?(json_encode($data["serialNo"][$stockID])):(NULL),"customerID"=>$customerID,"note"=>$_POST["note"],"usrID"=>$usrID));
	$pdo->exec("update stock set qty=qty-$qty where stockID=$stockID");
} 
if($data["print"]==1){ 
$hasInvoice=true;
ob_start();
include ($config["invoice_printer"]=="standard")?("../invoice.standard.php"):("../invoice.mini.php");
echo ob_get_clean();
}

if(!$hasInvoice) echo "<script>window.parent.Cart.Payment.success($invoice);</script>";
$pdo->commit();
}
catch (PDOException $ex){
	$pdo->rollBack();
	echo $err=$ex->getMessage();
echo "<script>window.parent.Cart.Payment.error('$err');</script>";
}
?>