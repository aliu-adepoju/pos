<?php
session_start();
session_regenerate_id();
ini_set("display_errors",1);
if(isset($_POST['login'])){
	include_once '../config.php';
	include '../lib/SignInController.php';
	$sic=new SignInController($pdo,$_POST,array("email","pwd"));
	if($sic->isOk){
		list($_SESSION['usrID'],$_SESSION['name'],$_SESSION['roleID'])=$sic->profile;
		header("Location: ../dashboard.php");
		exit(0);
	}
	else {
		$err=$sic->err;
		header("Location: ../index.php?auth={$err}");
		exit(0);
	}
}