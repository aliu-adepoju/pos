<?php
$isOk=false;
if (isset($_POST["supplier"])) {
	try{
		if(empty($_POST["name"])) throw new Exception(NULL,1);
		if(isset($_GET["p"]) && $_GET["p"]=="edit"){
			$supplier=$pdo->prepare("update supplier set name=:name,phone=:phone,email=:email,addr=:addr where supplierID=:supplierID");
			$supplier->execute(array("name"=>$_POST["name"],"phone"=>$_POST["phone"],"email"=>$_POST["email"],"addr"=>$_POST["addr"],"supplierID"=>intval($_POST["id"])));
		}
		else{
			$supplier=$pdo->prepare("insert into supplier set name=:name,email=:email,phone=:phone,addr=:addr,date=unix_timestamp()");
			$supplier->execute(array("name"=>$_POST["name"],"email"=>$_POST["email"],"phone"=>$_POST["phone"],"addr"=>$_POST["addr"]));
		}
		$isOk=true;
	}
	catch (PDOException $e) {
		$err=$e->getMessage();
	}
}