<?php
include '../config.php';
ini_set("display_errors",1);
session_start();
session_regenerate_id(); 
$usrID=intval($_SESSION['usrID']);
print("<pre>");
print_r($_POST);
print("</pre>");

if(!empty($_POST['email'])){
	if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
		Throw new PDOException("Invalid Email Address");
	$email=filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
}

if (!empty($_POST["phone"])) $phone=$_POST["phone"];

function getID($tb,$data){
	global $pdo;
	$str=array();
	foreach (array_keys($data) as $col) $str[]="{$col}=:{$col}";
	$cols=join(",",$str);
	$queryID=$pdo->prepare("insert into {$tb} set {$cols} on duplicate key update {$tb}ID=LAST_INSERT_ID({$tb}ID),{$cols}");
	$queryID->execute($data);
	return $pdo->lastInsertId();
}

try{
	$pdo->beginTransaction();
	$supplier=$_POST["supplier"];
	for ($i=0;$i<count($_POST["product"]);$i++){
		$codes=NULL;
		if(isset($_POST["codes"][$i])){
			$codes=$_POST["codes"][$i];
			if (count($codes) < $_POST["qty"][$i]) Throw new PDOException("Product codes are less than it quantity");
			if(count(array_unique($codes)) < count($codes)) Throw new PDOException("Duplicate product codes found");
			$codes=json_encode($codes);
		}
		
		$imgURL=(filter_var($_POST['icon'][$i],FILTER_VALIDATE_URL))?($_POST['icon'][$i]):("../{$_POST['icon'][$i]}");
		$img_path=pathinfo($imgURL);
		if(!file_exists("../view/upload/thumbnail/{$img_path["basename"]}")) {
			include_once '../lib/smartResizeImage.php';
			include_once '../lib/RandNumGen.php';
			$rand=new RandNumGen();
			$img_path["basename"]=$rand->init(12).".png";
			copy($imgURL,"../view/upload/large/{$img_path["basename"]}");
			smart_resize_image("../view/upload/large/{$img_path["basename"]}",64,64,true,"../view/upload/thumbnail/{$img_path["basename"]}");
		}
		
		$time=time();
		$imgID=getID("img",array("name"=>$img_path["basename"]));
		$manufacturerID=getID("manufacturer",array("name"=>$_POST['manufacturer'][$i]));
		$categoryID=getID("category",array("name"=>$_POST['category'][$i]));
		$productID=getID("product",array("name"=>$_POST['product'][$i],"manufacturerID"=>$manufacturerID,"categoryID"=>$categoryID));
		$supplierID=getID("supplier",array("name"=>$_POST['supplier'],"phone"=>$phone,"email"=>$email,"addr"=>$_POST["addr"],"usrID"=>$usrID));
		$stock=$pdo->prepare("insert into stock set productID=:productID,qty=:qty,imgID=:imgID,cprice=:cprice,sprice=:sprice,date=:date on duplicate key update productID=:productID,qty=qty+:qty,imgID=:imgID,cprice=:cprice,sprice=:sprice,date=:date");
		$stock->execute(array("productID"=>$productID,"qty"=>$_POST["qty"][$i],"imgID"=>$imgID,"cprice"=>$_POST["cprice"][$i],"sprice"=>$_POST["sprice"][$i],"date"=>$time));
		$purchaseID=getID("purchase",array("stockID"=>$pdo->lastInsertId(),"codes"=>$codes,"qty"=>$_POST["qty"][$i],"supplierID"=>$supplierID,"receiverID"=>$usrID,"note"=>@$_POST["note"],"date"=>$time));
		echo "<script>window.parent.Stock.Multiple.success($purchaseID);</script>";
	}
	echo "Successful";
	$pdo->commit();
}
catch (PDOException $e) {
	$pdo->rollBack();
	$err=$e->getMessage();
	echo "<script>window.parent.Stock.Multiple.error('$err');</script>";
}