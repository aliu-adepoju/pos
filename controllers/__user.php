<?php
$isOk=false;
if (isset($_POST["usr"])) {
	try{
		if(isset($_GET["p"]) && $_GET["p"]=="edit"){
			$usr=$pdo->prepare("update usr set name=:name,phone=:phone,email=:email,roleID=:roleID,storeID=:storeID where usrID=:usrID");
			$usr->execute(array("name"=>$_POST["name"],"phone"=>$_POST["phone"],"email"=>$_POST["email"],"storeID"=>$_POST["storeID"],"roleID"=>$_POST["roleID"],"usrID"=>intval($_POST["id"])));
		}
		else{
			$usr=$pdo->prepare("insert into usr set name=:name,email=:email,phone=:phone,storeID=:storeID,roleID=:roleID,date=unix_timestamp()");
			$usr->execute(array("name"=>$_POST["name"],"email"=>$_POST["email"],"phone"=>$_POST["phone"],"storeID"=>$_POST["storeID"],"roleID"=>$_POST["roleID"]));
		}
		$isOk=true;
	}
	catch (PDOException $e) {
		echo $err=$e->getMessage();
		
	}
}