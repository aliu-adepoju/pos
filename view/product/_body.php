 <div class="clear">&nbsp;</div>
<div class="col-sm-12">
<table class="table table-striped table-bordered" id="tb_data">
<thead>
<tr><th>#</th><th></th><th>Name</th><th>Category</th><th>Description / Features</th><th>In stock</th><th>Re-order level</th><th></th></tr>
</thead>
<tbody>
<?php
$i=1; 
foreach ($this->pdo->query("select p.*,concat(m.name,' ',p.name) as pname,(select i.name from (stock s join img i on i.imgID) where i.imgID=s.imgID and s.productID=p.productID limit 1) as iname,c.name as cname,(select sum(s.qty) from stock s where p.productID=s.productID) as instock from (product p join manufacturer m on m.manufacturerID join category c on c.categoryID) where c.categoryID=p.categoryID and m.manufacturerID=p.manufacturerID") as $fetch){
?>
<tr><td><?=$i;?></td>
<td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="view/upload/thumbnail/<?=$fetch["iname"];?>" />
</a>
</td>
<td><?=$fetch["pname"];?></td><td><?=$fetch["cname"];?></td><td><?=$fetch["description"];?></td><td><?=$fetch["instock"];?></td><td><?=$fetch["reorder"];?></td><td>
<a href="product.php?p=edit&name=<?=$fetch["name"];?>&reorder=<?=$fetch["reorder"];?>&id=<?=$fetch["productID"];?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i></a>
</td></tr>
<?php 
$i++;
}
?>
</tbody>
</table>
</div>