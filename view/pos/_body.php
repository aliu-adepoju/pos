<div class="row">
<div class="col-md-8 no-padding">
<div class="row">
		<div class="col-md-12 no-padding content" id="item_box">
		<div class="row no-margin ">
    	<?php 
    	//,(select m.freq from most_sold m where m.productID=s.productID limit 1) as freq
    	
    	foreach ($this->pdo->query("select s.*,m.name as mname,p.name as pname,p.description,c.name as cname,s.SKU,i.name as iname from (stock s join manufacturer m on m.manufacturerID join product p on p.productID join category c on c.categoryID join img i on i.imgID) where m.manufacturerID=p.manufacturerID and p.productID=s.productID and c.categoryID=p.categoryID and i.imgID=s.imgID and s.qty > 0") as $k=>$data){
	    	$label="{$data["mname"]} {$data["pname"]}";
	    	?>
	    	<div class="rel col-sm-3 padding5" data-search="<?="{$data["cname"]} {$label} {$data["sprice"]} {$data["qty"]} {$data["SKU"]} {$data["description"]}";?>">
	    	<div class="panel panel-primary no-margin">
	    	<div class="panel-heading padding5">
	    	<div class="panel-body bg-color-white text-center no-padding rel">
	    	<div class="hide2" id="edge_check" data-stockID="<?=$data["stockID"];?>">
	    	<span class="absTR edge-check"></span>
	    	<i class='absTR glyphicon glyphicon-ok'></i>
	    	</div>
	    	<a href="javascript:void(0)" title="<?="$label [₦{$data["sprice"]}]";?>" style="float:left;width:100%;height:100%;padding:10px" onclick="Cart.add([<?=$data["stockID"];?>,'<?=$label;?>',<?=$data["sprice"];?>,1,'btn btn-xs fg-color-lBlack','<?=$data["description"];?>'])"><img src="view/upload/thumbnail/<?=$data["iname"];?>" alt="image-name"/></a></div>
	    	</div>
	    	<div class="panel-footer padding5">
	    	<span class="badge"><?=$data["qty"];?></span>
	    	<a href="javascript:void(0)" onclick='Cart.Preview.show(<?=json_encode($data);?>)' class='fs15' title="<?=$label;?>"><?=(strlen($label) > 13)?(substr($label,0,13)."..."):($label);?></a>
	    	</div>
	    	</div>
	    	</div>
	    	<?php 
    	}
    	
    	?>
    	</div>
    	</div>
		</div>              		              		
              	</div>
              
              	<div class="col-md-4 cart-box padding5">
              	<div class="panel panel-primary rel hide2" id="suspended_sales">
              	 <div class="panel-heading"><h4 class="no-margin">Suspended sales&nbsp;&nbsp;<a href="javascript:void(0)" onclick="Cart.Suspend.close()" class="close" title="Close">&times;</a></h4></div>
 				 <div class="panel-body padding5">
 				 <div class="input-group">
                <input type="text" class="form-control" id="tb_data_search" onmouseover="this.focus()" name="x" placeholder="Search item name or scan barcode">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
            <div class="clear"></div>
 				 <div style="overflow:auto;min-height:500px">
 				 <table class="table table-striped display" id="suspend_tb">
 				 <thead>
 				 <tr><th>#</th><th>Customer</th><th>Date</th></tr>
 				 </thead>
 				 <tbody>
 				  </tbody>
 				 </table>
 				 </div>
 				 
 				 </div>
 				 </div>
              	
              	<div class="panel panel-primary rel adj" id="shopping_cart">
              	 <div class="panel-heading"><h4 class="no-margin">Shopping Cart&nbsp;&nbsp;<span id='total_qty'></span> <span class="glyphicon glyphicon-shopping-cart pull-right"></span></h4></div>
 				 <div class="panel-body padding5">
   				 <div id="cartScroll" class="content2">
              		
              		<div id="cart_box">
              			<?php 
	              		if(isset($_GET["trans"]) && !empty($_GET["trans"])){
	              		?>
	              		<div class="alert alert-success">Transaction successful</div>
	              		<?php 
	              		}else{
              			echo "<div class='text-center'><i class='icon-empty-cart fs250'></i><br/><h1><small>Empty cart</small></h1></div>";
              			}
              			?>
						</div>
              		</div>
              			<div class="rel" style="z-index:50">
              		<div class="panel panel-primary absBL bg-color-white hide2 padding5" id="global_discount" style="bottom:5px">
              		<div class="panel-body padding5">
              		Global discount to each item in cart
              		<div class="slider" style="width:270px">
 						<div class="progress" onclick="skipSash(event)"></div>
							<div class="sash" style="position:absolute;top:0px;left:0px" onmousedown="slider(event,this)"></div>
                          </div>
                          <div class="clear">&nbsp;</div>
                          </div>
                   </div>
                   
                   <div class="panel panel-primary absBL bg-color-white hide2 padding5" id="_customer" style="bottom:5px;width:100%">
                   <div class="panel-heading">Add Customer Detail</div>
              		<div class="panel-body no-padding cart-customer">
              		<form action="javascript:void(0)" method="post" class="customer-form">
              		<div class="rel">
              		<input type="text" autocomplete="off" class="form-control" placeholder="Customer Name" value="Anonymous" name="name" id="pri_dropdown_name" onpaste="Cart.Customer.search(event,this,getObj('pri_dropdown'))" onkeyup="Cart.Customer.search(event,this,getObj('pri_dropdown'))" onchange="Cart.Customer.update()"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="pri_dropdown">
				      <ul class="navlist link"></ul>
				      </div>
				      </div>
              		<label><input type="checkbox"/>&nbsp;Mail receipt</label>
              		<input type="text" class="form-control" name="email" onchange="Cart.Customer.update()" placeholder="Email Address"/>
              		<input type="text" class="form-control" name="phone" onchange="Cart.Customer.update()" placeholder="Phone number"/>
              		<textarea class="form-control" rows="3" name="addr" onchange="Cart.Customer.update()" placeholder="Office / Home Address"></textarea>
                          <div class="clear"></div>
                          </form>
                          </div>
                   </div>
                   
                   <div class="absBR hide2" id="_calculator" style="bottom:23px">
              		<?php 
              		include_once 'widget/calc.php';
              		?>
                   </div>
                   
                    <div class="row">
                    <div class="col-xs-5 no-padding">
              		<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-block" onclick="toggle('gd',function(o){$(o).show();},function(o){$(o).hide();},$('#global_discount'))"><i class="icon-discount"></i>&nbsp;Global discount &raquo; <span id='discount_value'>0%</span></a>
              		</div>
              		 <div class="col-xs-4 no-padding">
              		<a href="javascript:void(0)" class="btn btn-xs btn-default btn-block nobl" onclick="Cart.Customer.toggle(true)"><i class="icon-plus"></i>&nbsp;Add customer</a>
              		</div>
              		<div class="col-xs-3 no-padding">
              		<a href="javascript:void(0)" class="btn btn-xs btn-default btn-block nobl" onclick="toggle('_calc',function(o){$(o).show();},function(o){$(o).hide();},$('#_calculator'))"><i class="icon-calculate"></i>&nbsp;Calculator</a>
              		</div>
              		</div>
              		<div class="clear"></div>
              		</div>
              		<div class="row bg-color-black fg-color-white rel" id="total_box">
              		<div class='col-xs-7'>
              		<h5>Sub-Total &raquo; <strong>₦0.00</strong></h5>
              		<h5>Discounts &raquo; <strong>₦0.00</strong></h5>
              		</div>
              		<div class='col-xs-5 padding5'>
              		<h3 class='no-margin'>Total <br/> <strong class='digit fg-color-success'>₦0.00</strong></h3>
              		</div>
              		</div>
              		
  </div>
  <div class="panel-footer padding5 text-center">
              			 <form action="javascript:void(0)" method="post">
              			 <input type="hidden" name="data" value="{}"/>
              			<div class="row">
              			<div class="col-xs-3 no-padding">
              			<button type="button" class="btn btn-danger btn-block" onclick="Cart.cancel()">Cancel</button>
              			</div>
              			<div class="col-xs-3 no-padding">
              			<button type="submit" class="btn btn-warning btn-block" onclick="Cart.BackOrder.process(this.form)">Back-order</button>
              			</div>
              			<div class="col-xs-3 no-padding">
              			<button type="button" class="btn btn-primary btn-block" onclick="Cart.Suspend.add()">Suspend</button>
              			</div>
              			<div class="col-xs-3 no-padding">
              			<button type="button" class="btn btn-success btn-block" onclick="Cart.Checkout.process(this.form)">Checkout</button> 
              			</div>
              		</div>
              		</form>
              		<div class="clear"></div>
  </div>
  
  <div class="absBL ae-well padding10 hide2" data-commit-sale="0" style="width:100%;z-index:50">
  <form action="javascript:void(0)" method="post">
  <a href="javascript:void(0)" class="close" onclick="Cart.Payment.hide()">&times;</a>
  <label><input type="checkbox" checked="checked" onchange="Cart.Payment.receipt(this.checked)"/>&nbsp;Print Receipt</label>
  <table class="table table-striped">
  <tbody>
  <tr><td>Pay via</td><td>
  <select name="via" class="form-control">
  <option value="cash">Cash</option>
  <option value="debit">Debit Card</option>
  <option value="credit">Credit Card</option>
  <option value="cheque">Cheque</option>
  </select>
  </td></tr>
  <tr><td>Net sale</td><td><input type="text" class="form-control" name="total" value="0.00" disabled="disabled"/></td></tr>
  <tr><td>Paid</td><td><div class="rel"><div class="form-control" id='<?=uniqid();?>' onclick="onMenu(event,getObj('number_box_commit'),function(){});toggle('number_box_commit',function(o){$(o).show();},function(o){$(o).hide();},$('#number_box_commit'))">₦<span id='amt_paid'>0.00</span></div>
  <div class='number-box row hide2' id='number_box_commit'>
  <?php
  $num=range(1,9); 
  array_push($num,0);
  foreach ($num as $v) echo "<button class='col-xs-3 btn btn-default' onclick=\"NumberPicker.pick(-1,$v,$('#amt_paid'))\">$v</button>";
  ?>
   <button class='col-xs-6 btn btn-default' onclick="NumberPicker.clearEntry(-1,$('#amt_paid'))">CE</button>
   </div>
   </div>
  </td></tr>
  <tr><td>Change</td><td><input type="text" class="form-control" name="change" value="₦0.00" disabled="disabled"/></td></tr>
  <tr><td>Note</td><td>
  <label><input type="checkbox" name="onShowNote" value="1"/>&nbsp;Show on invoice</label>
  <textarea rows="3" class="form-control" name="note" placeholder="Order Note"></textarea>
  </td></tr>
  </tbody>
  </table>
  <input type="hidden" name="data" value="{}"/>
  <button type="submit" onclick="Cart.Payment.process(this.form)" name="commit" class="btn btn-primary pull-right">Make payment</button>
  </form>
  <iframe src="javascript:void())" name="checkoutFrame" id="checkout_frame" class="hide"></iframe>
  </div>
</div>
</div>
</div>

	<div class="hide" id="tmpl">
   <div class='item-box rel' id='item_box_%n%'>
   <div class='row'>
   <div class='col-md-12 no-padding'>
   <a href='javascript:void(0)' onclick='Cart.remove(%n%)' class='close' title="Remove">&times;</a>
   <a href="javascript:void(0)" onclick="Cart.SerialNo.box(%n%,this)" class="%hasSN%" title="Add serial number"><i class="glyphicon glyphicon-barcode"></i></a>&nbsp;<span>%pname%</span>
   <br/><span id='subtotal_%n%'>₦%subtotal%</span>
   <div class='clear'></div>
   <div class='btn-group'>
   <button class='btn btn-xs btn-default active'>₦%sprice%</button>
   <div class='btn btn-xs btn-default' data-qty='%qty%' style='width:50px' id='<?=uniqid();?>' onclick="onMenu(event,getObj('number_box_%n%'),function(){});toggle('number_box_%n%',function(o){$(o).show();},function(o){$(o).hide();},$('#number_box_%n%'))">%qty%</div>
   <button type="button" onclick="Cart.QTY.incr(%n%,this)" class="btn btn-xs btn-default">+</button>
   <button type="button" onclick="Cart.QTY.decr(%n%,this)" class="btn btn-xs btn-default">-</button>
   </div>
  
  <div class="pull-right" style="margin-right:15px">
  <a href="javascript:void(0)" class="discount fs15 hide2 icon-discount" id='discount_label_%n%' onmouseover="$('#discount_input_%n%').show();$('#discount_input_%n%').focus();$('#discount_label_%n%').hide()">%edis%</a>
   <input type="text" class="form-control discount-input hide2" placeholder="Discount" id='discount_input_%n%' onkeyup="Cart.Discount.setEach(%n%,this.value);$('#discount_label_%n%').html('-'+money(this.value))" onblur="$('#discount_input_%n%').hide();$('#discount_label_%n%').show()" value="%edis%"/>
   </div>
   
   <div data-code="0" class="panel panel-default absTL product-code hide2">
   <div class="panel-heading">Add Serial Number <a href="javascript:void(0)" class="close" onclick="Cart.SerialNo.close()">&times;</a></div>
  <div class="panel-body padding5">
   <div class="ae-well padding5" data-code-input="0" style="max-height:130px;margin-bottom:3px;overflow:auto">
   </div>
   
   <button class="btn btn-xs btn-default pull-left" onclick="Cart.SerialNo.clear(%n%)">Clear</button>
   <button class="btn btn-xs btn-default pull-right" onclick="Cart.SerialNo.apply(%n%)">Apply</button>
  
  </div>
  
</div>
   
   </div>
   </div>
   
   <button type="button" onclick="Cart.move(%n%)" class="btn btn-xs btn-default absBR cart-reorder hide2" title="Move to top"><i class="glyphicon glyphicon-circle-arrow-up"></i></button>
  
   <div class='number-box row hide2' id='number_box_%n%'>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,1)'>1</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,2)'>2</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,3)'>3</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,4)'>4</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,5)'>5</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,6)'>6</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,7)'>7</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,8)'>8</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,9)'>9</button>
   <button class='col-xs-3 btn btn-default' onclick='NumberPicker.pick(%n%,0)'>0</button>
   <button class='col-xs-6 btn btn-default' onclick='NumberPicker.clearEntry(%n%)'>CE</button>
   </div>
   <div class='clear'></div>
   </div>
   </div>
   <div class="panel panel-default cart-preview hide2" id="cart_preview">
   <div class="panel-heading"><h3 class="no-margin"><span></span> <a href="#" onclick="Cart.Preview.hide()" class="close">&times;</a></h3></div>
   <div class="panel-body row">
   <div class="col-sm-7">
   <a href="#" class="thumbnail">
   <img alt="large" src="javascript:void(0)" onload="$('#cart_preview').center()" class="img-responsive"/>
   </a>
   </div>
   <div class="col-sm-5">
   <table class="table table-striped">
   <tbody>
   </tbody>
   </table>
   <div class="btn-group">
   <a href="javascript:void(0)" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
   <a href="#" class="btn btn-sm btn-primary" id="item_edit"><i class="glyphicon glyphicon-edit"></i></a>
   </div>
   </div>
   <div class="clear"></div>
   </div>
   </div>