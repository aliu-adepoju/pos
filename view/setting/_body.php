<?php 
global $isOk,$post;
if(isset($post)) $_POST=$post;
ob_start();
?>
<div class="row bg-color-white padding10">
<?php
if ($isOk) {
	echo "<div class='alert alert-success' style='margin-bottom:10px'>Successfully updated</div>";
}
?>
<div class="col-md-6 no-padding">
<div class="ae-well padding10">
<h3 class="no-margin bb-blue">Profile</h3>
<div class="clear">&nbsp;</div>
<form action="<?=$_SERVER["PHP_SELF"];?>" method="post" enctype='multipart/form-data'>
<div class="control-group row" id="manu_box">
    <label>Logo</label>
    <div class="col-xs-12 no-padding">
    <input type="file" class="form-control" name="file" onmouseover="this.focus()"/>
   </div>
  </div>
  <div class="clear">&nbsp;</div>
<div class="control-group row" id="manu_box">
    <label>Store Name</label>
    <div class="col-xs-12 no-padding">
    <input type="text" class="form-control" name="name" onmouseover="this.focus()" value="<?=@$_POST["name"];?>"/>
   </div>
  </div>
  <div class="clear">&nbsp;</div>
<div class="control-group row" id="manu_box">
    <label>Store Unique ID</label>
    <div class="col-xs-12 no-padding">
    <input type="text" class="form-control" name="storeID" onmouseover="this.focus()" value="<?=@$_POST["storeID"];?>"/>
   </div>
  </div>
  <div class="clear">&nbsp;</div>
  <div class="control-group row">
    <label>Phone number(s)</label>
    <div class="col-xs-12 no-padding">
    <input type="text" class="form-control" name="phone" onmouseover="this.focus()" value="<?=@$_POST["phone"];?>"/>
   </div>
  </div>
  <div class="clear">&nbsp;</div>
  <div class="control-group row">
    <label>Email Address</label>
    <div class="col-xs-12 no-padding">
    <input type="text" class="form-control" name="email" onmouseover="this.focus()" value="<?=@$_POST["email"];?>"/>
   </div>
  </div>
  
  <div class="clear">&nbsp;</div>
  <div class="control-group row">
    <label>Web Address</label>
    <div class="col-xs-12 no-padding">
    <input type="text" class="form-control" name="web" onmouseover="this.focus()" value="<?=@$_POST["web"];?>"/>
   </div>
  </div>
  
  <div class="clear">&nbsp;</div>
  <div class="control-group row">
    <label>Store Address</label>
    <div class="col-xs-12 no-padding">
    <textarea class="form-control" rows="4" name="addr" onmouseover="this.focus()"><?=@$_POST["addr"];?></textarea>
   </div>
  </div>
  <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="profile" class="btn btn-primary pull-right">Save</button>
    </div>
  </form>
  <div class="clearfix"></div>
  </div>
</div>
<div class="col-md-6" style="padding-right:0px;padding-left:10px">
<div class="ae-well padding10">
<h3 class="no-margin bb-blue">Synchronization</h3>
 <form action="<?=$_SERVER["PHP_SELF"];?>" method="post">
<div class="control-group row">
   
    <div class="col-xs-12 no-padding">
    <div class="alert alert-warning padding5">
    <a href="">What is a synchronization and why do i need it?</a>
    </div>
     <label>
    <input type="checkbox"  name="auto_sync" <?=(@$_POST["auto_sync"]==1)?("checked='checked'"):(NULL);?>  onmouseover="this.focus()" value="1"/>
    Auto - Synchronize</label>
   </div>
  </div>
   <div class="clear">&nbsp;</div>
<div class="control-group row">
    <label>Synchronization Key</label>
    <div class="col-xs-12 no-padding">
    <input type="password" class="form-control" name="syncKey" onmouseover="this.focus()" value="<?=@$_POST["syncKey"];?>"/>
   </div>
  </div>
    <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="sync" class="btn btn-primary pull-right">Save</button>
    </div>
    <div class="clearfix"></div>
    </form>
   </div>
   
   
   <div class="ae-well padding10" style="margin-top:10px">
<h3 class="no-margin bb-blue">Invoice</h3>
 <form action="<?=$_SERVER["PHP_SELF"];?>" method="post">
 <div class="clear">&nbsp;</div>
 
 <label>
 <input type="checkbox"  name="item_desc" <?=(@$_POST["item_desc"]==1)?("checked='checked'"):(NULL);?>  onmouseover="this.focus()" value="1"/>
    Show item descriptions</label>
 
 <div class="clear">&nbsp;</div>
<div class="control-group row">
    <label>Return Policy / Note</label>
    <div class="col-xs-12 no-padding">
    <textarea rows="4" cols="5" class="form-control" name="note"><?=@$_POST["note"];?></textarea>
   </div>
  </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group row">
    <label>Printer</label>
    <div class="col-xs-12 no-padding">
    <select class="form-control" name="printer">
    <option value="standard" <?=(@$_POST["printer"]=="standard")?("selected=selected"):(NULL);?>>Standard printer</option>
    <option value="mini" <?=(@$_POST["printer"]=="mini")?("selected=selected"):(NULL);?>>Mini printer</option>
    </select>
   </div>
  </div>
    <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="invoice" class="btn btn-primary pull-right">Save</button>
    </div>
    </form>
    <div class="clearfix"></div>
   </div>
   
    <div class="ae-well padding10 hide" style="margin-top:10px">
<h3 class="no-margin bb-blue">Enable / Disable</h3>
 <form action="<?=$_SERVER["PHP_SELF"];?>" method="post">
 <div class="clearfix">&nbsp;</div>
 <label>
 <input type="checkbox"  name="status" <?=(@$_POST["expiry"]==1)?("checked='checked'"):(NULL);?>  onmouseover="this.focus()" value="1"/>
    Expiry date</label>
 
    <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="expiry" class="btn btn-primary pull-right">Save</button>
    </div>
    </form>
    <div class="clearfix"></div>
   </div>
   <?php 
   list($alert_days,$alert_noti)=json_decode($_POST["alert_noti"],1);
   ?>
   <div class="ae-well padding10 hide" style="margin-top:10px">
<h3 class="no-margin bb-blue">Notification</h3>
 <form action="<?=$_SERVER["PHP_SELF"];?>" method="post">
 <div class="clearfix">&nbsp;</div>
    <div class="control-group row">
    <label>Interval </label>
    <div class="col-xs-12 no-padding">
   <input type="text" name="alert_interval" class="form-control" value="<?=@$_POST["alert_interval"];?>"/>
   </div>
  </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group row">
    <div class="col-xs-1 no-padding">
   <small>Alert</small>
   </div>
   <div class="col-xs-3">
    <input type="number" min="0" class="form-control" value="<?=@$alert_days;?>" name="alert_days"/>
   </div>
   <div class="col-xs-3 no-padding">
   <select class="form-control" name="alert_noti">
   <?php 
   foreach (array(1=>"days",7=>"weeks",30=>"months") as $k=>$v){
	$sel=($k==$alert_noti)?("selected='selected'"):(NULL);
   	echo "<option {$sel} value='{$k}'>{$v}</option>";
   }
   ?>
   </select>
   </div>
   <div class="col-xs-5">
   <small>before expiry date</small>
   </div>
  </div>
  
    <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="alert" class="btn btn-primary pull-right">Save</button>
    </div>
    </form>
    <div class="clearfix"></div>
   </div>
   
</div>
</div>
<?php 
$contents=ob_get_clean();
include_once 'view/setting/_template.php';
?>