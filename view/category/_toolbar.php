<div class="row">
<div class="col-md-2 no-padding"><h3 class="no-margin">Category</h3></div>
<div class="col-md-6 no-padding">
<div class="input-group">
                <div class="input-group-btn search-panel">
                    <a href="category.php?p=add" class="btn btn-primary">Add category&nbsp;<i class="glyphicon glyphicon-plus"></i></a>
             	</div>
                <input type="hidden" name="search_param" value="all" id="search_param">         
                <input type="text" class="form-control" name="x" placeholder="Filter" id="tb_data_search" onmouseover="this.focus()"/>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
            </div>
            <div class="col-md-4">
            <?php 
            include_once 'view/stock/alert.php';
            ?>
            </div>
            </div>