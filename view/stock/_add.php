<?php 
global $data,$config;
ob_start();
?>

<div class="rel bg-color-white padding10" id="stock_box">
<?php 
if (isset($_GET["response"])) {
	?>
	<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
 <?=$_GET["response"];?>
</div>
	<?php 
}
?>
 <div id="err" class="alert alert-danger hide2">error</div>
<a href="stock.php?p=multiple" class="btn btn-xs btn-default absTR bluBT">Add Multiple</a>
 <form action="javascript:void(0)" method="post" enctype='multipart/form-data' class="stock-form">
 
 <div class="col-md-6">
 <?=(isset($_GET["status"]) && !empty($_GET["status"]))?("<div class='alert alert-success padding5'>Successfully added</div>"):(NULL);?>
 
  <div class="control-group row" id="manu_box">
    <label>Manufacturer</label>
   <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Manufacturer" value="<?=@$data["mname"];?>" name="manufacturer" id="<?=uniqid();?>" onchange="Stock.Icon.load(this.form)" onkeyup="DList.search(event,this,getObj('manufacturer_dlist'))"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="manufacturer_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>

 
 <div class="clear">&nbsp;</div>
 
 <div class="control-group row" id="pro_box">
    <label>Product Name</label>
    
  <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Product" value="<?=@$data["pname"];?>" name="product" id="<?=uniqid();?>" onchange="Stock.Icon.load(this.form)" onkeyup="DList.search(event,this,getObj('product_dlist'))"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="product_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>
  
  <div class="clear">&nbsp;</div>
   
    <div class="control-group row" id="cat_box">
    <label>Category</label>
    
   <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Category" value="<?=@$data["cname"];?>" name="category" id="<?=uniqid();?>" onchange="Stock.Icon.load(this.form)" onkeyup="DList.search(event,this,getObj('category_dlist'))"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="category_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>
  
   <div class="clear">&nbsp;</div>
   
   <div class="control-group row">
 <div class="col-xs-12 no-padding">
    <label onclick="toggle('desc',function(){$('#_desc').show();},function(){$('#_desc').hide();})">Descriptions / Features <i class="caret"></i></label>
	 <textarea rows="3" class="form-control hide2" name="description" onmouseover="this.focus()" id="_desc" placeholder="Describe product or list features"></textarea>
	 </div>
  </div>
  
  <div class="clear">&nbsp;</div>
  
  <?php 
  if($config["expiry"]==1){
  ?>
   <div class="control-group row">
 <div class="col-xs-6 no-padding">
    <label>Expiry Date</label>
	 <input type="text" class="form-control" name="expiry" onmouseover="$(this).datepicker({'viewMode':'years'})"/>
	 </div>
	 <div class="col-xs-6"></div>
  </div>
    <div class="clear">&nbsp;</div>
  <?php 
  }
  ?>
   
   <div class="control-group row">
    <div class="col-xs-4 no-padding">
    <label >Cost Price</label>
    <input type="text" class="form-control" name="cprice" value="<?=@$data["cprice"];?>" onmouseover="this.focus()" style="width:100px;"/>
    </div>
    
    <div class="col-xs-4 no-padding">
    <label >Sale Price</label>
    <input type="text" class="form-control" name="sprice" value="<?=@$data["sprice"];?>" onmouseover="this.focus()" style="width:100px;"/>
    </div>
    
    <div class="col-xs-4 no-padding" id="example">
   <label >Quantity</label>
    <input type="number" class="form-control" name="qty" min="1" value="<?=(!isset($data["qty"]))?(1):($data["qty"]);?>" onmouseover="this.focus()" style="width:100px;"/>
    </div>
     <div class="clear">&nbsp;</div>
 
 
    
    <div class="control-group row">
    <div class="col-xs-4 no-padding">
    <label>Product Image</label>
   <a href="javascript:void(0)" onclick='ImageStore.show({"manufacturer":$("form.stock-form")[0]["manufacturer"].value,"category":$("form.stock-form")[0]["category"].value})' class="thumbnail" title="Upload icon" style="width:100px">
   <img src="<?=(isset($_GET["edit"]))?("view/upload/thumbnail/{$data["iname"]}"):("img/burger1.png");?>" class="img-responsive" id="product_img" alt="image-name"/>
 	<input type="hidden" name="icon" value="<?=(isset($_GET["edit"]))?("view/upload/thumbnail/{$data["iname"]}"):("img/burger1.png");?>" id="icon_name_holder"/>
 	</a>
  </div>
    
    <div class="col-xs-8 rel">
    <label>Product codes | S/N &nbsp;<small>[Optional]</small></label>
    <button type="button" class="btn btn-sm btn-default" onclick="Stock.ProductCode.box(this)">Add product code <i class="glyphicon glyphicon-barcode"></i></button>
   
   <div data-dialog="productcode" class="panel panel-default product-code hide2" style="position:absolute;top:-230px;right:0px">
   <div class="panel-heading">Product codes <a href="javascript:void(0)" class="close" onclick="Stock.ProductCode.close()">&times;</a></div>
   <div class="panel-body padding5">
   <div class="ae-well padding5" data-code-input="0" style="height:250px;margin-bottom:3px;overflow:auto">
   </div>
   
   <button class="btn btn-xs btn-default pull-left" onclick="Stock.ProductCode.clear()">Clear</button>
  </div>
  </div>
  </div>
   
   </div>
  
    </div>
 
 
 
 </div>
 <div class="col-md-6">
    <div class="col-xs-12">
	  <h3>Supplier</h3>
	  </div>
    <div class="control-group col-xs-12">
    <label class="control-label">Company Name</label>
    <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="" value="" name="supplier" id="<?=uniqid();?>" onkeyup="DList.search(event,this,getObj('supplier_dlist'))"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="supplier_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Office Address</label>
    <textarea class="form-control" rows="5" name="addr">
    </textarea>
    </div>
    <div class="clear">&nbsp;</div>
   <div class="control-group row">
   <div class="col-xs-12">
    <label onclick="toggle('note',function(){$('#_note').show();},function(){$('#_note').hide();})">Order Note<i class="caret"></i></label>
	 <textarea rows="3" class="form-control hide2" name="note" onmouseover="this.focus()" id="_note" placeholder="Add order note"></textarea>
	 </div>
  </div>
     <div class="clear">&nbsp;</div>
    <div class="col-xs-12">
    <div class="btn-group pull-right">
    <button type="submit" onclick="Stock.add(this.form)" class="btn btn-primary">Submit</button>
    </div>
    </div>
    <div class="clear">&nbsp;</div>
    <?php 
    if(isset($_GET["req"])){
    	echo "<div class='alert alert-success padding5'>Successfully added</div>";
    }
    ?>
    
 </div>
 </form>
 <div class="clear"></div>
 </div>
 
					                    <iframe src="javascript:void(0)" name="addStockFrame" class="hide"></iframe>
					              
					              <?php 
$contents=ob_get_clean();
include_once 'view/setting/_template.php';
					              include_once 'widget/imgstore/ImageStore.php';
					              include_once 'widget/imgcrop/dialog.php';
					              ?>