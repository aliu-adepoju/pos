<form action="javascript:void(0)" method="post" enctype='multipart/form-data' class="stock-form rel">
<div class="ae-well padding5">
<strong class="fs25">Add Multiple Items</strong>
<div class="input-group pull-right">

<div class="btn-toolbar">

<div class="btn-group rel">
<button class="btn btn-default">Add Supplier</button>
<button class="btn btn-default" id="<?=uniqid();?>" onclick="toggle('supplier_box',function(box){$(box).show();},function(box){$(box).hide();},$('#supplier_box').get())"><i class="glyphicon glyphicon-plus"></i></button>
<div class="absBR ae-well padding10 hide2" id="supplier_box" style="top:100%;z-index:99;width:300px;height:430px">
<div class="control-group col-xs-12">
    <label class="control-label">Company Name</label>
    <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="" value=""  name="supplier" id="<?=uniqid();?>" onkeyup="DList.search(event,this,$('#supplier_dlist').get(0))"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="supplier_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Office Address</label>
    <textarea class="form-control" rows="5" name="addr">
    </textarea>
    </div>
<div class="clearfix"></div>
</div>
</div>

<div class="btn-group rel">
<button class="btn btn-default">Add Note</button>
<button class="btn btn-default" id="<?=uniqid();?>" onclick="toggle('note_box',function(box){$(box).show();},function(box){$(box).hide();},$('#note_box').get())"><i class="glyphicon glyphicon-plus"></i></button>
<div class="absBR ae-well padding10 hide2" id="note_box" style="top:100%;z-index:99;width:300px;height:170px">
    <div class="control-group col-xs-12">
    <label class="control-label">Add a note to purchase</label>
    <textarea class="form-control" rows="5" name="note">
    </textarea>
    </div>
<div class="clearfix"></div>
</div>
</div>

</div>
</div>
</div>

<div class="rel bg-color-white padding10">

<?php 
if (isset($_GET["response"])) {
	?>
	<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
 <?=$_GET["response"];?>
</div>
	<?php 
}
?>

<?=(isset($_GET["status"]) && !empty($_GET["status"]))?("<div class='alert alert-success padding5'>Successfully added</div>"):(NULL);?>
<table class="table table-striped add-multiple-stock">
<thead>
<tr><th>#</th><th></th><th>Manufacturer</th><th>Product</th><th>Category</th><th></th><th>Cost price</th><th>Sale price</th><th>Qty</th><th></th></tr>
</thead>
<tbody></tbody>
<tfoot>
<tr><td colspan="6"></td><td><h3 class="no-margin">₦<span id="total_supply_cost"></span></h3></td><td colspan="2"></td><td><button type="button" onclick="Stock.Multiple.add()" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i></button></td></tr>
</tfoot>
</table>
<div class="row">
<div class="col-md-10">
<div class="alert alert-danger padding5 hide2 pull-right" id="err"></div>
</div>
<div class="col-md-2">
<button type="submit" onclick="Stock.Multiple.submit(this.form)" name="submit" class="btn btn-primary pull-right">Submit</button>
</div>
</div>
</div>
<div class="clear"></div>
</form>
<iframe src="javascript:void(0)" name="addStockFrame" class="hide"></iframe>
<table class="hide" id="multiple_stock_tmpl">
<tr><td>%sn%</td><td>
<a href="javascript:void(0)" onclick='ImageStore.show({"manufacturer":$("form.stock-form")[0]["manufacturer[%i%]"].value,"category":$("form.stock-form")[0]["category[%i%]"].value},%i%)' class="thumbnail no-margin">
<img src="%icon%" class="product-img" data-image='%i%' style="width:20px;height:auto"/>
<input type="hidden" name="icon[%i%]" value="%icon%"/>
</a>

</td>
<td class="rel"><input type="text" class="form-control" value="%manufacturer%" name="manufacturer[%i%]" onmouseover="this.focus()" onchange="Stock.Multiple.update(this.form,%i%);Stock.Multiple.icon(this.form,%i%)" onkeyup="DList.search(event,this,getObj('manufacturer_dlist_%i%'),%i%)"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="manufacturer_dlist_%i%">
				      <ul class="navlist link"></ul>
				      </div></td>
				      <td class="rel">
				      <input type="text" class="form-control" value="%product%" name="product[%i%]" onmouseover="this.focus()" onchange="Stock.Multiple.update(this.form,%i%);Stock.Multiple.icon(this.form,%i%)" onkeyup="DList.search(event,this,getObj('product_dlist_%i%'),%i%)"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="product_dlist_%i%">
				      <ul class="navlist link"></ul>
				      </div>
				      
				      </td>
				      <td class="rel">
				     <input type="text" class="form-control" value="%category%" name="category[%i%]" onmouseover="this.focus()" onchange="Stock.Multiple.update(this.form,%i%);Stock.Multiple.icon(this.form,%i%)" onkeyup="DList.search(event,this,getObj('category_dlist_%i%'),%i%)"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="category_dlist_%i%">
				      <ul class="navlist link"></ul>
				      </div>
				      </td>
				      <td class="rel">
					<button type="button" class="btn btn-xs btn-default" onclick="Stock.Multiple.ProductCode.box(this,%i%)"><i class="glyphicon glyphicon-barcode"></i></button>
					<div data-dialog="productcode" class="panel panel-default product-code hide2 absTR" style="width:250px">
					   <div class="panel-heading">Product codes <a href="javascript:void(0)" class="close" onclick="Stock.Multiple.ProductCode.close()">&times;</a></div>
					   <div class="panel-body padding5">
					   <div class="ae-well padding5" data-code-input="0" style="margin-bottom:3px">
					   </div>
					   
					   <button class="btn btn-xs btn-default pull-left" onclick="Stock.Multiple.ProductCode.clear(%i%)">Clear</button>
					  </div>
					  </div>
</td>
				      <td>
				       <input type="text" class="form-control" name="cprice[%i%]" onkeyup="Stock.Multiple.calc(this.form,%i%)" value="%cprice%" onmouseover="this.focus()" style="width:100px;"/>
				      </td><td>
				      <input type="text" class="form-control" name="sprice[%i%]" onchange="Stock.Multiple.update(this.form,%i%);" value="%sprice%" onmouseover="this.focus()" style="width:100px;"/>
				      </td><td>
				      <input type="number" class="form-control" name="qty[%i%]" onchange="Stock.Multiple.calc(this.form,%i%);" onkeyup="Stock.Multiple.calc(this.form,%i%)" min="1" value="%qty%" onmouseover="this.focus()" style="width:100px;"/>
				      </td><td><button type="button" onclick="Stock.Multiple.remove(%i%)" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></button></td></tr>
				      </table>
				      
				       <?php
					   include_once 'widget/imgstore/ImageStore.php';
					   include_once 'widget/imgcrop/dialog.php';
					   ?>