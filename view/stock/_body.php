<div class="clear"></div>
<div class="col-md-12 mt10">
<table class="table table-striped table-bordered" id="tb_data">
<thead>
<tr><th>#</th><th></th><th>Product</th><th>Category<th>In stock</th><th>Qty. sold</th><th>Cost price</th><th>Sale price</th><th>Date</th><th></th></tr>
</thead>
<tbody>
<?php
$i=1;
foreach ($this->pdo->query("select s.*,(from_unixtime(s.date,'%d-%m-%Y')) as sdate,m.name as mname,p.name as pname,c.name as cname,i.name as iname,(select sum(x.qty) from stock x where x.stockID=s.stockID) as instock,(select sum(x.qty) from sale x where x.stockID=s.stockID and x.status='sold') as sqty from (stock s join manufacturer m on m.manufacturerID join product p on p.productID join category c on c.categoryID join img i on i.imgID) where m.manufacturerID=p.manufacturerID and p.productID=s.productID and c.categoryID=p.categoryID and i.imgID=s.imgID order by s.date desc") as $fetch){
?>
<tr><td><?=$i;?></td>
<td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="view/upload/thumbnail/<?=$fetch["iname"];?>" />
</a>
</td>
<td><a href=""><?="{$fetch["mname"]} {$fetch["pname"]}";?></a></td><td><?=$fetch["cname"];?></td><td><?=$fetch["instock"];?></td><td><?=intval($fetch["sqty"]);?></td><td><?=number_format($fetch["cprice"],2);?></td><td><?=number_format($fetch["sprice"],2);?></td><td><?=$fetch["sdate"];?></td>
<td>
<div class="btn-toolbar">
<div class="btn-group">
<a href="stock.php?p=edit&name=<?=$fetch["name"];?>&email=<?=$fetch["email"];?>&phone=<?=$fetch["phone"];?>&addr=<?=$fetch["addr"];?>&id=<?=$fetch["customerID"];?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i></a>
</div>

<div class="btn-group">
<a href="" class="btn btn-sm btn-danger" title="Move to Out-dated stocks">Out-dated</a>
</div>

</div>
</td></tr>
<?php 
$i++;
}
?>
</tbody>
</table>
</div>