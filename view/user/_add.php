<?php 
global $isOk;
?>
<div class="row bg-color-white padding10">
<div class="col-md-7">
<?php
if ($isOk) {
	echo "<div class='alert alert-success padding5'>Successfully added</div>";
}
?>
<form action="<?=$_SERVER["PHP_SELF"];?>?p=add" method="post">
<div class="control-group">
    <label class="control-label">Full Name</label>
    <input type="text" name="name" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control"/>
    </div>
    
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Role</label>
    <select class="form-control" name="roleID">
    <option value=""></option>
    <?=options("role");?>
    </select>
    </div>
  <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="seller" class="btn btn-primary">Add</button>
    </div>
  </form>
</div>
<div class="col-md-5">

</div>
</div>