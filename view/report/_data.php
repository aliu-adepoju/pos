<?php
$from=new DateTime(date("Y-m-d",mktime(0, 0, 0, $m,1,$y)));//date("Y-m-d",mktime(0, 0, 0, $m,1,$y))
$to=new DateTime(date("Y-m-d",mktime(0, 0, 0, $m+1, 0,$y)));//date("Y-m-d",mktime(0, 0, 0, $m+1, 0,$y))
$bool=false;
include_once 'lib/Time.php';
if(isset($_POST["from"]) && isset($_POST["to"])){
	session_start();
	session_regenerate_id();
	include_once '../../config.php';
	include_once '../../lib/Time.php';
	$date=$_POST["date"];
	list($y,$m,$d)=explode("-",$_POST["date"]);
	$bool=true;
	$from=new DateTime($_POST["from"]);
	$to=new DateTime($_POST["to"]);
}

$time=new Time();
$acqui=$pdo->prepare("select (select count(distinct(customerID)) from sale where from_unixtime(date,'%Y%m%d') > :from and from_unixtime(date,'%Y%m%d') <= :to) as customer,(select count(*) from supplier where from_unixtime(date,'%Y%m%d') > :from and from_unixtime(date,'%Y%m%d') <= :to) as supplier,(select sum(qty) from sale where status='backorder' and from_unixtime(date,'%Y%m%d') > :from and from_unixtime(date,'%Y%m%d') <= :to) as backorder");
$acqui->execute(array("from"=>$from->format('Ymd'),"to"=>$to->format('Ymd')));
$acquisitions=array();
$arr=array("customer"=>"Customers","supplier"=>"Suppliers","backorder"=>"Back Order");
foreach ($acqui->fetch(PDO::FETCH_ASSOC) as $k=>$v){
	$acquisitions["{$k}.php?from={$from->format('Ymd')}&to={$to->format('Ymd')}"]=array($arr[$k],$v);
}

//echo $from->format('Ymd')." - ".$to->format('Ymd');

function getSalesRange($start,$end){
	global $pdo;
	$sales=$pdo->query("select sum((x.sprice*s.qty)-s.deduct) as netsale,sum(((x.sprice*s.qty)-s.deduct)- (x.cprice*s.qty)) as netprofit from (sale s join stock x on x.stockID) where x.stockID=s.stockID and s.status='sold' and (from_unixtime(s.date,'%Y%m%d') >= $start and from_unixtime(s.date,'%Y%m%d') < $end)");
	return $sales->fetch(PDO::FETCH_ASSOC);
}


$interval=$from->diff($to);
$diff=$interval->format('%a');

if($diff <= 7){
	$range[$from->format('D jS')]=array($from->format('Ymd'),date("Ymd",mktime(0, 0, 0, $from->format('m'),$from->format('d')+1,$from->format('Y'))));
	for($i=0;$i<$diff;$i++){
		$label=$from->format('D jS');
		$begin=$from->format('Ymd');
		$nxt=$from->add(new DateInterval("P1D"));
		$end=$nxt->format('Ymd');
		$range[$label]=array($begin, $end);
	}
	$range[$to->format('D jS')]=array($end,$to->format('Ymd')+1);
}


if($diff > 7 && $diff <= 31){
	$range[$from->format('M jS')]=array($from->format('Ymd'),date("Ymd",mktime(0, 0, 0, $from->format('m'),$from->format('d')+1,$from->format('Y'))));
	for($i=0;$i<$diff;$i++){
		$label=$from->format('M jS');
		$begin=$from->format('Ymd');
		$nxt=$from->add(new DateInterval("P1D"));
		$end=$nxt->format('Ymd');
		$range[$label]=array($begin, $end);
	}
	
	$range[$to->format('M jS')]=array($end,$to->format('Ymd')+1);
}

if($diff > 31 && $diff <= 365){
	$range[$from->format('M jS')]=array($from->format('Ymd'),date("Ymd",mktime(0, 0, 0, $from->format('m')+1,$from->format('d'),$from->format('Y'))));
	for($i=0;$i<floor($diff/31);$i++){
		$label=$from->format('M jS');
		$begin=$from->format('Ymd');
		$nxt=$from->add(new DateInterval("P1M"));
		$end=$nxt->format('Ymd');
		$range[$label]=array($begin, $end);
	}

	$range[$to->format('M jS')]=array($end,$to->format('Ymd'));
}


if($diff > 365){
	$range[$from->format('Y')]=array($from->format('Ymd'),date("Ymd",mktime(0, 0, 0, $from->format('m'),$from->format('d'),$from->format('Y')+1)));
	for($i=0;$i<floor($diff/365);$i++){
		$label=$from->format('Y');
		$begin=$from->format('Ymd');
		$nxt=$from->add(new DateInterval("P1Y"));
		$end=$nxt->format('Ymd');
		$range[$label]=array($begin, $end);
	}
	$range[$to->format('Y')]=array($end,$to->format('Ymd'));
}


list($days,$sales,$profits)=array(array(),array(),array());
foreach ($range as $k=>$interval){
	$getData=getSalesRange($interval[0],$interval[1]);
	$days[]=$k/* ." ".$interval[0]." ".$interval[1] */;
	$sales[]=$getData["netsale"];
	$profits[]=$getData["netprofit"];
}

/* print("<pre>");
print_r($days);
print_r($sales);
print_r($profits);
print("</pre>"); */

$profitMargin=@round((array_sum($profits)/array_sum($sales))*100);

$_data=array("label"=>$days,"sales"=>$sales,"profits"=>$profits);
if($bool==true){
	echo json_encode(array("_data"=>json_encode(array("label"=>$days,"sales"=>$sales,"profits"=>$profits)),"profitMargin"=>$profitMargin,"acquisitions"=>$acquisitions,"revenue"=>array_sum($sales),"income"=>array_sum($profits)));
}