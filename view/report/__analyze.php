<?php
include_once '../../lib/Time.php';
$time=new Time();
include_once '../../config.php';
$sku=$pdo->quote($_POST["sku"]);
$product=$pdo->query("select x.*,x.qty as instock,m.name as mname,c.name as cname,p.name as pname,p.reorder,i.name as iname,(select sum(s.qty) from sale s where s.stockID=x.stockID and s.status='backorder') as backorder,(select sum(s.qty) from sale s where s.stockID=x.stockID and s.status='sold') as sold from (stock x join product p on p.productID join manufacturer m on m.manufacturerID join category c on c.categoryID join img i on i.imgID) where i.imgID=x.imgID and p.productID=x.productID and m.manufacturerID=p.manufacturerID and c.categoryID=p.categoryID and x.SKU=$sku");
$fetch=$product->fetch(PDO::FETCH_ASSOC);

$sale=$pdo->query("select c.name as cname,s.qty as sqty,((x.sprice*s.qty)-s.deduct) as amount,s.deduct,u.name as uname,s.date from (sale s join stock x on x.stockID join customer c on c.customerID join usr u on u.usrID) where u.usrID=s.usrID and x.stockID=s.stockID and c.customerID=s.customerID and s.stockID={$fetch['stockID']} order by s.date desc limit 1");
$sale=$sale->fetch(PDO::FETCH_ASSOC);

$purchase=$pdo->query("select p.qty,u.name as receiver,s.name as supplier,p.note,p.date from (purchase p join usr u on u.usrID join supplier s on s.supplierID) where u.usrID=p.receiverID and s.supplierID=p.supplierID and p.stockID={$fetch['stockID']} order by p.date desc limit 1");
$purchase=$purchase->fetch(PDO::FETCH_ASSOC);

/* print("<pre>");
print_r($purchase);
print("</pre>"); */
?>
<a href="#" name="analyze"></a>
<div class="row ae-well">
<h3>&nbsp;&nbsp;<?=$fetch["mname"]." ".$fetch["pname"];?> <small>Analysis</small></h3>
<div class="col-md-5 padding10">
 <a href="#" class="thumbnail no-margin">
   <img alt="large" src="view/upload/large/<?=$fetch["iname"];?>" class="img-responsive"/>
   </a>
</div>
<div class="col-md-7">
<table class="table table-striped">
<tbody>
<tr><td><strong>SKU</strong></td><td><?=$fetch["SKU"];?></td></tr>
<tr><td><strong>Manufacturer</strong></td><td><?=$fetch["mname"];?></td></tr>
<tr><td><strong>Category</strong></td><td><?=$fetch["cname"];?></td></tr>
<tr><td><strong>Sale Price</strong></td><td><?=$fetch["sprice"];?></td></tr>
<tr><td><strong>Cost Price</strong></td><td><?=$fetch["cprice"];?></td></tr>
<tr><td><strong>Descriptions / Features</strong></td><td><?=$fetch["description"];?></td></tr>
</tbody>
</table>
<div class="row">
<div class="col-xs-3 bg-primary text-center"><h1><?=$fetch["instock"];?></h1><small>In stock</small></div>
<div class="col-xs-3 bg-info text-center"><h1><?=intval($fetch["backorder"]);?></h1><small>Back-Order</small></div>
<div class="col-xs-3 bg-success text-center"><h1><?=intval($fetch["sold"]);?></h1><small>Quantity sold</small></div>
<div class="col-xs-3 bg-danger text-center"><h1><?=$fetch["reorder"];?></h1><small>Re-order level</small></div>
</div>
</div>
<div class="clear">&nbsp;</div>
<div class="col-md-12">
<div class="bg-color-white padding10">
<h3>Recent activities</h3>
<table class="table table-bordered">
<thead>
<tr><th colspan="5">Sale</th><th colspan="5">Purchase</th></tr>
<tr><th>Customer</th><th>Qty.</th><th>Amount</th><th>Seller</th><th>Date</th><th>Received by</th><th>Qty.</th><th>Supplier</th><th>Order Note</th><th>Date</th></tr>
</thead>
<tbody>
<tr><td><?=$sale["cname"];?></td><td><?=$sale["sqty"];?></td><td><?=$sale["amount"];?><br/><span class="label label-warning">Discount: <?=$sale["deduct"];?></span></td><td><?=$sale["uname"];?></td><td><small><?=$time->format($sale["date"]);?></small></td><td><?=$purchase["receiver"];?></td><td><?=$purchase["qty"];?></td><td><a href="#"><?=$purchase["supplier"];?></a></td><td><?=$purchase["note"];?></td><td><small><?=$time->format($purchase["date"]);?></small></td></tr>
</tbody>
</table>
</div>
</div>
<div class="clear">&nbsp;</div>

</div>