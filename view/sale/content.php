<?php
$xSeller=(in_array($_SESSION["roleID"],array(1,2)))?(NULL):("usrID={$_SESSION['usrID']} and");
$filter="$xSeller from_unixtime(date,'%Y%m%d')=date_format(now(),'%Y%m%d')";
$date=date("Y-m-d");
$salesType="sold";
$bool=false;
if(isset($_POST["date"])){
	session_start();
	session_regenerate_id();
	include_once '../../config.php';
	include_once '../../lib/Time.php';
	$date=$_POST["date"];
	$sellerID=intval($_POST["seller"]);
	$xSeller=(empty($_POST["seller"]))?((in_array($_SESSION["roleID"],array(1,2)))?(NULL):("usrID={$_SESSION['usrID']} and")):("usrID=$sellerID and");
	$filter="$xSeller from_unixtime(date,'%Y%m%d')=".str_replace("-","",$_POST["date"]);
	list($y,$m,$d)=explode("-",$_POST["date"]);
	$salesType=$_POST["salesType"];
	$bool=true;
} 
$time=new Time();
$sales=$pdo->query("select *,p.name as pname,c.name as cname,s.qty,((x.sprice*s.qty)-s.deduct) as netsale,x.qty as qtyR,s.discount,m.name as mname,s.status,i.name as iname from (sale s join stock x on x.stockID join product p on p.productID join customer c on c.customerID join manufacturer m on m.manufacturerID join img i on i.imgID) where x.imgID=i.imgID and x.stockID=s.stockID and p.productID=x.productID and c.customerID=s.customerID and m.manufacturerID=p.manufacturerID and s.invoice in (SELECT invoice FROM sale where $filter group by invoice) order by s.date desc");
$sales=$sales->fetchAll(PDO::FETCH_ASSOC);
function arrayRowSum($stack,$key){
	$arr=array();
	foreach ($stack as $i=>$data){
		if($data["status"]=="sold")
			$arr[]=$data[$key];
	}
	return array_sum($arr);
}

function arrayRowQuery($stack,$key,$query){
	$arr=array();
	if(is_null($query)){
		foreach ($stack as $i=>$data){
			$arr[]=$data[$key];
		}
		return $arr;
	}
	foreach ($stack as $i=>$data){
		if($data["status"]==$query)
			$arr[]=$key=="*"?$data:$data[$key];
		
	}
	return $arr;
}

ob_start();
?>


<div class="row mt10 adjMt20" style="padding:0px 10px">
<div class="col-sm-4 no-padding">
<div class="row">
<div class="col-md-6 bg-color-success fg-color-white padding10">
Net Sales<h3 class="no-margin"><strong>₦<?=number_format(arrayRowSum($sales, "netsale"),2);?></strong></h3>
</div>
<div class="col-md-6 bg-color-primary fg-color-white padding10">
Products<h3 class="no-margin"><strong><?=arrayRowSum($sales, "qty");?></strong></h3>
</div>
</div>
</div>
<div class="col-sm-4 no-padding">
<div class="row">
<div class="col-md-6 bg-color-info fg-color-white padding10">
Discount/Allowance<h3 class="no-margin"><strong>₦<?=number_format(arrayRowSum($sales, "deduct"),2);?></strong></h3>
</div>
<div class="col-md-6 bg-color-orange fg-color-white padding10">
Refund/Cancelled<h3 class="no-margin"><strong><?=array_sum(arrayRowQuery($sales,"qty","refund"));?></strong></h3>
</div>
</div>
</div>
<div class="col-sm-4 no-padding">
<div class="row">
<div class="col-md-6 bg-color-warning fg-color-white padding10">
Back Order<h3 class="no-margin"><strong><?=array_sum(arrayRowQuery($sales,"qty","backorder"));?></strong></h3>
</div>
<div class="col-md-6 bg-color-black fg-color-white padding10">
Customers<h3 class="no-margin"><strong><?=count(array_unique(arrayRowQuery($sales,"customerID",NULL)));?></strong></h3>
</div>
</div>
</div>
</div>


<div class="row mt10">
<div class=" col-sm-12 ae-well padding10">
<div class="btn-toolbar">

<div class="btn-group pull-right">
<button class="btn btn-info" onclick="Synchronize.start(<?=(isset($_POST["date"]))?(str_replace("-","",$_POST["date"])):(date("Ymd"));?>,'<?=base64_encode('xyz');?>')">Synchronize&nbsp;<i class="glyphicon glyphicon-globe"></i></button>
</div>


<div class="btn-group pull-right">
<a href="#table" role="tab" data-toggle="tab" class="btn btn-default active" onclick="toggleBButton(this)"><i class="glyphicon glyphicon-th-list"></i></a>
<a href="#summary" role="tab" data-toggle="tab" class="btn btn-default" onclick="toggleBButton(this)"><i class="glyphicon glyphicon-th-large"></i></a>
</div>

<div class="btn-group">
<button type="button" onclick="Sales.download('<?=$date;?>',$('#sales_type').val(),((isDefined($('#sellers').val()))?($('#sellers').val()):('')))" class="btn btn-default"><i class="glyphicon glyphicon-download"></i></button>
<button type="button" onclick="Sales.print('<?=$date;?>',$('#sales_type').val(),((isDefined($('#sellers').val()))?($('#sellers').val()):('')))" class="btn btn-default"><i class="glyphicon glyphicon-print"></i></button>
</div>

<div class="btn-group">
<select class="form-control" id="sales_type" onchange="getSalesData('<?=$date;?>',$('#sellers').val(),this.value)">
<?php 
                    foreach (array("sold"=>"Sold Items","refund"=>"Refund/Cancelled","backorder"=>"Back Order") as $k=>$v){
						$active=(@$_POST["salesType"]==$k)?("selected=selected"):(NULL);
                    	?>
                    	<option <?=$active;?> value="<?=$k;?>"><?=$v;?></option>
                    	 <?php 
                    }
                    ?>
</select>
</div>


<div class="btn-group">
<?php 
if(in_array($_SESSION["roleID"],array(1,2))){
	?>
<select class="form-control" id="sellers"  onchange="getSalesData('<?=$date;?>',this.value,$('#sales_type').val())">
<option value="">All Sellers</option>
<?php 
                    foreach ($pdo->query("select * from usr where roleID=3") as $fetch){
						$active=(@$_POST["seller"]==$fetch["usrID"])?("selected=selected"):(NULL);
                    	?>
                    	<option <?=$active;?> value="<?=$fetch["usrID"];?>"><?=$fetch["name"];?></option>
                    	 <?php 
                    }
                    ?>
</select>
<?php 
}
?>
</div>

<div class="btn-group">
<small style="line-height:30px">&nbsp;&nbsp;&nbsp;<?=(isset($_POST["date"]) && date("Ymd") > str_replace("-","",$_POST["date"])*1)?(date("D, j M Y",mktime(0, 0, 0,$m,$d,$y))." Sales"):("Today's Sales");?></small>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="table">
  <div class="clearfix">&nbsp;</div>
  <table class="table table-striped table-bordered" id="sales_data_table">
<thead>
<tr><th>#</th><th></th><th>Invoice</th><th>Items</th><th>Price</th><th>Discount</th><th>Qty.</th><th>Line Total</th><th>Customer</th></tr>
</thead>
<tbody>
<?php 
$discountList=array();
$group=array();
foreach (arrayRowQuery($sales,"*",$salesType) as $i=>$fetch){
	$group[$fetch["invoice"]][]=$fetch;
	$each=json_decode($fetch["discount"],1)["each"];
	$global=json_decode($fetch["discount"],1)["global"];
	//$discountList[]=(((($global*$fetch["sprice"])/100)*$fetch["qty"])+($each*$fetch["qty"]));
	$discountList[]=(($global*$fetch["sprice"])/100)+$each;
	?>
	<tr><td><?=$i+1;?></td>
	<td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="view/upload/thumbnail/<?=$fetch["iname"];?>"  />
</a>
</td>
	<td><?=$fetch["invoice"];?></td><td><?="{$fetch["mname"]} {$fetch["pname"]}";?></td><td><?=$fetch["sprice"];?></td><td><?=number_format($discountList[$i],2);?></td><td><?=$fetch["qty"];?></td><td><?=number_format(($fetch["sprice"]*$fetch["qty"])-($discountList[$i]*$fetch["qty"]),2);?></td><td><?=$fetch["cname"];?></td></tr>
	<?php 
}
?>
</tbody>
</table>
  </div>
  <div role="tabpanel" class="tab-pane" id="summary">
  <div class='clearfix'>&nbsp;</div>
  <div id="item_box">
<?php 
$products=array();
$prices=array();
$totalDiscount=array();
$totalLineTotal=array();
foreach($group as $invoice=>$fetch){
?>
<div data-search="1" class="row ae-well padding10" style="margin-bottom:3px">
<div class="col-xs-12 no-padding">
<div class="padding5">
Invoice # - <?=$invoice;?>
<small class="pull-right text-muted"><?=$time->format($fetch[0]["date"]);?></small>
<div class="clear"></div>
<?php 
$linetotal=array();
$discount=array();
foreach ($fetch as $item){
	$each=json_decode($item["discount"],1)["each"];
	$global=json_decode($item["discount"],1)["global"];
	$discount[]=($item["sprice"]*$item["qty"])-(((($global*$item["sprice"])/100)*$item["qty"])+($each*$item["qty"]));
	$linetotal[]=$item["sprice"]*$item["qty"];
	$cname=$item["cname"];
	$products["{$item["mname"]} {$item["pname"]}"][]=$item["qty"];
	$instock["{$item["mname"]} {$item["pname"]}"]=$item["qtyR"];
	$img["{$item["mname"]} {$item["pname"]}"]=$item["iname"];
?>
<span class="search-data hide"><?="{$invoice} {$item["mname"]} {$item["pname"]} {$time->format($fetch[0]["date"])} {$item["qty"]} {$item["cname"]} {$item["sprice"]}";?></span>
<div class="item-tag" title="<?="Discounts: ₦{$each} + {$global}%";?>">
<?="{$item["mname"]} {$item["pname"]}";?>
&nbsp;&nbsp;<div class="badge bg-color-primary"><?=$item["qty"];?></div>
</div>
<?php 
}
$totalDiscount[]=array_sum($discount);
$totalLineTotal[]=array_sum($linetotal);
?>
</div>
<div class="clear"></div>
<div class="padding5 no-margin">
<span class="label label-warning">₦<?=number_format(array_sum($discount),2);?></span>
<?php
if($linetotal > $discount){
?>
&nbsp;<del>₦<?=number_format(array_sum($linetotal),2);?></del>
<?php 
}
?>
<a href=""><?=$item["cname"];?></a>
<div class="btn-group pull-right">
<button type="button" onclick="Sales.invoice('<?=$invoice;?>')" class="btn btn-xs btn-info no-border"><i class="glyphicon glyphicon-print"></i></button>
<button type="button" onclick="lock(true);$('#confirm_box').show();$('#del_invoice').val('<?=$invoice;?>');$('#confirm_box').center()" class="btn btn-xs btn-danger no-border"><i class="glyphicon glyphicon-trash"></i></button>
</div>
</div>
<div class="clear"></div>
</div>

</div>
<?php 
}
?>
</div>
<div class="clearfix">&nbsp;</div>
<table class="table table-striped table-bordered tb_data">
<thead>
<tr><th>#</th><th></th><th>Items</th><th>Qty</th><th>Closing stock</th></tr>
</thead>
<tbody>
<?php 
$i=1;
$data=array();
foreach ($products as $product=>$qty){
	$data["product"][]=$product;
	$data["sold"][]=intval(array_sum($qty));
	$data["instock"][]=intval($instock[$product]);
	?>
	<tr><td style="width:30px"><?=$i;?></td>
	<td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="view/upload/thumbnail/<?=$img[$product];?>" />
</a>
</td>
	<td><?=$product;?></td><td><?=array_sum($qty);?></td><td><?=$instock[$product];?></td></tr>
	<?php 
	$i++;
}
//print_r($data);
?>
</tbody>
</table>
  </div>
</div>

</div>
</div>

<iframe src="javascript:void(0)" id="reprintFrame" class="hide"></iframe>
<div class="confirm-box padding10 hide2" id="confirm_box">
<form action="sale.php" method="post">
<strong>Refund/Cancelled</strong>
<hr class="no-margin">
<h4>Are you sure?</h4>
<textarea rows="2" class="form-control" name="note" placeholder="Add Note"></textarea>
<div class="clearfix">&nbsp;</div>
<div class="btn-group pull-right">
<input type="hidden" name="del" id="del_invoice" value=""/>
<button type="submit" class="btn btn-default btn-xs">Yes</button>
<button type="button" onclick="lock();$('#confirm_box').hide()" class="btn btn-default btn-xs">No</button>
</div>
</form>
</div>
<?php 
$contents=ob_get_clean();
echo $bool==true?json_encode(array("contents"=>$contents,"data"=>json_encode($data))):$contents;
?>