<?php
include_once '../../config.php';
$date=str_replace("-","",$_GET["date"]);
$date=str_replace("-","",$_GET["date"]);
$xSeller=(empty($_GET["seller"]))?(NULL):("usrID={$_GET["seller"]} and");
$filter="$xSeller from_unixtime(date,'%Y%m%d')=$date";
$sales=$pdo->query("select *,p.name as pname,c.name as cname,s.qty,((x.sprice*s.qty)-s.deduct) as netsale,x.qty as qtyR,s.discount,m.name as mname,s.status,i.name as iname,u.name as uname from (sale s join stock x on x.stockID join product p on p.productID join customer c on c.customerID join manufacturer m on m.manufacturerID join img i on i.imgID join usr u on u.usrID) where u.usrID=s.usrID and x.imgID=i.imgID and x.stockID=s.stockID and p.productID=x.productID and c.customerID=s.customerID and m.manufacturerID=p.manufacturerID and s.invoice in (SELECT invoice FROM sale where $filter group by invoice) order by s.date desc");
$sales=$sales->fetchAll(PDO::FETCH_ASSOC);
$salesType=$_GET["salesType"];

list($y,$m,$d)=explode("-",$_GET["date"]);
function arrayRowQuery($stack,$key,$query){
	$arr=array();
	if(is_null($query)){
		foreach ($stack as $i=>$data){
			$arr[]=$data[$key];
		}
		return $arr;
	}
	foreach ($stack as $i=>$data){
		if($data["status"]==$query)
			$arr[]=$key=="*"?$data:$data[$key];

	}
	return $arr;
}

ob_end_clean();
$filename="sales_$date";
header("Content-Type: text/csv");
header("Content-Disposition: attachment;filename={$filename}.csv");
$fp=fopen('php://output','w');
fputcsv($fp,array("Invoice","Items","Price","Discount","Qty.","Line Total","Customer","Seller"));
$discountList=array();
foreach (arrayRowQuery($sales,"*",$salesType) as $i=>$fetch){
	$each=json_decode($fetch["discount"],1)["each"];
	$global=json_decode($fetch["discount"],1)["global"];
	$discountList[]=(($global*$fetch["sprice"])/100)+$each;
	fputcsv($fp,array("{$fetch["invoice"]}","{$fetch["mname"]} {$fetch["pname"]}",$fetch["sprice"],number_format($discountList[$i],2),$fetch["qty"],number_format(($fetch["sprice"]*$fetch["qty"])-($discountList[$i]*$fetch["qty"]),2),$fetch["cname"],$fetch["uname"]));
}
fclose($fp);