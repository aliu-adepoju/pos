<div class="dialog" id="dialog" style="width:80%;height:auto">
<div class="row no-margin">
<div class="col-md-9 rel border no-padding">
<iframe name="imgCropFrame" id="imgCropFrame" style="position:relative;width:100%;height:500px;border:none;margin:0px;padding:0px;overflow:hidden"></iframe>
<div class="absTL loading lead" id="loading">Please wait, Uploading...</div>
</div>
<div class="col-md-3 rel" style="height:500px">
<div class="hide2" id="thumbnail_1">
<h5>Resized</h5>
<div class="thumbnail">
<img alt="thumbnail" src="" class="img-reponsive"/>
</div>
</div>
<div class="clear">&nbsp;</div>
<div class="rel" style="text-align:center">
<table style="margin:0px auto">
	<tr><td>&nbsp;</td><td><button onclick="window.frames['imgCropFrame'].navN(this)" ondblclick="window.frames['imgCropFrame'].navN(this)" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-chevron-up"></i></button></td><td>&nbsp;</td></tr>
	<tr><td><button onclick="window.frames['imgCropFrame'].navE(this)" ondblclick="window.frames['imgCropFrame'].navE(this)" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-chevron-left"></i></button></td><td><button onclick="window.frames['imgCropFrame'].navC()" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-align-justify"></i></button></td><td><button onclick="window.frames['imgCropFrame'].navW(this)" ondblclick="window.frames['imgCropFrame'].navW(this)" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-chevron-right"></i></button></td></tr>
	<tr><td>&nbsp;</td><td><button onclick="window.frames['imgCropFrame'].navS(this)" ondblclick="window.frames['imgCropFrame'].navS(this)" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-chevron-down"></i></button></td><td>&nbsp;</td></tr>
	</table>
</div>
<div class="btn-group absBR" style="right:10px;bottom:10px">
<button class="btn btn-default" onclick="lock();$('#dialog').hide()"><i class="glyphicon glyphicon-cross"></i>&nbsp;Save & Close</button>
<button class="btn btn-default" onclick="getObj('imgCropFrame').src='widget/imgcrop/upload.php?coor='+window.frames['imgCropFrame'].getCoord()+'&img='+escape(window.frames['imgCropFrame'].getObj('srcimg').src)"><i class="glyphicon glyphicon-crop"></i>&nbsp;Crop</button>
</div>
<div class="clear">&nbsp;</div>
</div>
</div>
</div>