<?php
mt_srand((double)microtime()*1000000);
class RandNumGen{
	public function ranChar(){
		$charset="0aA1bB2CcD3Ed4eF5Gf6Ig7Jh8Ki9Lj0MkN1OlP2Qm3Rn4So5TpU6Vq7Wr8Xs9t0Yu1Zw2x3y4z5";
		return $charset[mt_rand(0,(strlen($charset)-1))];
	}
	public function init($len=6){
		$retrn="";
		for ($i=0;$i<$len;$i++){
			$retrn  .= $this->ranChar();
		}
		return($retrn);
	}
}
?>