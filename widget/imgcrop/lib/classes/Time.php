<?php
class Time{
	public function __construct(){
	}
	public function format($timestamp){
		if ((time()-$timestamp) < 60){
			$sec = ((time()-$timestamp) > 1)?("secs"):("sec");
			$actime = (time()-$timestamp)." {$sec} ago.";
			return($actime);
		}
		elseif ((time() - $timestamp) < 3600){
			$rawTime1 = (time()-$timestamp);
			$rawTime2 = $rawTime1 / 60;
			$mins1 = floor($rawTime2);
			$mins2 = floor($rawTime2) * 60;
			$secs = $rawTime1 - $mins2;
			$min = ($mins1 > 1)?("mins"):("min");
			$sec = ($secs > 1)?("secs"):("sec");
			$actime =  "{$mins1} {$min} {$secs} {$sec} ago";
			return($actime);
		}
		elseif ((time() - $timestamp) < 86400){
			$rawhr1 = (time()-$timestamp);
			$rawhr2 = $rawhr1 / 3600;
			$hrs1 = floor($rawhr2);
			$hrs2 = floor($rawhr2) * 3600;
			$mins = round(($rawhr1 - $hrs2) / 60);
			$hr = ($hrs1 > 1)?("hrs"):("hr");
			$min = ($mins > 1)?("mins"):("min");
			$actime = "{$hrs1} {$hr} {$mins} {$min} ago.";
			return($actime);
		}
		elseif ((time() - $timestamp) < 172800){
			$date = date('h:i A',$timestamp);
			$actime = "{$date}, Yesterday";
			return($actime);
		}
		else{
			$date = date('F d \a\t h:ia',$timestamp);
			$actime = $date;
			return($actime);
		}
	}
}
?>