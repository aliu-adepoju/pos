<?php
class ImageCrop{
	var $img;
	function __construct($img,$save_as){
		$this->img=$img;
		$this->save_as=$save_as;
	}
	
function coor($v=array()) {
	$image_data=getimagesize($this->img);
	list($width,$height)=$image_data;
	$imageType=image_type_to_mime_type($image_data[2]);
	list($x1,$x2,$y1,$y2)=$v;//coordinates
	$newImage=imagecreatetruecolor($width+($x1+$x2),$height+($y1+$y2));
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($this->img);
			break;
		case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($this->img);
			break;
		case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($this->img);
			break;
	}
	imagecopyresampled($newImage,$source,$x1,$y1,0,0,$width,$height,$width,$height);

	switch($imageType) {
		case "image/gif":
			imagegif($newImage,$this->save_as);
			break;
		case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			imagejpeg($newImage,$this->save_as,90);
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$this->save_as);
			break;
	}

	chmod($this->img, 0777);
	return $this->img;
}
	
}
?> 
