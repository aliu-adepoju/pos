<?php
function imagePad($filename,$w,$h,$save_as){
		$imgRes=new ImageResizer;
		$imgRes->load($filename);
		$imgRes->resizeToWidth(($imgRes->getWidth()>$w)?($w):($imgRes->getWidth()));
		list($width,$height)=array($imgRes->getWidth(),$imgRes->getHeight());
		$dst=imagecreatetruecolor($w,$h);
		imagefilledrectangle($dst,0,0,$w,$h,imagecolortransparent($dst,imagecolorallocate($dst,255,255,255)));
		imagecopy($dst,$imgRes->image,($w-$width)/2,($h-$height)/2,0,0,$width,$height);
		imagepng($dst,$save_as);
		chmod($save_as,0777);
}
?> 
