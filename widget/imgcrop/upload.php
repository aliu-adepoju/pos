<?php 
ini_set("display_errors",1);
//echo $dsn;
/* print("<pre>");
print_r($_POST);
print_r($_FILES);
print("</pre>"); */
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<style>
	*{
	padding:0px;
	margin:0px;
	}
	body{
	text-align:left;
	overflow-y:hidden;
	}
	
.draggable{
	position:fixed;
	left:0px;
	top:0px;
	width:480px;
	height:480px;
	/*border:1px dashed #606060;*/
	background:#fff;
	opacity: 0.5;
	filter: alpha(opacity=50);
	cursor:move;
}
.tr{
	position: absolute;
	top:-5px;
	right:-5px;
	}
	
	.tl{
	position: absolute;
	top:-5px;
	left:-5px;
	}
	
	.bl{
	position: absolute;
	bottom:-5px;
	left:-5px;
	}
	
	.br{
	position: absolute;
	bottom:-5px;
	right:-5px;
	}
	
	.box{
			padding: 5px;
			background:#222;
			}
			
</style>
<script src="../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="../../js/jquery.extend.js" type="text/javascript"></script>
<script src="../../js/pi.js" type="text/javascript"></script>
<script src="../../js/imgResizer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#draggable').center();
	getCoord();	
});


function imgLoaded(filename){
	with(window.parent){
		$('#loading').hide();
		$("#image_store").find("a.thumbnail").each(function(index){
			if($(this).attr('data-src')=="sync/large/"+filename) {
				$(this).find("img")[0].src='sync/thumbnail/'+filename+'?r='+Math.random();
				ImageStore.Web.select(this);
			}
		});
		$('#thumbnail_1').show();
		$('#thumbnail_1').find('img')[0].src='sync/thumbnail/'+filename+'?r='+Math.random();
		} 
}
	
</script>
</head>
<body>
<?php
include_once '../../lib/smartResizeImage.php';
function __autoload($class_name){include_once "../../lib/{$class_name}.php";} 

$path2arr=pathinfo($_GET['img']);
$filename=$path2arr['basename'];
$path="../../sync";

if(!empty($_GET['coor'])){
	function clean($v){ return -intval($v);}//clean coordinate
	$coor=array_map("clean",explode(",",$_GET['coor']));
	$path2arr=pathinfo($_GET['img']);
	$imgCrop=new ImageCrop("$path/large/{$path2arr['basename']}","$path/thumbnail/{$path2arr['basename']}");
	$path2arr=pathinfo($imgCrop->coor($coor));
	$filename=$path2arr['basename'];
}


try{
if (file_exists("$path/large/$filename")){
	if(file_exists("$path/thumbnail/$filename")){
		//smart_resize_image("$path/thumbnail/$filename",64,64,true,"$path/thumbnail/$filename");
		}
	else{
		list($width,$height,$imageType)=getimagesize("$path/large/$filename");
		$adj_height=($height > 565)?(565):($height);
		$adj_width=($adj_height/$height)*$width;
		smart_resize_image("$path/large/$filename",$adj_width,$adj_height,true,"$path/large/$filename");
		smart_resize_image("$path/large/$filename",$adj_width,$adj_height,true,"$path/thumbnail/$filename");
		} 
	echo "<img src='$path/large/$filename'  id='srcimg' onload=\"imgLoaded('$filename')\"/>";

}

}
catch (Exception $ex){
	$err=$ex->getMessage();
}
?>
<div id="draggable" class='draggable' style='position:fixed;left:0px;top:0px' onmousedown='dragObj(event)'>
<div class='tr box'></div>
<div class='tl box'></div>
<div class='bl box'></div>
<div class='br box'></div>
</div>
</body>
</html>