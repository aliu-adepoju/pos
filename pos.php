<?php
session_start();
session_regenerate_id();
if(!isset($_SESSION['usrID'])){
	header("Location: index.php");
	exit(0);
}
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
//include_once 'controllers/__profile.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="";
$layout->content("view/pos/_toolbar.php","view/pos/_body.php"); 